﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using XLabs.Ioc;
using XLabs.Platform.Services.Media;
using XLabs.Platform.Device;
using Xamarin.Forms;
using FFImageLoading;
using FormsPlugin.Iconize.Droid;
using RohTJ.Services;
using RohTJ.Droid.Helpers;
using Gcm.Client;
using FormsToolkit;
using RohTJ.Models;
using FormsToolkit.Droid;
using Newtonsoft.Json;


namespace RohTJ.Droid
{
    [Activity(Label = "Roh", Icon = "@drawable/icon", Theme = "@style/MainTheme",
        ScreenOrientation = ScreenOrientation.Portrait,
        Exported = true,
        LaunchMode = LaunchMode.SingleTask,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        bool _initialized = false;

        protected override void OnCreate(Bundle bundle)
        {

            base.OnCreate(bundle);
            
            // Keep's screen on
            //this.Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);

            #region FFImageLoading
            FFImageLoading.Forms.Droid.CachedImageRenderer.Init();

            var config = new FFImageLoading.Config.Configuration()
            {
                VerboseLogging = false,
                VerbosePerformanceLogging = false,
                VerboseMemoryCacheLogging = false,
                VerboseLoadingCancelledLogging = false,
            };
            ImageService.Instance.Initialize(config);

            #endregion
            global::Xamarin.Forms.Forms.Init(this, bundle);
            Toolkit.Init();
            IconControls.Init(Resource.Id.toolbar, Android.Resource.Id.Tabs);
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            Xamarin.FormsGoogleMaps.Init(this, bundle); // initialize for Xamarin.Forms.GoogleMaps
            if (!_initialized) SetIoc();

            var width = Resources.DisplayMetrics.WidthPixels;
            var height = Resources.DisplayMetrics.HeightPixels;
            var density = Resources.DisplayMetrics.Density;

            App.ScreenWidth = (width - 0.5f) / density;
            App.ScreenHeight = (height - 0.5f) / density;
            RegisterWithGCM();

            LoadApplication(new App());

            OnNewIntent(Intent);
        }

        private void ParseIntentParam(Intent intent)
        {
            string parameterValue = intent.GetStringExtra("param");
            if (!string.IsNullOrWhiteSpace(parameterValue))
            {
                RohTJ.Helpers.Settings.PushParameter = parameterValue;
                var paramObject = JsonConvert.DeserializeObject<PushParameter>(parameterValue);
                switch (paramObject.Code)
                {
                    case "review":
                        var deepLinkPage = new DeepLinkPage { Id = paramObject.Id };
                        deepLinkPage.Page = deepLinkPage.GetAppPage(paramObject.Code);// AppPage.Review
                        MessagingService.Current.SendMessage<DeepLinkPage>("DeepLinkPage", deepLinkPage);
                        break;
                    default:
                        break;
                }
            }
        }

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            ParseIntentParam(intent);
        }

        private void RegisterWithGCM()
        {
            // Check to ensure everything's setup right for push
            GcmClient.CheckDevice(this);
            GcmClient.CheckManifest(this);
            GcmClient.Register(this, NotificationsBroadcastReceiver.SenderIDs);
        }

        void SetIoc()
        {
            DependencyService.Register<IMediaPicker, MediaPicker>();
            DependencyService.Register<IDevice, AndroidDevice>();
            DependencyService.Register<IScreenService, ScreenService>();
            _initialized = true;
        }
    }
}

