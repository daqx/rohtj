using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RohTJ.Services;
using System.Threading.Tasks;
using Android.Media;
using Xamarin.Forms;
using RohTJ.Droid.Helpers;

[assembly: Dependency(typeof(SoundProvider))]
namespace RohTJ.Droid.Helpers
{
    public class SoundProvider : ISoundProvider
    {
        public void PlaySoundAsync(string filename)
        {
            // Create media player
            var player = new MediaPlayer();

            // Create task completion source to support async/await
            var tcs = new TaskCompletionSource<bool>();
            var _player = MediaPlayer.Create(global::Android.App.Application.Context, Resource.Raw.notification);
            _player.Start();
            // Open the resource
            //var fd = Xamarin.Forms.Forms.Context.Assets.OpenFd(filename);

            //// Hook up some events
            //player.Prepared += (s, e) =>
            //{
            //    player.Start();
            //};

            //player.Completion += (sender, e) =>
            //{
            //    tcs.SetResult(true);
            //};

            //// Start playing
            //player.SetDataSource(fd.FileDescriptor); player.Prepare();

            //return tcs.Task;
        }
    }
}