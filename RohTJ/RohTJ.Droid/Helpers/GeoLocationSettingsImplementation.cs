using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RohTJ.Services;
using Android.Locations;
using Xamarin.Forms;
using RohTJ.Droid.Helpers;

[assembly: Xamarin.Forms.Dependency (typeof (GeoLocationSettingsImplementation))]
namespace RohTJ.Droid.Helpers
{
    public class GeoLocationSettingsImplementation : IGeoLocationSettings
    {
        public GeoLocationSettingsImplementation(){}

        public bool IsEnabled()
        {
            LocationManager locationManager = (LocationManager)Forms.Context.GetSystemService(Context.LocationService);
            return locationManager.IsProviderEnabled(LocationManager.GpsProvider);
        }

        public void OpenGeoLocationSettings()
        {
            Intent gpsSettingIntent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
            Forms.Context.StartActivity(gpsSettingIntent);
        }
    }
}