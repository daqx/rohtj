using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Telephony;
using FormsToolkit;
using RohTJ.Models;
using Xamarin.Forms;

namespace RohTJ.Droid.Helpers
{
    [BroadcastReceiver()]
    [IntentFilter(new[] { "android.intent.action.PHONE_STATE" })]
    public class IncomingCallReceiver : BroadcastReceiver
    {
        static bool offhook = false;

        public override void OnReceive(Context context, Intent intent)
        {
            // ensure there is information
            if (intent.Extras != null)
            {
                // get the incoming call state
                string state = intent.GetStringExtra(TelephonyManager.ExtraState);

                // check the current state
                if (state == TelephonyManager.ExtraStateRinging)
                {
                    // read the incoming call telephone number...
                    string telephone = intent.GetStringExtra(TelephonyManager.ExtraIncomingNumber);
                    // check the reade telephone
                    if (string.IsNullOrEmpty(telephone))
                        telephone = string.Empty;
                }
                else if (state == TelephonyManager.ExtraStateOffhook)
                {
                    // incoming call answer
                    string telephone = intent.GetStringExtra(TelephonyManager.ExtraIncomingNumber) ?? "+";
                    offhook = true;
                }
                else if (state == TelephonyManager.ExtraStateIdle)
                {
                    // incoming call end
                    string telephone = intent.GetStringExtra(TelephonyManager.ExtraIncomingNumber) ?? "+";
                    if (offhook && App.CurrentApp != null)
                    {
                        
                        MessagingCenter.Send<Xamarin.Forms.Application, string>(App.CurrentApp, MessageKeys.IncomingCall,telephone);
                    }
                    offhook = false;
                }
            }
        }
    }
}