using System;

using Android.App;
using Android.Content;
using Gcm.Client;
using Java.Lang;
using WindowsAzure.Messaging;
using Newtonsoft.Json.Linq;
using RohTJ.Models;
using Newtonsoft.Json;
using System.Threading.Tasks;
using FormsToolkit;

// These attributes are to register the right permissions for our app concerning push messages
[assembly: Permission(Name = "com.versluisit.xamarinnotifications.permission.C2D_MESSAGE")]
[assembly: UsesPermission(Name = "com.versluisit.xamarinnotifications.permission.C2D_MESSAGE")]
[assembly: UsesPermission(Name = "com.google.android.c2dm.permission.RECEIVE")]

//GET_ACCOUNTS is only needed for android versions 4.0.3 and below
[assembly: UsesPermission(Name = "android.permission.GET_ACCOUNTS")]
[assembly: UsesPermission(Name = "android.permission.INTERNET")]
[assembly: UsesPermission(Name = "android.permission.WAKE_LOCK")]

namespace RohTJ.Droid.Helpers
{
    // These attributes belong to the BroadcastReceiver, they register for the right intents
    [BroadcastReceiver(Permission = Constants.PERMISSION_GCM_INTENTS)]
    [IntentFilter(new[] { Constants.INTENT_FROM_GCM_MESSAGE }, Categories = new[] { "com.versluisit.xamarinnotifications" })]
    [IntentFilter(new[] { Constants.INTENT_FROM_GCM_REGISTRATION_CALLBACK }, Categories = new[] { "com.versluisit.xamarinnotifications" })]
    [IntentFilter(new[] { Constants.INTENT_FROM_GCM_LIBRARY_RETRY }, Categories = new[] { "com.versluisit.xamarinnotifications" })]

    // This is the bradcast reciever
    public class NotificationsBroadcastReceiver : GcmBroadcastReceiverBase<PushHandlerService>
    {
        // TODO add your project number here
        public static string[] SenderIDs = { "441894223829" };
    }

    [Service] // Don't forget this one! This tells Xamarin that this class is a Android Service
    public class PushHandlerService : GcmServiceBase
    {
        // TODO add your own access key
        private string _connectionString = ConnectionString.CreateUsingSharedAccessKeyWithListenAccess(
            new Java.Net.URI("sb://rohtj2017.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=TeAQPTUwrSGmtLjMxCVEpNAqPvR+iN/1JyZoB43crAo="), "rohtj");

        // TODO add your own hub name
        private string _hubName = "rohtj";

        public static string RegistrationID { get; private set; }

        public PushHandlerService() : base(NotificationsBroadcastReceiver.SenderIDs)
        {
        }

        // This is the entry point for when a notification is received
        protected override void OnMessage(Context context, Intent intent)
        {
            var title = "Roh";

            if (intent.Extras.ContainsKey("title"))
                title = intent.Extras.GetString("title");

            var messageText = intent.Extras.GetString("message");
            var param = intent.Extras.GetString("param");
            if (HandlePushParam(param))
            {
                return;
            }

            if (!string.IsNullOrEmpty(messageText))
                CreateNotification(title, messageText, param);
        }

        private bool HandlePushParam(string param)
        {
            bool result = false;
            PushParameter paramObject;
            try
            {
                paramObject = JsonConvert.DeserializeObject<PushParameter>(param);
                if (paramObject == null)
                    return false;
            }
            catch (System.Exception)
            {
                return false;
            }

            switch (paramObject.Code)
            {
                case "client_accepted":
                    SendClientAcceptedMessage(paramObject.Code, paramObject.Id);
                    result = true;
                    break;
                default:
                    break;
            }

            return result;
        }

        private void SendClientAcceptedMessage(string code, string id)
        {
            var deepLinkPage = new DeepLinkPage { Id = id };
            deepLinkPage.Page = deepLinkPage.GetAppPage(code);
            MessagingService.Current.SendMessage<DeepLinkPage>("DeepLinkPage", deepLinkPage);
        }

        // The method we use to compose our notification
        private void CreateNotification(string title, string desc, string param)
        {
            // First we make sure our app will start when the notification is pressed

            var notificationIntent = new Intent(this, typeof(MainActivity));
            notificationIntent.AddFlags(ActivityFlags.ClearTop | ActivityFlags.NewTask);
            if (!string.IsNullOrEmpty(param))
            {
                notificationIntent.PutExtra("param", param.ToString());
            }

            var pendingIntent = PendingIntent.GetActivity(this, 0, notificationIntent, PendingIntentFlags.UpdateCurrent);

            //var soundUri = Android.Net.Uri.Parse("android.resource://" + ApplicationContext.PackageName + "/raw/" + Resource.Raw.notification);
            // Here we start building our actual notification, this has some more
            // interesting customization options!
            var builder = new Notification.Builder(this)
                .SetContentIntent(pendingIntent)
                .SetContentTitle(title)
                .SetAutoCancel(true)
                .SetContentText(desc)
                .SetSmallIcon(Resource.Drawable.icon);
            //.SetSound(soundUri);

            // Build the notification
            var notification = builder.Build();
            notification.Defaults = NotificationDefaults.All;

            // Get the notification manager
            var notificationManager = GetSystemService(NotificationService) as NotificationManager;
            // Publish the notification to the notification manager
            notificationManager.Notify(Global.PushNumber++, notification);
        }

        // Whenever an error occurs in regard to push registering, this fires
        protected override void OnError(Context context, string errorId)
        {
            Console.Out.WriteLine(errorId);
        }

        // This handles the successful registration of our device to Google
        // We need to register with Azure here ourselves
        protected override void OnRegistered(Context context, string registrationId)
        {
            RohTJ.Helpers.Settings.PnsHandle = registrationId;
        }

        // This handles when our device unregisters at Google
        // We need to unregister with Azure
        protected override void OnUnRegistered(Context context, string registrationId)
        {
            var hub = new NotificationHub(_hubName, _connectionString, context);
            try
            {
                hub.UnregisterAll(registrationId);
            }
            catch (NotificationHubUnauthorizedException)
            {
                
            }
        }
    }
}