using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RohTJ.Services;

namespace RohTJ.Droid.Helpers
{
    public class ScreenService : IScreenService
    {
        /// <summary>
        /// While setting true to turnOn keeps screen On
        /// </summary>
        /// <param name="turnOn"></param>
        public void KeepScreenOn(bool turnOn)
        {
            var activity = Xamarin.Forms.Forms.Context as Activity;
            if (activity != null)
            {
                if (turnOn)
                    activity.Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);
                else
                    activity.Window.ClearFlags(WindowManagerFlags.KeepScreenOn);
            }
        }
    }
}