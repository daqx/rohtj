﻿using RohTJ.ViewModels.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RohTJ.Models;
using RohTJ.Pages;
using Xamarin.Forms;

namespace RohTJ
{
    public static class Global
    {
        public static SettingsViewModel SettingsViewModel { get; set; }

        public static string ServiceUrl { get { return ServiceHost + ":" + ServicePort + "/"; } }

        //public static string ServiceHost { get { return @"http://10.71.34.1"; } }
        //public static int ServicePort { get { return 56680; } }

        //public static string ServiceHost { get { return @"http://rohtjweb20160920064554.azurewebsites.net"; } }
        //public static int ServicePort { get { return 80; } }

        public static string ServiceHost { get { return @"https://roh.tj"; } }
        public static int ServicePort { get { return 443; } }

        public static MenuType LastVisited { get; set; } = MenuType.CityOrders;
        public static int PushNumber { get; set; }

        public static async Task<bool> IsBalanceEnough(Page page)
        {
            if (Global.SettingsViewModel.Client.IsDriver && Global.SettingsViewModel.Client.Balance < 1)
            {
                await page.DisplayAlert("Пополните баланс", "Не достаточно средств на вашем счету.", "Ок");
                return false;
            }
            return true;
        }

    }
}