﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.Models
{
    public class BrandModel : BaseModel
    {
        public string Name { get; set; }
        public Brand Brand { get; set; }
        public string BrandId { get; set; }

    }
}
