﻿using RohTJ.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace RohTj.Models
{
    public class GeoLocation: BaseModel
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public Client Client { get; set; }
        public string ClientId { get; set; }
        public bool IsDriver { get; set; }
    }
}
