﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RohTJ.Models
{
    public enum OrderCallState
    {
        New, Yes, No
    }

    /// <summary>
    /// Call to confirm order
    /// </summary>
    public class OrderCall: BaseModel
    {
        public Order Order { get; set; }
        public string OrderId { get; set; }

        public Client WhoCalled { get; set; }
        public string WhoCalledId { get; set; }

        public OrderCallState State { get; set; }
    }
}
