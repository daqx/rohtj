﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.Models
{
    public class City : BaseModel
    {
        public string Name { get; set; }
        public string RegionId { get; set; }
        public Region Region { get; set; }
        public string District { get; set; }
    }
}
