﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.Models
{
    public class Car : BaseModel
    {
        public Brand Brand { get; set; }
        public BrandModel BrandModel { get; set; }
        public string Number { get; set; }
        public string Color { get; set; }
        public int YearOfManifacture { get; set; }
    }
}
