﻿namespace RohTJ.Models
{
    public class Region: BaseModel
    {
        public string Name { get; set; }
    }
}