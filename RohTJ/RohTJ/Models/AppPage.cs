﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.Models
{
    public class DeepLinkPage
    {
        public AppPage Page { get; set; }
        public string Id { get; set; }
        public AppPage GetAppPage(string code)
        {
            AppPage page;
            switch (code)
            {
                case "review":
                    page = AppPage.Review;
                    break;
                case "client_accepted":
                    page = AppPage.ClientAccepted;
                    break;
                default:
                    page = AppPage.Information;
                    break;
            }
            return page;
        }
    }
    public enum AppPage
    {
        Information,
        ClientAccepted,
        Review
    }

    public static class MessageKeys
    {
        public const string IncomingCall = "incoming_call";
        public const string Error = "error";
        public const string Connection = "connection";
        public const string LoggedIn = "loggedin";
        public const string Message = "message";
        public const string Question = "question";
        public const string Choice = "choice";
    }
}
