﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.Models
{
    public class Review : BaseModel
    {
        public string ClientId { get; set; }
        public string DriverId { get; set; }
        /// <summary>
        /// Client who did review
        /// </summary>
        public Client Client { get; set; }
        /// <summary>
        /// Client who were reviewed
        /// </summary>
        public Client Driver { get; set; }

        /// <summary>
        /// Raiting from 1-5
        /// </summary>
        public int Raiting { get; set; }

        public string Comment { get; set; }
    }
}
