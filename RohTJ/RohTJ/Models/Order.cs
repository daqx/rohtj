﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.Models
{
    public enum OrderStatus
    {
        New,
        Started,
        Completed,
        Cancelled
    }

    public enum OrderType
    {
        City,
        BetweenCity,
        Truck
    }
    public class Order : BaseModel
    {
        public string From { get; set; }
        public string To { get; set; }
        double price;
        public double Price
        {
            get { return price; }
            set
            {
                price = value;
                NotifyPropertyChanged();
            }
        }
        public int FreePlace { get; set; }
        public double Weight { get; set; }
        public bool IsDriver { get; set; }
        public OrderType OrderType { get; set; }
        public string Note { get; set; }
        public string ImagePath { get; set; }
        public OrderStatus Status { get; set; }

        public string ClientId { get; set; }
        public Client Client { get; set; }

        public string DriverId { get; set; }
        public Client Driver { get; set; }
    }
}
