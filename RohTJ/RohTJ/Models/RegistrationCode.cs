﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.Models
{
    public class RegistrationCode : BaseModel
    {
        public string Code { get; set; }
        public Client Client { get; set; }
        public string ClientId { get; set; }
    }
}
