﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.Models
{
    public enum Platform
    {
        Android, iOS, Windows
    }
    public class Client : BaseModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }
        bool isDriver;
        public bool IsDriver
        {
            get { return isDriver; }
            set
            {
                isDriver = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(IsFreeToString));
            }
        }

        bool isFree;
        public bool IsFree
        {
            get { return isFree; }
            set
            {
                isFree = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(IsFreeToString));
                NotifyPropertyChanged(nameof(IsFreeToImage));
            }
        }

        public string IsFreeToString
        {
            get { return isDriver ? (isFree ? "Свободен" : "Занят") : ""; }
        }

        public string IsFreeToImage
        {
            get { return isDriver ? (isFree ? "free.png" : "busy.png") : ""; }
        }

        public double Balance { get; set; }
        DateTime? birthDay;
        public DateTime? BirthDay
        {
            get { return birthDay; }
            set
            {
                if (value == null)
                    birthDay = new DateTime(2000, 1, 1);
                else
                    birthDay = value;

                NotifyPropertyChanged(nameof(BirthDay));
            }
        }
        public int Gender { get; set; }
        public Platform Platform { get; set; }
        public string Email { get; set; }
        public string CityId { get; set; }
        public City City { get; set; }
        public string PhoneNumber { get; set; }

        public string BrandId { get; set; }
        public Brand Brand { get; set; }

        public string BrandModelId { get; set; }
        public BrandModel BrandModel { get; set; }
        public string Number { get; set; }
        public string Color { get; set; }
        public int YearOfManifacture { get; set; }

        bool uploadedPhoto;
        public bool UploadedPhoto
        {
            get { return uploadedPhoto; }
            set
            {
                if (value != uploadedPhoto)
                {
                    uploadedPhoto = value;
                    NotifyPropertyChanged();
                    NotifyPropertyChanged(nameof(PhotoPath));
                }
            }
        }

        public bool Blocked { get; set; }
        public bool IsNotificationsOn { get; set; }

        double rating=0;
        public double Rating { get { return Math.Round(rating, 2); }
            set { rating = value;
                NotifyPropertyChanged();
            } }

        public bool IsDispatching { get; set; }

        public string PhotoPath { get { return this.UploadedPhoto ? Global.ServiceUrl + "images/client/" + Id + ".jpg" : ErrorPhotoPath; } }
        public string ErrorPhotoPath { get { return Global.ServiceUrl + "images/client/blank.png"; } }
    }
}
