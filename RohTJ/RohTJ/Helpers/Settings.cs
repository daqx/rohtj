// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace RohTJ.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private static readonly string SettingsDefault = string.Empty;

        #endregion

        #region IsVoiceOn Constants

        private const string IsVoiceOnKey = "isvoiceon_key";
        private static readonly bool IsVoiceOnDefault = true;

        #endregion
        #region IsNotifyOn Constants

        private const string IsNotifyOnKey = "isnotifyon_key";
        private static readonly bool IsNotifyOnDefault = true;

        #endregion
        #region IsDisplayOn Constants

        private const string IsDisplayOnKey = "isdisplayon_key";
        private static readonly bool IsDisplayOnDefault = true;

        #endregion

        #region ClientId Constants

        private const string ClientIdKey = "clientid_key";
        private static readonly string ClientIdDefault = string.Empty;

        #endregion
        #region RegistrationCode Constants

        private const string RegistrationCodeKey = "registrationcode_key";
        private static readonly string RegistrationCodeDefault = string.Empty;

        #endregion
        #region PnsHandle Constants

        private const string PnsHandleKey = "pnshandlekey";
        private static readonly string PnsHandleDefault = null;

        #endregion
        #region HubRegistrationId Constants

        private const string HubRegistrationIdKey = "hubregistrationid_key";
        private static readonly string HubRegistrationIdDefault = null;

        #endregion

        #region PushParameter Constants

        private const string PushParameterKey = "pushparameter_key";
        private static readonly string PushParameterDefault = null;

        #endregion


        public static string GeneralSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SettingsKey, value);
            }
        }

        public static string ClientId
        {
            get
            {
                return AppSettings.GetValueOrDefault(ClientIdKey, ClientIdDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(ClientIdKey, value);
            }
        }
        public static string RegistrationCode
        {
            get
            {
                return AppSettings.GetValueOrDefault(RegistrationCodeKey, RegistrationCodeDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(RegistrationCodeKey, value);
            }
        }
        public static bool IsVoiceOn
        {
            get
            {
                return AppSettings.GetValueOrDefault(IsVoiceOnKey, IsVoiceOnDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(IsVoiceOnKey, value);
            }
        }
        public static bool IsNotifyOn
        {
            get
            {
                return AppSettings.GetValueOrDefault(IsNotifyOnKey, IsNotifyOnDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(IsNotifyOnKey, value);
            }
        }
        public static bool IsDisplayOn
        {
            get
            {
                return AppSettings.GetValueOrDefault(IsDisplayOnKey, IsDisplayOnDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(IsDisplayOnKey, value);
            }
        }
        public static string PnsHandle
        {
            get
            {
                return AppSettings.GetValueOrDefault(PnsHandleKey, PnsHandleDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(PnsHandleKey, value);
            }
        }

        public static string HubRegistrationId
        {
            get
            {
                return AppSettings.GetValueOrDefault(HubRegistrationIdKey, HubRegistrationIdDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(HubRegistrationIdKey, value);
            }
        }

        public static string PushParameter
        {
            get
            {
                return AppSettings.GetValueOrDefault(PushParameterKey, PushParameterDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(PushParameterKey, value);
            }
        }

    }
}