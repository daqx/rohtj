﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.Helpers
{
    public class Timer
    {
        private bool timerRunning;
        private TimeSpan interval;
        private Action tick;
        private bool runOnce;

        public Timer(TimeSpan interval, Action tick, bool runOnce = false)
        {
            this.interval = interval;
            this.tick = tick;
            this.runOnce = runOnce;
        }

        public async Task<Timer> Start()
        {
            if (!timerRunning)
            {
                timerRunning = true;
                await RunTimer();
            }

            return this;
        }

        public void Stop()
        {
            timerRunning = false;
        }

        private async Task RunTimer()
        {
            while (timerRunning)
            {
                await Task.Delay(interval);

                if (timerRunning)
                {
                    tick();

                    if (runOnce)
                    {
                        Stop();
                    }
                }
            }
        }
    }
}
