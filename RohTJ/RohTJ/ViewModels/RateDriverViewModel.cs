﻿using RohTJ.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.ViewModels
{
    public class RateDriverViewModel : BaseViewModel
    {
        int rating;
        public int Rating
        {
            get { return rating; }
            set
            {
                SetProperty(ref rating, value);
            }
        }

        public Models.Order Order { get; set; }
    }
}
