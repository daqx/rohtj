﻿using RohTJ.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.ViewModels
{
    public class RatingViewModel : BaseViewModel
    {
        double rating;
        public double Rating
        {
            get { return rating; }
            set
            {
                SetProperty(ref rating, value);
            }
        }
    }
}
