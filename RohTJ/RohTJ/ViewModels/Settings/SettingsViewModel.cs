﻿using RohTJ.Models;
using RohTJ.Pages;
using RohTJ.Services;
using RohTJ.ViewModels.Base;
using System.Collections.Generic;
using Xamarin.Forms;

namespace RohTJ.ViewModels.Settings
{
    public class SettingsViewModel : BaseViewModel
    {
        Models.Client client;

        public bool IsVoiceOn { get { return Helpers.Settings.IsVoiceOn; } private set { Helpers.Settings.IsVoiceOn = value; } }
        public bool IsNotifyOn
        {
            get
            {
                return Client?.IsNotificationsOn ?? true;
            }
            set
            {
                if (Client != null && Client.IsNotificationsOn != value)
                {
                    Client.IsNotificationsOn = value;
                    IsNotifyOnUpdateCommand.Execute(null);
                    OnPropertyChanged(nameof(IsNotifyOn));
                }
            }
        }
        public bool IsDisplayOn
        {
            get { return Helpers.Settings.IsDisplayOn; }
            internal set { Helpers.Settings.IsDisplayOn = value; }
        }
        public string ClientId { get { return Helpers.Settings.ClientId; } private set { Helpers.Settings.ClientId = value; } }
        public string RegistrationCode { get { return Helpers.Settings.RegistrationCode; } internal set { Helpers.Settings.RegistrationCode = value; } }

        public List<Brand> Brands { get; internal set; } = new List<Brand>();
        public List<BrandModel> BrandModels { get; internal set; } = new List<BrandModel>();
        public List<Region> Regions { get; internal set; } = new List<Region>();
        public List<City> Cities { get; internal set; } = new List<City>();
        public Xamarin.Forms.GoogleMaps.Position Position { get; internal set; }

        public Models.Client Client
        {
            get { return client; }
            internal set
            {
                client = value;
                ClientId = client?.Id;
            }
        }
        public bool IsAuthenticated { get; internal set; }


        public void SaveState(IDictionary<string, object> dictionary)
        {
        }
        public void RestoreState(IDictionary<string, object> dictionary)
        {
            //RefreshCanExecutes();
        }

        public T GetDictionaryEntry<T>(IDictionary<string, object> dictionary, string key, T defaultValue)
        {
            if (dictionary.ContainsKey(key))
                return (T)dictionary[key];
            return defaultValue;
        }

        #region LogOutCommand
        Command _LogOutCommand;

        /// <summary>
        /// Command to log out
        /// </summary>
        public Command LogOutCommand
        {
            get { return _LogOutCommand ?? (_LogOutCommand = new Command(ExecuteLogOutCommand)); }
        }


        void ExecuteLogOutCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            LogOutCommand.ChangeCanExecute();

            Global.SettingsViewModel.ClientId = string.Empty;
            Global.SettingsViewModel.IsAuthenticated = false;
            App.GotoLoginPage();
            IsBusy = false;
            LogOutCommand.ChangeCanExecute();
        }
        #endregion

        #region IsNotifyOnUpdateCommand
        Command _IsNotifyOnUpdateCommand;

        /// <summary>
        /// Command to update IsNotifyOn
        /// </summary>
        public Command IsNotifyOnUpdateCommand
        {
            get { return _IsNotifyOnUpdateCommand ?? (_IsNotifyOnUpdateCommand = new Command(ExecuteIsNotifyOnUpdateCommand)); }
        }

        async void ExecuteIsNotifyOnUpdateCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            IsNotifyOnUpdateCommand.ChangeCanExecute();

            var dataService = new DataService();
            var client =await dataService.GetByIdAsync<Models.Client>(Client.Id);
            client.IsNotificationsOn = Client.IsNotificationsOn;
            Client = client;
            await dataService.PutAsync(Client);
            IsBusy = false;
            IsNotifyOnUpdateCommand.ChangeCanExecute();
        }
        #endregion
    }
}
