﻿using RohTJ.Models;
using RohTJ.Pages;
using RohTJ.Services;
using RohTJ.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XLabs.Platform.Device;

namespace RohTJ.ViewModels.Orders
{

    public class OrdersViewModel : BaseViewModel, IClientTypeHolder
    {
        DataService dataService;

        ObservableCollection<Order> orders;
        public ObservableCollection<Order> Orders
        {
            get
            {
                return orders;
            }
            set
            {
                orders = value;
                OnPropertyChanged();
            }
        }

        ObservableCollection<Order> myOrders;
        public ObservableCollection<Order> MyOrders
        {
            get
            {
                return myOrders;
            }
            set
            {
                myOrders = value;
                OnPropertyChanged();
            }
        }

        string from;
        string to;
        double price;
        string note;

        int fromSelectedIndex;
        int toSelectedIndex;
        private ISoundProvider soundProvider;

        public string From { get { return from; } set { SetProperty(ref from, value); } }
        public string To { get { return to; } set { SetProperty(ref to, value); } }
        public double Price { get { return price; } set { SetProperty(ref price, value); } }
        public string Note { get { return note; } set { SetProperty(ref note, value); } }

        public int FromSelectedIndex { get { return fromSelectedIndex; } set { SetProperty(ref fromSelectedIndex, value); } }
        public int ToSelectedIndex { get { return toSelectedIndex; } set { SetProperty(ref toSelectedIndex, value); } }

        public Models.OrderType OrderType { get; set; }
        public bool IsDriver { get; private set; }
        public double Balance { get { return Global.SettingsViewModel.Client.Balance; } set { } }

        public OrdersViewModel()
        {
            dataService = new DataService();
            Orders = new ObservableCollection<Order>();
            MyOrders = new ObservableCollection<Order>();
            IsDriver = Global.SettingsViewModel.Client.IsDriver;
            soundProvider = DependencyService.Get<ISoundProvider>();
        }

        #region Load Orders
        Command _LoadOrdersCommand;

        /// <summary>
        /// Command to load orders
        /// </summary>
        public Command LoadOrdersCommand
        {
            get { return _LoadOrdersCommand ?? (_LoadOrdersCommand = new Command(ExecuteLoadOrdersCommand)); }
        }
        DateTimeOffset? previousMaxDateTime = DateTimeOffset.MinValue;
        async void ExecuteLoadOrdersCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            LoadOrdersCommand.ChangeCanExecute();

            Orders = new ObservableCollection<Order>(await dataService.GetOrdersAsync(Global.SettingsViewModel.Client, OrderType));
            NotifyAboutNewOrders();

            IsBusy = false;
            LoadOrdersCommand.ChangeCanExecute();
        }

        private void NotifyAboutNewOrders()
        {
            if (Global.SettingsViewModel.IsNotifyOn)
            {
                var maxDateTime = Orders?.Where(o => o.Status == OrderStatus.New).Max(o => o.CreatedAt) ?? DateTimeOffset.MinValue;
                if (previousMaxDateTime < maxDateTime)
                    soundProvider?.PlaySoundAsync("notification.mp3");

                previousMaxDateTime = maxDateTime;
            }
        }
        #endregion

        #region Load MyOrders
        Command _LoadMyOrdersCommand;

        /// <summary>
        /// Command to load my orders
        /// </summary>
        public Command LoadMyOrdersCommand
        {
            get { return _LoadMyOrdersCommand ?? (_LoadMyOrdersCommand = new Command(ExecuteLoadMyOrdersCommand)); }
        }


        async void ExecuteLoadMyOrdersCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            LoadMyOrdersCommand.ChangeCanExecute();

            MyOrders = new ObservableCollection<Order>(await dataService.GetMyOrdersAsync(Global.SettingsViewModel.Client, OrderType));

            IsBusy = false;
            LoadMyOrdersCommand.ChangeCanExecute();
        }
        #endregion

        #region Delete MyOrder
        Command _DeleteMyOrderCommand;

        /// <summary>
        /// Command to delete order
        /// </summary>
        public Command DeleteMyOrderCommand
        {
            get { return _DeleteMyOrderCommand ?? (_DeleteMyOrderCommand = new Command(param => { ExecuteDeleteMyOrderCommand(param); })); }
        }


        async void ExecuteDeleteMyOrderCommand(object param)
        {
            if (IsBusy)
                return;
            var order = param as Order;
            if (order == null)
                return;

            IsBusy = true;

            await dataService.DeleteAsync(order);

            IsBusy = false;
            LoadMyOrdersCommand.Execute(null);
        }
        #endregion

        #region Fill between order form
        Command _FillBetweenOrderFormCommand;

        /// <summary>
        /// Command to Fill BetweenOrder Form
        /// </summary>
        public Command FillBetweenOrderFormCommand
        {
            get { return _FillBetweenOrderFormCommand ?? (_FillBetweenOrderFormCommand = new Command(param => { ExecuteFillBetweenOrderFormCommand(param); })); }
        }


        async void ExecuteFillBetweenOrderFormCommand(object param)
        {
            if (param == null)
            {
                return;
            }
            var order = param as Order;
            if (order == null)
                return;

            IsBusy = true;

            await dataService.DeleteAsync(order);

            IsBusy = false;
            LoadMyOrdersCommand.Execute(null);
        }
        #endregion

        #region Call to Order
        Command _CallToOrderCommand;

        /// <summary>
        /// Command to call order
        /// </summary>
        public Command CallToOrderCommand
        {
            get { return _CallToOrderCommand ?? (_CallToOrderCommand = new Command(param => { ExecuteCallToOrderCommand(param); })); }
        }


        async void ExecuteCallToOrderCommand(object param)
        {
            if (IsBusy)
                return;
            var order = param as Order;
            if (order == null)
                return;

            IsBusy = true;
            // Register call
            var orderCall = new OrderCall()
            {
                Id = Guid.NewGuid().ToString(),
                OrderId = order.Id,
                WhoCalledId = Global.SettingsViewModel.ClientId,
                State = OrderCallState.New
            };
            await dataService.PostAsync(orderCall);

            var device = DependencyService.Get<IDevice>();
            if (device?.PhoneService != null)
            {
                device.PhoneService.DialNumber(order.Client.PhoneNumber);
            }

            IsBusy = false;
            LoadMyOrdersCommand.Execute(null);
        }
        #endregion
    }
}
