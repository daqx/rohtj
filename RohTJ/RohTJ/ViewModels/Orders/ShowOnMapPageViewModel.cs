﻿using RohTJ.Models;
using RohTJ.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XLabs.Platform.Device;

namespace RohTJ.ViewModels.Orders
{
    public class ShowOnMapPageViewModel: BaseViewModel
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Price { get; set; }
        public Order Order { get; set; }

        #region Call to Order
        Command _CallToOrderCommand;

        /// <summary>
        /// Command to call order
        /// </summary>
        public Command CallToOrderCommand
        {
            get { return _CallToOrderCommand ?? (_CallToOrderCommand = new Command(param => { ExecuteCallToOrderCommand(param); })); }
        }


        void ExecuteCallToOrderCommand(object param)
        {
            if (IsBusy)
                return;
            var order = param as Order;
            if (order == null)
                return;

            IsBusy = true;

            var device = DependencyService.Get<IDevice>();
            if (device?.PhoneService != null)
                device.PhoneService.DialNumber(GetPhoneNumber(order));

            IsBusy = false;
        }

        string GetPhoneNumber(Order order)
        {
            if (Global.SettingsViewModel.Client.IsDriver)
                return order.Client.PhoneNumber;
            else
                return order.Driver.PhoneNumber;
        }
        #endregion
    }
}
