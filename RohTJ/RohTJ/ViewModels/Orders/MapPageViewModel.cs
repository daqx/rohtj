﻿using RohTJ.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.ViewModels.Orders
{
    public class MapPageViewModel: BaseViewModel
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Price { get; set; }
        public Models.Order Order { get; set; }
    }
}
