﻿using RohTJ.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.ViewModels.Orders
{
    public class OrderFormPageViewModel : BaseViewModel
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Price { get; set; }
        public string Note { get; set; }
        public double Balance { get { return Global.SettingsViewModel.Client.Balance; } set { } }
        public bool IsDriver { get { return Global.SettingsViewModel.Client.IsDriver; } set { } }

    }
}
