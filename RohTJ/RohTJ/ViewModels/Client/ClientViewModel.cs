﻿using RohTJ.ViewModels.Base;
using RohTJ.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RohTJ.ViewModels.Client
{
    class ClientViewModel : BaseViewModel
    {
        public ClientViewModel(INavigation navigation = null) : base(navigation)
        {
        }

        public Models.Client Client { get; set; }


    }
}
