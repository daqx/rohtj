﻿using RohTJ.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.ViewModels
{
    public class WebViewViewModel : BaseViewModel
    {
        private string url;
        public string Url
        {
            get { return url; }
            set { url = value; OnPropertyChanged(nameof(Url)); }
        }
    }
}
