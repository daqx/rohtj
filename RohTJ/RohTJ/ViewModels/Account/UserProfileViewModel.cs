﻿using RohTJ.Models;
using RohTJ.Services;
using RohTJ.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RohTJ.ViewModels.Account
{
    public class UserProfileViewModel : BaseViewModel
    {
        public UserProfileViewModel()
        {
            Cities = new ObservableCollection<City>(Global.SettingsViewModel.Cities);

            Brands = new ObservableCollection<Brand>(Global.SettingsViewModel.Brands);
            BrandModels = new ObservableCollection<BrandModel>(Global.SettingsViewModel.BrandModels);
        }
        Models.Client client = null;
        public Models.Client Client
        {
            get
            {
                return client;
            }
            set
            {
                if (client != value)
                {
                    client = value;
                    
                    if (client.Brand == null )
                    {
                        SelectedBrandIndex = 0;
                    }
                    else
                    {                        
                        SelectedBrandIndex = Brands.IndexOf(client.Brand);
                    }
                }
            }
        }

        private void UpdateBrandModels()
        {
            var selctedBrand = Brands[SelectedBrandIndex];
            var result = Global.SettingsViewModel.BrandModels.Where(b => b.BrandId == selctedBrand.Id);
            BrandModels.Clear();
            foreach (var model in result)
            {
                BrandModels.Add(model);
            }
        }


        #region Brand

        /// <summary>
        /// Returns collection of BrandModels
        /// </summary>
        public ObservableCollection<City> Cities { get; internal set; }
        public ObservableCollection<Brand> Brands { get; internal set; }
        public ObservableCollection<BrandModel> BrandModels { get; internal set; }

        int selectedBrandIndex = -1;
        public int SelectedBrandIndex
        {
            get
            {
                var result = Brands.FirstOrDefault(b => b.Name == Client?.Brand?.Name);
                return Brands.IndexOf(result);
            }
            set
            {
                if (selectedBrandIndex != value)
                {
                    selectedBrandIndex = value;
                    Client.Brand = Brands[selectedBrandIndex];
                    UpdateBrandModels();
                    OnPropertyChanged(nameof(SelectedBrandIndex));
                    OnPropertyChanged(nameof(BrandModels));
                }
            }
        }
        #endregion

        //Command _SaveCommand;

        //public Command SaveCommand
        //{
        //    get
        //    {
        //        return _SaveCommand ?? (_SaveCommand = new Command(async () => await DoSave()));
        //    }
        //}

        //async Task DoSave()
        //{
        //    if (IsBusy)
        //        return;

        //    IsBusy = true;

        //    App.GoToRoot();
        //    //TODO: Login logic

        //    IsBusy = false;

        //    await PopAsync();
        //}

    }
}
