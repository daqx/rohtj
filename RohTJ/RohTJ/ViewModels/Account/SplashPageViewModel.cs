﻿using RohTJ.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RohTJ.ViewModels.Account
{
    public class SplashPageViewModel: BaseViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }

        Command _LoginCommand;

        public Command LoginCommand
        {
            get
            {
                return _LoginCommand ?? (_LoginCommand = new Command(async () => await DoLogin()));
            }
        }

        async Task DoLogin()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            App.GoToRoot();
            //TODO: Login logic

            IsBusy = false;

            await PopAsync();
        }
    }
}
