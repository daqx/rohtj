﻿using RohTJ.Models;
using RohTJ.Pages.Base;
using RohTJ.Services;
using RohTJ.ViewModels;
using RohTJ.ViewModels.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RohTJ.Pages
{
    public partial class RateDriverPage : RateDriverPageXaml
    {
        public RateDriverPage()
        {
            InitializeComponent();

        }

        private async void SendClicked(object sender, EventArgs e)
        {
            Global.SettingsViewModel.Client.IsFree = !Global.SettingsViewModel.Client.IsFree;
            var review = new Review()
            {

                ClientId = Global.SettingsViewModel.Client.Id,
                DriverId = ViewModel.Order.DriverId,
                Raiting = ViewModel.Rating,
                Comment = uiMessage.Text
            };
            await (new DataService()).PostAsync<Review>(review);
            await DisplayAlert("Отзыв", "Ваш отзыв отправлен!","Ok");
            ((RootPage)App.CurrentApp.MainPage).NavigateAsync(MenuType.CityOrders);
        }

        private void OnStarTapped(object sender, EventArgs e)
        {
            if (sender == uiStarOne)
                ViewModel.Rating = 1;
            else if (sender == uiStarTwo)
                ViewModel.Rating = 2;
            else if (sender == uiStarThree)
                ViewModel.Rating = 3;
            else if (sender == uiStarFour)
                ViewModel.Rating = 4;
            else
                ViewModel.Rating = 5;
        }
    }

    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>1
    public abstract class RateDriverPageXaml : ModelBoundContentPage<RateDriverViewModel>
    {
    }
}
