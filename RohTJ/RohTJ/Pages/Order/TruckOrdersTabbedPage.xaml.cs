﻿using RohTJ.Pages.Order;
using RohTJ.ViewModels.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RohTJ.Pages.Order
{
    public partial class TruckOrdersTabbedPage : TabbedPage
    {
        bool isDriver;
        OrdersViewModel viewModel;
        public TruckOrdersTabbedPage()
        {
            InitializeComponent();
            InitializeTabs();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (isDriver != Global.SettingsViewModel.Client.IsDriver)
            {
                InitializeTabs();
            }
        }

        private void InitializeTabs()
        {
            Children.Clear();
            isDriver = Global.SettingsViewModel.Client.IsDriver;
            if (isDriver)
            {
                Children.Add(new TruckOrderFormPage() { BindingContext = this.viewModel });
                Children.Add(new OrdersPage() { BindingContext = this.viewModel });
            }
            else
            {
                Children.Add(new TruckOrderFormPage() { BindingContext = this.viewModel });
                Children.Add(new FreeAutoPage() { BindingContext = this.viewModel });
            }
            Children.Add(new MyOrdersPage() { BindingContext = this.viewModel });
        }

        public TruckOrdersTabbedPage(OrdersViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = viewModel;
            this.viewModel = viewModel;
            InitializeTabs();
        }
    }
}
