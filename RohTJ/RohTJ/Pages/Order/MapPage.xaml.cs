﻿using RohTJ.Controls;
using RohTJ.Pages.Base;
using RohTJ.ViewModels.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Plugin.Geolocator;
using System.Diagnostics;
using RohTJ.Helpers;
using RohTJ.Services;
using RohTJ.Models;
using Xamarin.Forms.GoogleMaps;
using RohTj.Models;
using FormsToolkit;

namespace RohTJ.Pages.Order
{
    public partial class MapPage : MapPageXaml
    {
        DataService dataService;
        Animation animation;
        const string CircleAnimation = "CircleAnimation";

        public MapPage()
        {
            InitializeComponent();

            ToolbarItems.Add(new ToolbarItem() { Text = "Снять заказ", Order = ToolbarItemOrder.Primary, Command = new Command(async () => await CancelOrder()) });

            dataService = new DataService();
            customMap.HasScrollEnabled = false;
            customMap.HasZoomEnabled = false;
            RequestPeriodicallyForCall();
            customMap.PinClicked += Pin_Clicked;

            animation = new Animation(v => circle.Radius = Distance.FromMeters(v), 10, 2000);
        }

        private async Task CancelOrder()
        {
            if (ViewModel?.Order != null)
            {
                ViewModel.Order.Status = OrderStatus.Cancelled;
                await dataService.PutAsync<Models.Order>(ViewModel.Order);
            }

            await Navigation.PopModalAsync();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            isDisappeared = false;
            //Move to Khujand
            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(40.2862, 69.6288), Distance.FromMiles(1.0)));
            MessagingCenter.Subscribe<Xamarin.Forms.Application, string>(App.CurrentApp, MessageKeys.IncomingCall, async (m, p) => await HandleIncomingCall(m, p));

            // MessagingService.Current.Subscribe<string>(MessageKeys.IncomingCall, async (m, p) => await HandleIncomingCall(m, p));
            await GetCurrentPosition();
            await UpdateGeoLocations();
            await requestForCallTimer.Start();
        }

        private async Task HandleIncomingCall(Application app, string phoneNumber)
        {
            requestForCallTimer.Stop();
            OrderCall orderCall = null;
            phoneNumber = phoneNumber.Replace("+", "");
            if (string.IsNullOrEmpty(phoneNumber))
                orderCall = await dataService.GetNewCall(ViewModel?.Order?.Id);
            else
                orderCall = await dataService.GetNewCallByPhone(ViewModel?.Order?.Id, phoneNumber);

            if (orderCall != null)
            {
                var result = await DisplayAlert("Закрытие заявки", $"Рейтинг: {orderCall.WhoCalled.Rating}, Вы договорились с водителем авто: {orderCall.WhoCalled.Color} {orderCall.WhoCalled?.Brand.Name} {orderCall.WhoCalled?.BrandModel?.Name}, тел: {orderCall.WhoCalled.PhoneNumber}?", "Да", "Нет");

                if (result)
                {
                    await DealWithDriverSucceeded(orderCall);
                }
                else
                {
                    await DealWithDriverUnsucceeded(orderCall);
                }
            }

            await requestForCallTimer.Start();
        }

        bool isDisappeared;
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            requestForCallTimer.Stop();
            isDisappeared = true;
            // MessagingCenter.Unsubscribe<Application, string>(App.CurrentApp, MessageKeys.IncomingCall);
        }

        Pin mainPin;
        Circle circle;
        Timer requestForCallTimer;
        int orderCallTryCount = 0;
        private void RequestPeriodicallyForCall()
        {
            requestForCallTimer = new Timer(TimeSpan.FromSeconds(20), async () =>
            {
                if (ViewModel?.Order == null)
                    return;

                if (ViewModel.Order.Status == OrderStatus.New)
                {
                    requestForCallTimer.Stop();
                    orderCallTryCount++;
                    if (orderCallTryCount >= 3)
                        await RaisePriceOrContinueSearch();

                    if (!isDisappeared)
                    {
                        await UpdateGeoLocations();
                        await requestForCallTimer.Start();
                    }
                }
                else
                    await UpdateClientAndDriverLocations();
            });
        }

        private async Task UpdateClientAndDriverLocations()
        {
            if (ViewModel?.Order?.Driver != null)
            {
                GeoLocation geoLocation = await dataService.GetClientGeoLocationAsync(ViewModel.Order.Driver);
                if (geoLocation != null)
                {
                    await UpdateGeoLocations(new List<GeoLocation> { geoLocation });
                }
            }
        }

        private async Task DealWithDriverUnsucceeded(OrderCall orderCall)
        {
            orderCall.State = OrderCallState.No;
            await dataService.PutAsync(orderCall);
        }

        private async Task DealWithDriverSucceeded(OrderCall orderCall)
        {
            StopCircleAnimation();
            orderCall.State = OrderCallState.Yes;
            await dataService.PutAsync<OrderCall>(orderCall);
            // Update order
            ViewModel.Order.Status = OrderStatus.Started;
            ViewModel.Order.DriverId = orderCall.WhoCalledId;
            await dataService.PutAsync<Models.Order>(ViewModel.Order);
            ViewModel.Order = await dataService.GetByIdAsync<Models.Order>(ViewModel.Order.Id);
            await dataService.NotifyDriverAboutOrderCall(orderCall);
            await MakeDriverBusy(orderCall.WhoCalled);

            //Close this page
            ToolbarItems[0].Text = "Выйти";
            ToolbarItems[0].Command = new Command(async () => await Navigation.PopModalAsync());
        }

        private async Task MakeDriverBusy(Client driver)
        {
            driver.IsFree = false;
            await dataService.PutAsync(driver);
        }

        bool isMessageCurrentlyDisplayed = false;
        private async Task RaisePriceOrContinueSearch()
        {
            if (isMessageCurrentlyDisplayed)
                return;
            orderCallTryCount = 0;
            isMessageCurrentlyDisplayed = true;
            var result = await DisplayActionSheet("Поднимите цену заказа", "Продолжить поиск", "Поднять цену на 1 сомон");
            isMessageCurrentlyDisplayed = false;
            if (result == "Поднять цену на 1 сомон")
            {
                ViewModel.Order.Price++;
                await dataService.PutAsync<Models.Order>(ViewModel.Order);
            }
        }

        //private void InsertShowOnMap(Models.Order order)
        //{
        //    Navigation.InsertPageBefore(new ShowOnMapPage() { BindingContext = new ShowOnMapPageViewModel() { Order = order } }
        //    , this);
        //}

        private async Task GetCurrentPosition()
        {
            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 50; //100 is new default
            Position position = Global.SettingsViewModel.Position; //new Position(40.2862, 69.6288);
            if (locator.IsGeolocationEnabled && position.Latitude == 0)
            {
                try
                {
                    var pos = await locator.GetPositionAsync(timeout:TimeSpan.FromMilliseconds( 20000));
                    position = new Position(pos.Latitude, pos.Longitude);
                }
                catch (OperationCanceledException coe)
                {
                    Debug.WriteLine(coe.Message);
                }
                Global.SettingsViewModel.Position = position;
            }

            SetMainPin(position);
            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(position, Distance.FromMiles(1.0)));
        }

        private void SetMainPin(Position position)
        {
            bool addPin = false;
            if (mainPin == null)
            {
                addPin = true;
                mainPin = new Pin()
                {
                    Type = PinType.Place,
                    Label = "Пассажир",
                    //Address = Global.SettingsViewModel.Client.PhoneNumber,
                    Icon = BitmapDescriptorFactory.FromBundle("pin.png"),
                };
            }
            mainPin.Position = position;

            if (addPin)
                customMap.Pins.Add(mainPin);

            UpdateCircle(position);
        }

        /// <summary>
        /// Finds near by drivers
        /// </summary>
        /// <returns></returns>
        async Task UpdateGeoLocations(List<GeoLocation> geoLocations = null)
        {
            if (geoLocations == null)
                geoLocations = await dataService.GetNearBy(Global.SettingsViewModel.Client);

            var position = Global.SettingsViewModel.Position; //new Position(40.2862, 69.6288);

            SetMainPin(position);
            ClearAllPinsButMain();
            MoveToPosition(geoLocations, position);

            foreach (var location in geoLocations)
            {
                position = new Position(location.Latitude, location.Longitude);
                var pin = new Pin
                {
                    Type = PinType.Place,
                    Position = position,
                    Label = "Водитель",
                    //Address = location.Client.PhoneNumber,
                    Icon = BitmapDescriptorFactory.FromBundle("wheel.png")
                };
                customMap.Pins.Add(pin);
            }
        }

        private void MoveToPosition(List<GeoLocation> geoLocations, Position position)
        {
            if (geoLocations?.Any() == true)
            {

                Position moveToPosition = position;
                if (ViewModel.Order.Status == OrderStatus.Started)
                    moveToPosition = new Position(geoLocations[0].Latitude, geoLocations[0].Longitude);

                customMap.MoveToRegion(MapSpan.FromCenterAndRadius(position, Distance.FromMiles(1.0)));
            }
        }

        private void ClearAllPinsButMain()
        {
            foreach (var pin in customMap.Pins.ToList())
            {
                if (pin.Label != Global.SettingsViewModel.Client.FullName && pin.Position != mainPin.Position)
                {
                    customMap.Pins.Remove(pin);
                }
            }
        }

        private void UpdateCircle(Position position)
        {
            bool addCircle = false;
            if (circle == null)
            {
                circle = new Circle();
                addCircle = true;
                RunCircleAnimation();
            }
            circle.Center = position;
            circle.Radius = Distance.FromMeters(10f);

            circle.StrokeColor = Color.White;
            circle.StrokeWidth = 0f;
            circle.FillColor = Color.FromRgba(150, 150, 245, 100);
            circle.Tag = "CIRCLE"; // Can set any object

            if (addCircle)
                customMap.Circles.Add(circle);

            if (!this.AnimationIsRunning(CircleAnimation) && ViewModel.Order.Status == OrderStatus.New)
                RunCircleAnimation();

        }

        private void RunCircleAnimation()
        {
            animation.Commit(this, CircleAnimation, 16, 5000, Easing.Linear, (v, c) => circle.Radius = Distance.FromMeters(10f), () => true);
            uiOfferingPrice.IsVisible = true;
        }

        private void StopCircleAnimation()
        {
            this.AbortAnimation(CircleAnimation);
            uiOfferingPrice.IsVisible = false;
        }

        private void Pin_Clicked(object sender, EventArgs e)
        {

        }
    }

    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>
    public abstract class MapPageXaml : ModelBoundContentPage<MapPageViewModel> { }
}
