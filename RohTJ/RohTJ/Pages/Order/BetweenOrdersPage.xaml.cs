﻿using RohTJ.Pages.Base;
using RohTJ.ViewModels.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using RohTJ.ViewModels;

namespace RohTJ.Pages.Order
{
    public partial class BetweenOrdersPage : BetweenOrderPageXaml
    {
        public BetweenOrdersPage()
        {
            InitializeComponent();
            this.AddBalanceToolbarItem();
        }
        
    }

    public abstract class BetweenOrderPageXaml : TabChildModelBoundContentPage<BetweenOrderPageViewModel>
    {
    }
}
