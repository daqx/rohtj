﻿using RohTJ.Pages.Base;
using RohTJ.Services;
using RohTJ.ViewModels.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RohTJ.Pages.Order
{
    public partial class BetweenOrderFormPage : BetweenOrderFormPageXaml
    {
        public BetweenOrderFormPage()
        {
            InitializeComponent();
            uiFrom.ItemsSource = Global.SettingsViewModel.Cities;
            uiTo.ItemsSource = Global.SettingsViewModel.Cities;

            uiCreateOrder.Clicked += uiCreateOrder_Clicked;
            this.AddBalanceToolbarItem();
        }
       
        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.ViewModel.FromSelectedIndex = 0;
            this.ViewModel.ToSelectedIndex = 0;
            this.ViewModel.Price = 0;
            this.ViewModel.Note = "";
        }

        private async void uiCreateOrder_Clicked(object sender, EventArgs e)
        {
            if (uiFrom.SelectedIndex<0)
            {
                await DisplayAlert("Ошибка заполнения", "Поле откуда не заполнена", "Ok");
                return;
            }

            if (uiTo.SelectedIndex<0)
            {
                await DisplayAlert("Ошибка заполнения", "Поле куда не заполнена", "Ok");
                return;
            }

            if (string.IsNullOrWhiteSpace(uiPrice.Text))
            {
                await DisplayAlert("Ошибка заполнения", "Поле стоимость не заполнена", "Ok");
                return;
            }

            IKeyboardInteractions keyboardInteractions = DependencyService.Get<IKeyboardInteractions>();
            keyboardInteractions.HideKeyboard();
            uiCreateOrder.IsEnabled = false;

            string from = uiFrom.Items[uiFrom.SelectedIndex];
            string to = uiTo.Items[uiTo.SelectedIndex];
            var order = await SaveOrder(from, to);

            var mapPageViewModel = new MapPageViewModel() { Order = order };
            var mapPage = new MapPage() { BindingContext = mapPageViewModel };
            await Navigation.PushModalAsync(new RohNavigationPage(mapPage));
            uiCreateOrder.IsEnabled = true;
        }

        async Task<Models.Order> SaveOrder(string from, string to)
        {
            Models.Order order = new Models.Order() { OrderType = Models.OrderType.BetweenCity };
            order.ClientId = Global.SettingsViewModel.Client.Id;
            order.From = from;
            order.To = to;
            order.Price = double.Parse(uiPrice.Text);
            order.Note = uiNote.Text;
            DataService service = new DataService();
            return await service.CreateOrderAsync(order);
        }
    }

    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>
    public abstract class BetweenOrderFormPageXaml : TabChildModelBoundContentPage<OrdersViewModel>
    {
    }
}
