﻿using RohTJ.Pages.Base;
using RohTJ.ViewModels.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RohTJ.Pages.Order
{
    public partial class BetweenMyOrdersPage : BetweenMyOrdersPageXaml
    {
        public BetweenMyOrdersPage()
        {
            InitializeComponent();
            this.AddBalanceToolbarItem();
        }
        
        protected override void OnAppearing()
        {
            base.OnAppearing();
            //if (ViewModel.IsInitialized)
            //    return;

            ViewModel.LoadMyOrdersCommand.Execute(null);

            ViewModel.IsInitialized = true;
        }
    }

    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>
    public abstract class BetweenMyOrdersPageXaml : TabChildModelBoundContentPage<OrdersViewModel>
    {
    }
}
