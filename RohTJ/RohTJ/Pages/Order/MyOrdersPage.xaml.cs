﻿using RohTJ.Pages.Base;
using RohTJ.Services;
using RohTJ.ViewModels.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using XLabs.Platform.Device;
using RohTJ.Models;
using RohTJ.ViewModels;

namespace RohTJ.Pages.Order
{
    public partial class MyOrdersPage : MyOrdersPageXaml
    {
        DataService dataService;
        public MyOrdersPage()
        {
            InitializeComponent();
            this.AddBalanceToolbarItem();
            dataService = new DataService();
        }

        async void ListItemTapped(object sender, ItemTappedEventArgs e)
        {
            string result;
            var order = e.Item as Models.Order;

            string[] param = new string[] { };
            if (Global.SettingsViewModel.Client.IsDriver)
            {
                if (order.IsDriver)
                {
                    param = new string[] { "Повторить заказ" };
                }
                else if (order.Status == OrderStatus.Started)
                {
                    param = new string[] { "Показать на карте", "Позвонить" };
                }
                result = await DisplayActionSheet("Выбери действие", "Закрыть", "Удалить заказ", param);
            }
            else
            {
                if (order.Status == OrderStatus.Started)
                {
                    param = new string[] { "Повторить заказ", "Показать на карте"/*,"Review" */};
                }
                else
                    param = new string[] { "Повторить заказ"/*, "Review" */};
                
                result = await DisplayActionSheet("Выбери действие", "Закрыть", "Удалить заказ", param);
            }
            switch (result)
            {
                case "Удалить заказ":
                    ViewModel.DeleteMyOrderCommand.Execute(e.Item);
                    break;
                case "Повторить заказ":
                    RepeatOrder(order);
                    break;
                case "Показать на карте":
                    await ShowOnMap(order);
                    break;
                case "Позвонить":
                    var device = DependencyService.Get<IDevice>();
                    if (device?.PhoneService != null)
                    {
                        device.PhoneService.DialNumber(order.Client.PhoneNumber);
                    }
                    break;
                case "Review":
                    await Navigation.PushAsync(new RateDriverPage()
                    {
                        BindingContext = new RateDriverViewModel() {Order = order }
                    });
                    break;

                default:
                    break;
            }
        }

        private async Task ShowOnMap(Models.Order order)
        {
            ShowOnMapPage showOnMapPage = new ShowOnMapPage()
            {
                BindingContext = new ShowOnMapPageViewModel() { Order = order }
            };
            showOnMapPage.AddCloseButton();
            await Navigation.PushAsync(showOnMapPage);
        }

        private void RepeatOrder(Models.Order order)
        {
            var masterPage = this.Parent as TabbedPage;
            masterPage.CurrentPage = masterPage.Children[0];
            var viewModel = masterPage.CurrentPage.BindingContext as OrdersViewModel;
            if (viewModel != null)
            {
                if (masterPage.CurrentPage is BetweenOrderFormPage)
                {
                    viewModel.FromSelectedIndex = GetCityIndex(order.From);
                    viewModel.ToSelectedIndex = GetCityIndex(order.To);
                }
                else
                {
                    viewModel.From = order.From;
                    viewModel.To = order.To;
                }
                viewModel.Price = order.Price;
                viewModel.Note = order.Note;
            }
        }

        private int GetCityIndex(string cityName)
        {
            var city = Global.SettingsViewModel.Cities.FirstOrDefault(c => c.Name == cityName);
            if (city == null)
            {
                return 0;
            }
            return Global.SettingsViewModel.Cities.IndexOf(city);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            // ToolbarItem 1 is icon
            ViewModel.LoadMyOrdersCommand.Execute(null);
            ViewModel.IsInitialized = true;
        }

        public void OnMore(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            DisplayAlert("Повторить", "Повторить эту заявку?", "Да", "Нет");
        }

        public async void OnDelete(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            if (await DisplayAlert("Удаление", "Вы действительно хотите удалить эту заявку", "Да", "Нет"))
            {
                ViewModel.DeleteMyOrderCommand.Execute(mi.CommandParameter);
            }
        }
    }

    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>
    public abstract class MyOrdersPageXaml : TabChildModelBoundContentPage<OrdersViewModel>
    {
    }
}
