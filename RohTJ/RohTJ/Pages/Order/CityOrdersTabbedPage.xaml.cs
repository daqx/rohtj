﻿using RohTJ.Pages.Order;
using RohTJ.ViewModels.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RohTJ.Pages.Order
{
    public partial class CityOrdersTabbedPage : TabbedPage
    {
        bool isDriver;
        OrdersViewModel viewModel;
        public CityOrdersTabbedPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (isDriver != Global.SettingsViewModel.Client.IsDriver)
            {
                InitializeTabs();
            }
        }

        private void InitializeTabs()
        {
            Children.Clear();
            isDriver = Global.SettingsViewModel.Client.IsDriver;
            if (isDriver)
            {
                Children.Add(new DriverOrderFormPage() { BindingContext = this.viewModel });
                Children.Add(new OrdersPage() { BindingContext = this.viewModel});
            }
            else
            {
                Children.Add(new OrderFormPage() { BindingContext = this.viewModel});
                Children.Add(new FreeAutoPage() { BindingContext = this.viewModel});
            }
            Children.Add(new MyOrdersPage() { BindingContext = this.viewModel});
        }

        public CityOrdersTabbedPage(OrdersViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = viewModel;
            this.viewModel = viewModel;
            InitializeTabs();
        }


    }
}
