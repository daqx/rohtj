﻿using RohTJ.Controls;
using RohTJ.Pages.Base;
using RohTJ.ViewModels.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Plugin.Geolocator;
using System.Diagnostics;
using RohTJ.Helpers;
using RohTJ.Services;
using RohTJ.Models;
using Xamarin.Forms.GoogleMaps;
using RohTj.Models;

namespace RohTJ.Pages.Order
{
    public partial class ShowOnMapPage : ShowOnMapPageXaml
    {
        DataService dataService;

        public ShowOnMapPage()
        {
            InitializeComponent();

            ToolbarItems.Add(new ToolbarItem() { Text = "Обновить", Order = ToolbarItemOrder.Primary, Command = new Command(async () => await UpdateClientPosition()) });

            dataService = new DataService();
            //customMap.HasScrollEnabled = false;
            //customMap.HasZoomEnabled = false;
        }
        public void AddCloseButton()
        {
            ToolbarItems.Add(new ToolbarItem() { Text = "Закрыть", Order = ToolbarItemOrder.Primary, Command = new Command(async () => await Navigation.PopAsync()) });
        }

        private async Task UpdateClientPosition()
        {
            if (ViewModel?.Order?.Client != null)
            {
                GeoLocation geoLocation = await dataService.GetClientGeoLocationAsync(ViewModel.Order.Client);

                if (geoLocation == null)
                {
                    await DisplayAlert("Внимание", "Не удалось определить местоположение клиента!", "Ok");
                    return;
                }

                var position = new Position(geoLocation.Latitude, geoLocation.Longitude);
                SetPin(position, ref clientPin, ViewModel.Order.Client);
                customMap.MoveToRegion(MapSpan.FromCenterAndRadius(position, Distance.FromMiles(1.0)));
            }

            if (ViewModel?.Order?.Driver != null)
            {
                GeoLocation geoLocation = await dataService.GetClientGeoLocationAsync(ViewModel.Order.Driver);
                if (geoLocation != null)
                {
                    var position = new Position(geoLocation.Latitude, geoLocation.Longitude);
                    SetPin(position, ref driverPin, ViewModel.Order.Driver);
                }
            }
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(40.2862, 69.6288), Distance.FromMiles(1.0)));
            ViewModel.Order = await dataService.GetByIdAsync<Models.Order>(ViewModel.Order.Id);
            await UpdateClientPosition();
        }

        Pin clientPin;
        Pin driverPin;
        private void SetPin(Position position, ref Pin pin, Client client)
        {
            bool addPin = false;
            if (pin == null)
            {
                addPin = true;
                string iconName = client.IsDriver ? "wheel.png" : "pin.png";
                pin = new Pin()
                {
                    Type = PinType.Place,
                    Label = client.IsDriver ? client.Brand?.Name : "Пассажир",
                    //Address = client.PhoneNumber,
                    Icon = BitmapDescriptorFactory.FromBundle(iconName),
                };
            }
            pin.Position = position;

            if (addPin)
                customMap.Pins.Add(pin);
        }

        protected async void OnPhoneTapped(object sender, EventArgs e)
        {
            Models.Order order = ViewModel.Order;
            if (order.Status == OrderStatus.Cancelled || order.Status == OrderStatus.Completed)
            {
                await DisplayAlert("Внимание", "Заказ устарел", "Ок");
                return;
            }
            if (order?.Client != null)
            {
                ViewModel.CallToOrderCommand.Execute(order);
            }
        }
    }

    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>
    public abstract class ShowOnMapPageXaml : ModelBoundContentPage<ShowOnMapPageViewModel> { }
}
