﻿using RohTJ.Helpers;
using RohTJ.Models;
using RohTJ.Pages.Base;
using RohTJ.ViewModels.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using XLabs.Platform.Device;

namespace RohTJ.Pages.Order
{
    public partial class OrdersPage : OrdersPageXaml
    {
        public OrdersPage()
        {
            InitializeComponent();
            this.AddBalanceToolbarItem();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.LoadOrdersCommand.Execute(null);
            ViewModel.IsInitialized = true;
            await StartReloadTimer();
        }

        Timer reloadListTimer;
        /// <summary>
        /// Runs ReloadList timer
        /// </summary>
        /// <returns></returns>
        private async Task StartReloadTimer()
        {
            reloadListTimer = new Timer(TimeSpan.FromSeconds(30), () =>
            {
                ViewModel.LoadOrdersCommand.Execute(null);
            });
            await reloadListTimer.Start();
        }

        protected async void OrderItemTapped(object sender, ItemTappedEventArgs e)
        {
            Models.Order order = ((Models.Order)e.Item);
            if (order.Status != OrderStatus.New)
            {
                await DisplayAlert("Внимание", "Заказ устарел", "Ок");
                return;
            }
            if (order?.Client != null)
            {
                if (!await Global.IsBalanceEnough(this))
                    return;
                ViewModel.CallToOrderCommand.Execute(order);
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            reloadListTimer.Stop();
        }
    }

    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>
    public abstract class OrdersPageXaml : TabChildModelBoundContentPage<OrdersViewModel>
    {
    }
}
