﻿using RohTJ.Helpers;
using RohTJ.Pages.Base;
using RohTJ.ViewModels.Orders;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using XLabs.Platform.Device;

namespace RohTJ.Pages.Order
{
    public partial class FreeAutoPage : FreeAutoPageXaml
    {
        public FreeAutoPage()
        {
            InitializeComponent();
            this.AddBalanceToolbarItem();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
           
            ViewModel.LoadOrdersCommand.Execute(null);

            ViewModel.IsInitialized = true;
            await StartReloadTimer();
        }

        Timer reloadListTimer;
        /// <summary>
        /// Runs ReloadList timer
        /// </summary>
        /// <returns></returns>
        private async Task StartReloadTimer()
        {
            reloadListTimer = new Timer(TimeSpan.FromSeconds(30), () =>
            {
                ViewModel.LoadOrdersCommand.Execute(null);
            });
            await reloadListTimer.Start();
        }
        protected void OrderItemTapped(object sender, ItemTappedEventArgs e)
        {
            Models.Order order = ((Models.Order)e.Item);
            if (order?.Client != null)
            {
                var device = DependencyService.Get<IDevice>();
                if (device?.PhoneService != null)
                {
                    device.PhoneService.DialNumber(order.Client.PhoneNumber);
                }
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            reloadListTimer.Stop();
        }
    }

    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>
    public abstract class FreeAutoPageXaml : TabChildModelBoundContentPage<OrdersViewModel>
    {
    }
}
