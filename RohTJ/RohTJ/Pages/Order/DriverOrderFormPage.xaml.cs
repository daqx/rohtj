﻿using RohTJ.Pages.Base;
using RohTJ.Services;
using RohTJ.ViewModels.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RohTJ.Pages.Order
{
    public partial class DriverOrderFormPage : DriverOrderFormPageXaml
    {
        public DriverOrderFormPage()
        {
            InitializeComponent();
            uiCreateOrder.Clicked += uiCreateOrder_Clicked;
            this.AddBalanceToolbarItem();
        }
        
        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.ViewModel.From = "";
            this.ViewModel.To = "";
            this.ViewModel.Price = 0;
            this.ViewModel.Note = "";
        }

        private async void uiCreateOrder_Clicked(object sender, EventArgs e)
        {
            if (!await Global.IsBalanceEnough(this))
                return;

            if (string.IsNullOrWhiteSpace(uiFrom.Text))
            {
                await DisplayAlert("Ошибка заполнения", "Поле откуда не заполнена", "Ok");
                return;
            }

            if (string.IsNullOrWhiteSpace(uiTo.Text))
            {
                await DisplayAlert("Ошибка заполнения", "Поле куда не заполнена", "Ok");
                return;
            }

            if (string.IsNullOrWhiteSpace(uiPrice.Text))
            {
                await DisplayAlert("Ошибка заполнения", "Поле стоимость не заполнена", "Ok");
                return;
            }
            uiCreateOrder.IsEnabled = false;

            var order = await SaveOrder();
            await DisplayAlert("Создание заявки", "Заявка успешно создана", "Ok");
            uiPrice.Text = "";
            uiCreateOrder.IsEnabled = true;
        }

        async Task<Models.Order> SaveOrder()
        {
            Models.Order order = new Models.Order() { OrderType = Models.OrderType.City };
            order.DriverId = Global.SettingsViewModel.Client.Id;
            order.From = uiFrom.Text;
            order.To = uiTo.Text;
            order.Price = double.Parse(uiPrice.Text);
            order.Note = uiNote.Text;
            order.IsDriver = true;

            DataService service = new DataService();
            return await service.CreateOrderAsync(order);
        }
    }

    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>
    public abstract class DriverOrderFormPageXaml : TabChildModelBoundContentPage<OrdersViewModel>
    {
    }
}
