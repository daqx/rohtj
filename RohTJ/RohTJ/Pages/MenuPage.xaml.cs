﻿using Plugin.Geolocator;
using RohTj.Models;
using RohTJ.Helpers;
using RohTJ.Models;
using RohTJ.Services;
using RohTJ.ViewModels.Base;
using RohTJ.ViewModels.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;
using Xamarin.Forms.GoogleMaps;
using RohTJ.Pages.Order;
using RohTJ.ViewModels.Orders;
using Plugin.Share;
using Plugin.Share.Abstractions;
using RohTJ.ViewModels.Account;
using RohTJ.Pages.Account;

namespace RohTJ.Pages
{
    public partial class MenuPage : ContentPage
    {
        RootPage root;
        ClientViewModel clientViewModel;
        DataService dataService;

        public MenuPage(RootPage rootPage)
        {
            root = rootPage;
            InitializeComponent();
            clientViewModel = new ClientViewModel(Navigation)
            {
                Title = "Рох1",
                //Subtitle = "Рох2",
                Client = Global.SettingsViewModel.Client
                //Icon = "slideout.png"
            };
            BindingContext = clientViewModel;

            this.BackgroundColor = Color.White;
            InitClientMode();
            uiClientMode.Toggled += OnClientModeTapped;
            dataService = new DataService();
            // This is shadow view color, you can set a transparent color
            //this.BackgroundViewColor = Color.Transparent;
        }


        protected void OnChangeTapped(object sender, EventArgs args)
        {
            this.root.NavigateAsync(MenuType.UserProfile);
        }

        protected void OnCityTapped(object sender, EventArgs args)
        {
            this.root.NavigateAsync(MenuType.CityOrders);
        }

        protected void OnBetweenCityTapped(object sender, EventArgs args)
        {
            this.root.NavigateAsync(MenuType.BetweenCityOrders);
        }

        protected void OnTruckTapped(object sender, EventArgs args)
        {
            this.root.NavigateAsync(MenuType.TruckOrders);
        }

        protected void OnMyRatingTapped(object sender, EventArgs args)
        {
            this.root.NavigateAsync(MenuType.RatingPage);
        }

        Timer timer;
        private async Task PeriodicallySendLocation()
        {
            timer = new Timer(TimeSpan.FromSeconds(40), async () =>
            {
                await SendPosition();

                if (Global.SettingsViewModel.Client.IsDriver)
                {
                    await GetClientBusyStatus();
                    //await UpdateOrderCallState();
                }
            });

            await timer.Start();
        }

        //private async Task UpdateOrderCallState()
        //{
        //    if (!string.IsNullOrEmpty(Global.OrderCallId))
        //    {
        //        var orderCall = await dataService.GetByIdAsync<OrderCall>(Global.OrderCallId);
        //        if (orderCall?.State == OrderCallState.No)
        //        {
        //            Global.OrderCallId = null;
        //        }
        //        else if (orderCall?.State == OrderCallState.Yes)
        //        {
        //            if (orderCall.Order == null)
        //            {
        //                orderCall.Order = await dataService.GetByIdAsync<Models.Order>(orderCall.OrderId);
        //            }

        //            Global.OrderCallId = null;
        //            var page = new ShowOnMapPage()
        //            {
        //                BindingContext = new ShowOnMapPageViewModel() { Order = orderCall.Order }
        //            };
        //            await this.root.NavigateAsync(MenuType.CustomPage, page);
        //        }
        //    }
        //}

        private async Task GetClientBusyStatus()
        {
            var client = await dataService.GetByIdAsync<Client>(Global.SettingsViewModel.Client.Id);
            if (client != null && client.IsFree != Global.SettingsViewModel.Client.IsFree)
            {
                Global.SettingsViewModel.Client.IsFree = client.IsFree;
            }
        }

        private async Task SendPosition()
        {
            if (Global.SettingsViewModel.Position == null)
                Global.SettingsViewModel.Position = new Position();

            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 100; //100 is new default
                if (locator.IsGeolocationEnabled)
                {
                    var pos = await locator.GetPositionAsync(timeout: TimeSpan.FromMilliseconds( 30000));
                    var position = new Position(pos.Latitude, pos.Longitude);
                    Global.SettingsViewModel.Position = position;
                    GeoLocation geoLocation = new GeoLocation()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Latitude = position.Latitude,
                        Longitude = position.Longitude,
                        ClientId = Global.SettingsViewModel.Client.Id
                    };
                    await dataService.PostAsync<GeoLocation>(geoLocation);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        protected void OnSettingsTapped(object sender, EventArgs args)
        {
            this.root.NavigateAsync(MenuType.Settings);
        }

        protected async void OnClientModeTapped(object sender, EventArgs args)
        {
            clientViewModel.Client.IsDriver = !clientViewModel.Client.IsDriver;

            await (new DataService()).PutAsync<Client>(clientViewModel.Client);
            if (Global.SettingsViewModel.Client.IsDriver)
            {
                if (Global.SettingsViewModel.Client.BrandModel == null || Global.SettingsViewModel.Client.Brand == null 
                    || string.IsNullOrEmpty(Global.SettingsViewModel.Client.Color) || string.IsNullOrEmpty(Global.SettingsViewModel.Client.Number)) 
                {
                    var viewModel = new UserProfileViewModel() { Client = Global.SettingsViewModel.Client };
                    var userProfilePage = new UserProfilePage() { Title = "Профиль" };
                    userProfilePage.BindingContext = viewModel;
                    await this.root.Navigation.PushModalAsync(userProfilePage);
                }
                else {
                    this.root.NavigateAsync(Global.LastVisited);
                }
            }
            else { 
                this.root.NavigateAsync(Global.LastVisited);
            }
        }

        private void InitClientMode()
        {
            uiClientMode.IsToggled = clientViewModel?.Client.IsDriver ?? false;
            //uiClientIsFree.IsToggled = clientViewModel?.Client.IsFree ?? false;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            //uiClientIsFree.IsToggled = Global.SettingsViewModel.Client.IsFree;
            await SendPosition();
            await PeriodicallySendLocation();
        }

        protected void OnFeedbackTapped(object sender, EventArgs args)
        {
            var feedbackPage = new WebViewPage()
            {
                BindingContext = new ViewModels.WebViewViewModel() { Url = Global.ServiceUrl + "feedback/" + Global.SettingsViewModel.Client.PhoneNumber.Replace("+", "") }
            };
            this.root.NavigateAsync(MenuType.CustomPage, feedbackPage);
        }
        protected void OnHelpTapped(object sender, EventArgs args)
        {
            var helpPage = new WebViewPage()
            {
                BindingContext = new ViewModels.WebViewViewModel() { Url = Global.ServiceUrl + "mobile/help" }
            };
            this.root.NavigateAsync(MenuType.CustomPage, helpPage);
        }
        protected void OnInfoTapped(object sender, EventArgs args)
        {
            this.root.NavigateAsync(MenuType.Info);
        }

        protected async void OnSharaTapped(object sender, EventArgs args)
        {
            // var url = "http://www.facebook.com/sharer.php?u=roh.tj&t=Roh&src=sp";
            // var device = DependencyService.Get<IDevice>();
            var shareMessage = new ShareMessage() { Title = "Roh", Text = "Пользователи ROH знают толк в выгодных поездках! Установи ROH и начни экономить на такси до 50%!", Url = "https://roh.tj/" };
            await CrossShare.Current.Share(shareMessage);

            /*Device.OpenUri
             {
                 device.PhoneService.DialNumber(order.Client.PhoneNumber);
             }*/
            // Device.OpenUri(new Uri(url));
            /*
            var action = await DisplayActionSheet("ПОДЕЛИТСЯ?", "Отмена", null, "Twitter", "Facebook");
            Debug.WriteLine("Action: " + action);

            switch (action)
            {
                case "Twitter":
                    Device.OpenUri(new Uri("http://twitter.com/share?text=roh&url=roh.tj"));
                    break;
                case "Facebook":
                    Device.OpenUri(new Uri("http://www.facebook.com/sharer.php?u=roh.tj&t=Roh&src=sp"));
                    break;
                default:

                    break;
            }
            */
            // Device.OpenUri(new Uri("http://www.facebook.com/sharer.php?u=roh.tj&t=Roh&src=sp"));
        }
    }
}
