﻿using RohTJ.Models;
using RohTJ.Services;
using RohTJ.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XLabs.Platform.Device;

namespace RohTJ.Pages.Base
{
    /// <summary>
    /// A generically typed ContentPage that enforces the type of its BindingContext according to TViewModel.
    /// </summary>
    public abstract class ModelBoundContentPage<TViewModel> : ContentPage where TViewModel : BaseViewModel
    {
        /// <summary>
        /// Gets the generically typed ViewModel from the underlying BindingContext.
        /// </summary>
        /// <value>The generically typed ViewModel.</value>
        protected TViewModel ViewModel
        {
            get { return base.BindingContext as TViewModel; }
        }

        /// <summary>
        /// Sets the underlying BindingContext as the defined generic type.
        /// </summary>
        /// <value>The generically typed ViewModel.</value>
        /// <remarks>Enforces a generically typed BindingContext, instead of the underlying loosely object-typed BindingContext.</remarks>
        public new TViewModel BindingContext
        {
            set
            {
                base.BindingContext = value;
                base.OnPropertyChanged("BindingContext");
            }
        }
    }

    public abstract class TabChildModelBoundContentPage<TViewModel>: ModelBoundContentPage<TViewModel> where TViewModel : BaseViewModel
    {
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (Global.SettingsViewModel.Client.IsDriver)
            {
                // ToolbarItem 1 is icon
                this.UpdateFreeBusyToolbarItem(ToolbarItems[1]);
            }            
        }

        public void AddBalanceToolbarItem()
        {
            if (Global.SettingsViewModel.Client.IsDriver)
            {
                var balanceToolBarItem = new ToolbarItem() { Order = ToolbarItemOrder.Primary };
                balanceToolBarItem.SetBinding(ToolbarItem.TextProperty, new Binding("Balance") { Source = Global.SettingsViewModel.Client, StringFormat = "{0:N2}" });
                var isFreeToolBarItem = new ToolbarItem() { Order = ToolbarItemOrder.Primary };
                var isFreeTextToolBarItem = new ToolbarItem() { Order = ToolbarItemOrder.Primary };
                isFreeToolBarItem.Clicked += IsFreeToolBarItem_Clicked;
                isFreeTextToolBarItem.SetBinding(ToolbarItem.TextProperty, new Binding("IsFreeToString") { Source = Global.SettingsViewModel.Client });

                if (Global.SettingsViewModel.Client.IsFree)
                    isFreeToolBarItem.Icon = "free.png";
                else
                    isFreeToolBarItem.Icon = "busy.png";

                this.ToolbarItems.Add(isFreeTextToolBarItem);
                this.ToolbarItems.Add(isFreeToolBarItem);
                this.ToolbarItems.Add(balanceToolBarItem);
            }
            else
            {
                var CallToolBarItem = new ToolbarItem() { Order = ToolbarItemOrder.Primary };
                CallToolBarItem.Clicked += CallToolBarItem_Clicked;
                CallToolBarItem.Icon = "call.png";
                this.ToolbarItems.Add(CallToolBarItem);
            }
        }

        private void CallToolBarItem_Clicked(object sender, EventArgs e)
        {
            var device = DependencyService.Get<IDevice>();
            if (device?.PhoneService != null)
            {
                device.PhoneService.DialNumber("+99292-888-55-88");
            }
        }

        private async void IsFreeToolBarItem_Clicked(object sender, EventArgs e)
        {
            Global.SettingsViewModel.Client.IsFree = !Global.SettingsViewModel.Client.IsFree;
            var item = sender as ToolbarItem;
            if (Global.SettingsViewModel.Client.IsFree)
                item.Icon = "free.png";
            else
                item.Icon = "busy.png";
            await (new DataService()).PutAsync<Client>(Global.SettingsViewModel.Client);
        }

        public void UpdateFreeBusyToolbarItem( ToolbarItem toolbarItem)
        {
            if (Global.SettingsViewModel.Client.IsFree)
                toolbarItem.Icon = "free.png";
            else
                toolbarItem.Icon = "busy.png";
        }
    }
}
