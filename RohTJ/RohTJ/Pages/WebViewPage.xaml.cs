﻿using RohTJ.Pages.Base;
using RohTJ.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RohTJ.Pages
{
    public partial class WebViewPage : WebViewPageXaml
    {
        public WebViewPage()
        {
            InitializeComponent();
        }
    }
    public class WebViewPageXaml : ModelBoundContentPage<WebViewViewModel> { }
}
