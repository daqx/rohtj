﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RohTJ.Pages
{
    public partial class InfoPage : CarouselPage
    {

        public InfoPage()
        {
            InitializeComponent();
        }

        protected void uiOkClicked(object sender, EventArgs args)
        {
            ((RootPage)App.CurrentApp.MainPage).NavigateAsync(MenuType.CityOrders);
        }
    }
}
