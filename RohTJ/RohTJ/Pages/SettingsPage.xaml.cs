﻿using RohTJ.Pages.Base;
using RohTJ.Services;
using RohTJ.ViewModels.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RohTJ.Pages
{
    public partial class SettingsPage : SettingsPageXaml
    {
        public SettingsPage()
        {
            InitializeComponent();
            uiRules.Command = new Command(async () => await Navigation.PushAsync(new WebViewPage()
            {
                BindingContext = new ViewModels.WebViewViewModel() { Url = Global.ServiceUrl + "mobile/rules" }
            }));

            uiOferta.Command = new Command(async () => await Navigation.PushAsync(new WebViewPage()
            {
                BindingContext = new ViewModels.WebViewViewModel() { Url = Global.ServiceUrl + "mobile/oferta" }
            }));
            uiLogOut.Command = new Command(async () => await LogOut());
            uiIsDisplayOn.OnChanged += UiIsDisplayOn_OnChanged;
        }

        private void UiIsDisplayOn_OnChanged(object sender, ToggledEventArgs e)
        {
            ViewModel.IsDisplayOn = !ViewModel.IsDisplayOn;
            IScreenService screenService = DependencyService.Get<IScreenService>();
            screenService.KeepScreenOn(ViewModel.IsDisplayOn);
        }

        private async Task LogOut()
        {
            if (await DisplayAlert("Внимание", "Вы действительно хотите выйти?", "Да", "Нет"))
            {
                ViewModel.LogOutCommand.Execute(null);
            }
        }
    }

    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>
    public abstract class SettingsPageXaml : ModelBoundContentPage<SettingsViewModel>
    {
    }
}
