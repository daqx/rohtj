﻿using Plugin.Connectivity;
using Plugin.Geolocator;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using RohTJ.Models;
using RohTJ.Pages.Base;
using RohTJ.Services;
using RohTJ.ViewModels.Account;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RohTJ.Pages.Account
{
    public partial class SplashPage : SplashPageXaml
    {
        private DataService service;

        public SplashPage()
        {
            InitializeComponent();
            service = new DataService();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var timer = DateTime.Now;
            var totalTimer = DateTime.Now;
            while (!await App.IsConnectedToInternet())
            {
                await DisplayAlert("Ошибка связи", "Установите подключение к интернету, и перезапустите приложение", "Ok");
            }
            var time = DateTime.Now - timer;
            Debug.WriteLine($"+++++++++++++ Oshibka svyazi -{time.ToString()}");
            timer = DateTime.Now;
            /// commented code was taking 8 seconds, it should be optimized
            while (!await CrossConnectivity.Current.IsRemoteReachable(Global.ServiceHost, Global.ServicePort))
            {
                await DisplayAlert("Ошибка подключения", "Не удалось установить связь с сервером, проверьте настройки ", "Ok");
            }
            time = DateTime.Now - timer;
            Debug.WriteLine($"+++++++++++++ Oshibka podklyucheniya -{time.ToString()}");
            timer = DateTime.Now;

            var geoLocationSettings = DependencyService.Get<IGeoLocationSettings>();
			//TODO: для ios создали if (geoLocationSettings != null) 
			if (geoLocationSettings != null)
            {
                while (!geoLocationSettings.IsEnabled())
                {
                    var result = await DisplayActionSheet("Не обходимо включить GPS.", null, null, "Открыть настройки", "Попробовать снова");
                    if (result == "Открыть настройки")
                    {
                        geoLocationSettings.OpenGeoLocationSettings();
                    }
                }
            }

            //Global.SettingsViewModel.RestoreState(Current.Properties);
            if (!string.IsNullOrEmpty(Global.SettingsViewModel.ClientId) && !string.IsNullOrEmpty(Global.SettingsViewModel.RegistrationCode))
            {
                Global.SettingsViewModel.IsAuthenticated = Global.SettingsViewModel.Client != null;
                var client = await service.GetClientAsync(Global.SettingsViewModel.ClientId);
                if (client != null)
                {
                    var isRegistrationCodeCorrect = await service.CheckRegistrationCodeAsync(Global.SettingsViewModel.RegistrationCode, client);
                    if (isRegistrationCodeCorrect)
                    {
                        Global.SettingsViewModel.IsAuthenticated = client != null;
                        Global.SettingsViewModel.Client = client;
                        await CheckIfAccountBlocked(client);
                    }
                }
            }
            time = DateTime.Now - timer;
            Debug.WriteLine($"+++++++++++++ GetClient -{time.ToString()}");
            timer = DateTime.Now;

            Global.SettingsViewModel.Brands = await service.GetAsync<Brand>();
            Global.SettingsViewModel.BrandModels = await service.GetAsync<BrandModel>();
            Global.SettingsViewModel.Regions = await service.GetAsync<Region>();
            Global.SettingsViewModel.Cities = await service.GetAsync<City>();
            time = DateTime.Now - timer;
            Debug.WriteLine($"+++++++++++++ Brands.... -{time.ToString()}");
            var total = DateTime.Now - totalTimer;
            Debug.WriteLine($"+++++++++++++ Total time -{total.ToString()}");
            App.LoginIfNotAutheticated();

        }

        private async Task CheckIfAccountBlocked(Client client)
        {
            while (client.Blocked)
            {
                await DisplayAlert("Аккаунт заблокирован", "Ваш аккаунт заблокирован!", "Ok");
            }
        }
    }
    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>
    public abstract class SplashPageXaml : ModelBoundContentPage<LoginPageViewModel> { }
}
