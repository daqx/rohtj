﻿using RohTJ.Models;
using RohTJ.Pages.Base;
using RohTJ.Services;
using RohTJ.ViewModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RohTJ.Pages.Account
{
    public partial class LoginPage : LoginPageXaml
    {
        public LoginPage()
        {
            InitializeComponent();
        }
        Client client = new Client();
        protected async void EnterClicked(object sender, EventArgs args)
        {
            if (uiPhoneNumber.Text?.Trim()?.Length != 9)
            {
                uiPhoneNumberErrorMessage.Text = "Вы ввели неправельный номер.";
                return;
            }
            else
                uiPhoneNumberErrorMessage.Text = "";

            client.PhoneNumber = "+992" + uiPhoneNumber.Text;
            client.IsFree = true;
            var service = new DataService();
            uiEnter.IsEnabled = false;
            client = await service.ClientRegistrationAsync(client);

            if (client.Blocked)
            {
                uiPhoneNumberErrorMessage.Text = "Ваш аккаунт заблокирован.";
                return;
            }

            uiPhonePanel.IsVisible = false;
            uiRegistrationCodePanel.IsVisible = true;
        }

        protected async void ConfirmationClicked(object sender, EventArgs args)
        {
            if (uiRegistrationCode.Text?.Trim()?.Length != 4)
            {
                uiConfirmationErrorMessage.Text = "Вы ввели неправельный номер.";
                return;
            }
            client.PhoneNumber = "+992" + uiPhoneNumber.Text;
            var service = new DataService();
            var isCorrectCode = await service.CheckRegistrationCodeAsync(uiRegistrationCode.Text, client);
            if (isCorrectCode)
            {
                uiRegistrationCodePanel.IsVisible = false;
                uiCategorySelectionPanel.IsVisible = true;
                Global.SettingsViewModel.Client = client;
                Global.SettingsViewModel.RegistrationCode = uiRegistrationCode.Text;
                App.SaveSettings();
            }
            else
            {
                uiConfirmationErrorMessage.Text = "Извение. Вы ввели неправельный номер.";
                uiConfirmation.Text = "Повтор";
            }
        }

        protected void DriverClicked(object sender, EventArgs args)
        {
            client.IsDriver = true;
            App.GotoUserProfilePage(client);
        }

        protected void PassengerClicked(object sender, EventArgs args)
        {
            client.IsDriver = false;
            App.GotoUserProfilePage(client);
        }
    }
    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>
    public abstract class LoginPageXaml : ModelBoundContentPage<LoginPageViewModel> { }
}
