﻿using FFImageLoading.Cache;
using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using RohTJ.Pages.Base;
using RohTJ.Services;
using RohTJ.ViewModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RohTJ.Pages.Account
{
    public partial class CameraPage : CameraPageXaml
    {
        ///int degree = 0;
        public CameraPage()
        {
            InitializeComponent();
        }

        async void uiUsePicture_Clicked(object sender, EventArgs e)
        {
            var image = await uiImage.GetImageAsJpgAsync();
            await (new DataService()).PostClientsPhotoAsync(image, Global.SettingsViewModel.Client);
            await CachedImage.InvalidateCache(Global.SettingsViewModel.Client.PhotoPath, CacheType.All, removeSimilar: true);
            await Navigation.PopModalAsync();
        }

        void uiRotatePicture_Clicked(object sender, EventArgs e)
        {
            ViewModel.RotationDegree += 90;
            uiImage.Transformations = new List<ITransformation> { new RotateTransformation(ViewModel.RotationDegree) };
        }
    }
    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>
    public abstract class CameraPageXaml : ModelBoundContentPage<CameraPageViewModel> { }
}
