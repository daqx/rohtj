﻿using RohTJ.Pages.Base;
using RohTJ.Services;
using RohTJ.ViewModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using RohTJ.Models;

namespace RohTJ.Pages.Account
{
    public partial class UserProfilePage : UserProfilePageXaml
    {
        public UserProfilePage()
        {
            InitializeComponent();
            ToolbarItems.Add(new ToolbarItem() { Text = "Сохранить", Order = ToolbarItemOrder.Primary, Command = new Command(async () => { await Save(); }) });
            uiGender.Items.Add("Мужской");
            uiGender.Items.Add("Женский");
            uiBrand.SelectedIndexChanged += UiBrand_SelectedIndexChanged;
        }

        private void UiBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            uiBrandModels.Items.Clear();
            foreach (var brandModel in ViewModel.BrandModels)
            {
                uiBrandModels.Items.Add(brandModel.Name);
            }            
        }
        private async void OnAddPhotoTapped(object sender, EventArgs e)
        {
            var cameraPage = new CameraPage() { BindingContext = new CameraPageViewModel() };
            await Navigation.PushModalAsync(cameraPage);
        }
        private async void SaveClicked(object sender, EventArgs e)
        {
            await Save();
        }

        private async Task Save()
        {
            DataService service = new DataService();
            var item = uiCity.SelectedItem;
            if (uiCity.SelectedIndex > -1 )
            {
                ViewModel.Client.CityId = ViewModel.Cities[uiCity.SelectedIndex].Id;
            }

            if (Global.SettingsViewModel.Client.IsDriver) { 
                if (uiBrand.SelectedIndex > -1)
                {
                    ViewModel.Client.BrandId = ViewModel.Brands[uiBrand.SelectedIndex].Id;
                }

                if (uiBrandModels.SelectedIndex > -1)
                {
                    ViewModel.Client.BrandModelId = ViewModel.BrandModels[uiBrandModels.SelectedIndex].Id;
                }

                if (uiBrand.SelectedIndex < 0)
                {
                    await DisplayAlert("Ошибка заполнения", "Поле марка не заполнена", "Ok");
                    return;
                }

                if (uiBrandModels.SelectedIndex < 0)
                {
                    await DisplayAlert("Ошибка заполнения", "Поле Модель не заполнена", "Ok");
                    return;
                }
                if (string.IsNullOrEmpty(ViewModel.Client.Color))
                {
                    await DisplayAlert("Ошибка заполнения", "Поле цвет не заполнена", "Ok");
                    return;
                }

                if (string.IsNullOrEmpty(ViewModel.Client.Number))
                {
                    await DisplayAlert("Ошибка заполнения", "Поле гос номер не заполнена", "Ok");
                    return;
                }
            }
            await service.PutAsync(ViewModel.Client);
            Global.SettingsViewModel.IsAuthenticated = true;
            App.GoToRoot();
        }

        protected override void OnAppearing()
        {  
            base.OnAppearing();
            if (ViewModel.Client != null && ViewModel.Client.BirthDay == null)
            {
                ViewModel.Client.BirthDay = new DateTime(2000, 1, 1);
            }

            if (!string.IsNullOrEmpty(ViewModel.Client.CityId ))
            {
                var city = ViewModel.Cities.FirstOrDefault(c => c.Id == ViewModel.Client.CityId);
                uiCity.SelectedIndex = ViewModel.Cities.IndexOf(city);
            }

            if (!string.IsNullOrEmpty(ViewModel.Client.BrandId))
            {
                var brand = ViewModel.Brands.FirstOrDefault(c => c.Id == ViewModel.Client.BrandId);
                uiBrand.SelectedIndex = ViewModel.Brands.IndexOf(brand);
            }

            if (!string.IsNullOrEmpty(ViewModel.Client.BrandModelId))
            {
                var brandModel = ViewModel.BrandModels.FirstOrDefault(c => c.Id == ViewModel.Client.BrandModelId);
                uiBrandModels.SelectedIndex = ViewModel.BrandModels.IndexOf(brandModel);
            }         
            
        }
                
    }

    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>
    public abstract class UserProfilePageXaml : ModelBoundContentPage<UserProfileViewModel> { }
}