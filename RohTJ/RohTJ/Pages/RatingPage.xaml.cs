﻿using RohTJ.Pages.Base;
using RohTJ.Services;
using RohTJ.ViewModels;
using RohTJ.ViewModels.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RohTJ.Pages
{
    public partial class RatingPage : RatingPageXaml
    {
        public RatingPage()
        {
            InitializeComponent();

        }


        /*
         private static async void IsFreeToolBarItem_Clicked(object sender, EventArgs e)
        {
            Global.SettingsViewModel.Client.IsFree = !Global.SettingsViewModel.Client.IsFree;
            var item = sender as ToolbarItem;
            if (Global.SettingsViewModel.Client.IsFree)
                item.Icon = "free.png";
            else
                item.Icon = "busy.png";
            await (new DataService()).PutAsync<Client>(Global.SettingsViewModel.Client);
        }
        */
    }

    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>1
    public abstract class RatingPageXaml : ModelBoundContentPage<RatingViewModel>
    {
    }
}
