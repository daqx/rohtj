﻿//using FormsToolkit;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RohTJ.Helpers;
using RohTJ.Models;
using RohTJ.Pages.Account;
using RohTJ.Pages.Order;
using RohTJ.Services;
using RohTJ.Statics;
using RohTJ.ViewModels;
using RohTJ.ViewModels.Account;
using RohTJ.ViewModels.Base;
using RohTJ.ViewModels.Orders;
using RohTJ.ViewModels.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RohTJ.Pages
{
    public class RootPage : MasterDetailPage
    {
        DeepLinkPage deepLinkPage;
        bool isRunning = false;
        Dictionary<string, NavigationPage> Pages { get; set; }

        public SettingsViewModel SettingsViewModel { get; set; }

        DataService dataService = null;

        public RootPage()
        {
            dataService = new DataService();
            Pages = new Dictionary<string, NavigationPage>();
            Master = new MenuPage(this);
            BindingContext = new BaseViewModel(Navigation)
            {
                Title = "Рох3",
                Icon = "slideout.png"
            };
            //setup home page
            NavigateAsync(MenuType.CityOrders);
            IScreenService screenService = DependencyService.Get<IScreenService>();
            //TODO : для ios закрыл screenService после надо устранить
            //screenService.KeepScreenOn(Global.SettingsViewModel.IsDisplayOn);

            FormsToolkit.MessagingService.Current.Subscribe<DeepLinkPage>("DeepLinkPage", async (m, p) =>
            {
                deepLinkPage = p;

                if (isRunning)
                    await GoToDeepLink();
            });
        }

        void SetDetailIfNull(Page page)
        {
            if (Detail == null && page != null)
                Detail = page;
        }

        public void NavigateAsync(MenuType id, Page customPage = null)
        {
            Page newPage;
            Page rohCustomPage = null;
            var key = GetMenuTypeKey(id);
            if (!Pages.ContainsKey(key))
            {
                var isDriver = Global.SettingsViewModel.Client.IsDriver;

                switch (id)
                {
                    case MenuType.CityOrders:
                        var orderTabPage = new CityOrdersTabbedPage(new OrdersViewModel() { OrderType = Models.OrderType.City })
                        {
                            Title = "Город",
                        };

                        var page = new RohNavigationPage(orderTabPage);
                        SetDetailIfNull(page);
                        Pages.Add(key, page);
                        Global.LastVisited = id;
                        break;

                    case MenuType.BetweenCityOrders:
                        var betweenOrderTabPage = new BetweenCityOrdersTabbedPage(new OrdersViewModel() { OrderType = Models.OrderType.BetweenCity })
                        {
                            Title = "Меж.город",
                        };

                        var betweenPage = new RohNavigationPage(betweenOrderTabPage);
                        SetDetailIfNull(betweenPage);
                        Pages.Add(key, betweenPage);

                        Global.LastVisited = id;

                        break;

                    case MenuType.TruckOrders:
                        var truckOrderTabPage = new TruckOrdersTabbedPage(new OrdersViewModel() { OrderType = Models.OrderType.Truck })
                        {
                            Title = "Грузовые",
                        };

                        var truckPage = new RohNavigationPage(truckOrderTabPage);
                        SetDetailIfNull(truckPage);
                        Pages.Add(key, truckPage);
                        Global.LastVisited = id;

                        break;
                    case MenuType.UserProfile:
                        var userProfilePage = new UserProfilePage()
                        {
                            Title = "Профиль",
                            BindingContext = new UserProfileViewModel() { Client = Global.SettingsViewModel.Client, Navigation = Navigation }
                        };

                        var userPage = new RohNavigationPage(userProfilePage);
                        SetDetailIfNull(userPage);
                        Pages.Add(key, userPage);
                        break;
                    case MenuType.Settings:
                        SettingsViewModel.Navigation = Navigation;
                        page = new RohNavigationPage(new SettingsPage
                        {
                            Title = "Настройки",
                            //Icon = new FileImageSource { File = "about.png" },
                            BindingContext = SettingsViewModel
                        });
                        SetDetailIfNull(page);
                        Pages.Add(key, page);
                        break;

                    case MenuType.Help:
                        page = new RohNavigationPage(new HelpPage
                        {
                            Title = "Помощь",
                            //Icon = new FileImageSource { File = "about.png" },
                            BindingContext = new ViewModels.HelpViewModel() { Navigation = this.Navigation }
                        });
                        SetDetailIfNull(page);
                        Pages.Add(key, page);
                        break;
                    case MenuType.Info:
                        page = new RohNavigationPage(new InfoPage
                        {
                            //Title = "Инфо",
                            //Icon = new FileImageSource { File = "about.png" },
                            BindingContext = new ViewModels.HelpViewModel() { Navigation = this.Navigation }
                        });
                        SetDetailIfNull(page);
                        Pages.Add(key, page);
                        break;

                    case MenuType.CustomPage:
                        if (customPage != null)
                        {
                            rohCustomPage = new RohNavigationPage(customPage);
                            SetDetailIfNull(rohCustomPage);
                        }
                        break;
                    case MenuType.RatingPage:
                        page = new RohNavigationPage(new RatingPage
                        {
                            Title = "Мой рейтинг",
                            //Icon = new FileImageSource { File = "about.png" },
                            BindingContext = new ViewModels.RatingViewModel() { Navigation = this.Navigation }
                        });
                        SetDetailIfNull(page);
                        Pages.Add(key, page);
                        break;
                }
            }

            if (id == MenuType.CustomPage)
                newPage = rohCustomPage;
            else
                newPage = Pages[key];

            if (newPage == null)
                return;

            Detail = newPage;

            if (Device.Idiom != TargetIdiom.Tablet)
                IsPresented = false;
        }

        /// <summary>
        /// Some menu pages can be opened in two modes: IsDriver == true and false
        /// for them IsDriver is concatinated to MenuType
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private string GetMenuTypeKey(MenuType id)
        {
            return id.ToString() + Global.SettingsViewModel.Client.IsDriver;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            isRunning = true;
            await RegisterForPush();
            await GoToDeepLink();
        }

        async Task GoToDeepLink()
        {
            if (deepLinkPage == null)
            {
                if (string.IsNullOrEmpty(RohTJ.Helpers.Settings.PushParameter))
                {
                    return;
                }
                else
                {
                    var paramObject = JsonConvert.DeserializeObject<PushParameter>(RohTJ.Helpers.Settings.PushParameter);
                    deepLinkPage = new DeepLinkPage() { Id = paramObject.Id };
                    deepLinkPage.Page = deepLinkPage.GetAppPage(paramObject.Code);
                }
            }

            var _page = deepLinkPage.Page;
            var id = deepLinkPage.Id;
            deepLinkPage = null;
            Settings.PushParameter = null;

            switch (_page)
            {
                case AppPage.Review:
                    await DidDriverTakeToDestination(id);
                    break;
                case AppPage.ClientAccepted:
                    await ClientAcceptedDeel(id);
                    break;
            }
        }

        private async Task ClientAcceptedDeel(string id)
        {
            DataService dataService = new DataService();
            var orderCall = await dataService.GetByIdAsync<OrderCall>(id);
            if (orderCall?.State == OrderCallState.Yes)
            {
                if (orderCall.Order == null)
                {
                    orderCall.Order = await dataService.GetByIdAsync<Models.Order>(orderCall.OrderId);
                }

                ShowClientOnMap(orderCall);
            }
        }

        private void ShowClientOnMap(OrderCall orderCall)
        {
            var page = new ShowOnMapPage()
            {
                BindingContext = new ShowOnMapPageViewModel() { Order = orderCall.Order }
            };
            Device.BeginInvokeOnMainThread(() => (Detail as RohNavigationPage).PushAsync(page));
        }

        private async Task DidDriverTakeToDestination(string id)
        {
            Guid orderGuid;
            if (Guid.TryParse(id, out orderGuid))
            {
                var order = await dataService.GetByIdAsync<Models.Order>(orderGuid.ToString());

                var result = await DisplayAlert("Водитель " + order?.Driver?.FirstName + " вас подвез?",
                    "Машина: " + order?.Driver?.Brand?.Name + " " + order?.Driver?.BrandModel?.Name + ". Номер: " + order?.Driver?.Number,
                    "Да", "Нет");
                if (result)
                {
                    NavigateAsync(MenuType.CustomPage, new RateDriverPage()
                    {
                        BindingContext = new RateDriverViewModel() { Order = order }
                    });
                }
            }
        }

        private async Task RegisterForPush()
        {
            if (!string.IsNullOrEmpty(RohTJ.Helpers.Settings.PnsHandle)
                //&& string.IsNullOrEmpty(RohTJ.Helpers.Settings.HubRegistrationId)
                )
            {
                var phoneNumber = Global.SettingsViewModel.Client.PhoneNumber.Substring(1);
                var tags = new string[] { phoneNumber };
                var dataService = new DataService();

                var registrationId = RohTJ.Helpers.Settings.PnsHandle;
                var hubRegistrationId = await dataService.PostRegisterAsync(registrationId);
                Helpers.Settings.HubRegistrationId = hubRegistrationId;

                JObject deviceInfo = new JObject();
                deviceInfo.Add("Platform", "gcm");
                deviceInfo.Add("Handle", registrationId);
                deviceInfo.Add("Tags", new JArray(tags));

                await dataService.PutRegisterAsync(hubRegistrationId, deviceInfo);
            }
        }
    }

    public class RootTabPage : TabbedPage
    {
        
        public RootTabPage()
        {
			Children.Add(new RohNavigationPage(new OrdersPage
			{
				Title = "Рох",
				//Icon = new FileImageSource { File = "sales.png" }
			})
			{
				Title = "Такси",
				//Icon = new FileImageSource { File = "sales.png" }
			});
        }



        /*protected override void OnCurrentPageChanged()
        {
            base.OnCurrentPageChanged();
            this.Title = this.CurrentPage.Title;
        }*/
    }

    public class RohNavigationPage : NavigationPage
    {
        public RohNavigationPage(Page root)
            : base(root)
        {
            Init();
        }

        public RohNavigationPage()
        {
            Init();
        }

        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            return true;
        }

        void Init()
        {

            if (Global.SettingsViewModel.Client.IsDriver)
            {
                BarBackgroundColor = Palette._014;
            }
            else
            {
                BarBackgroundColor = Palette._001;
            }
            BarTextColor = Color.White;
        }
    }

    public enum MenuType
    {
        CityOrders,
        BetweenCityOrders,
        TruckOrders,
        Settings,
        RatingPage,
        UserProfile,
        Help,
        Info,
        ShowOnMap,
        CustomPage
    }
}
