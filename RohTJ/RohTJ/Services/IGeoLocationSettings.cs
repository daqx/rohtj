﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohTJ.Services
{
    public interface IGeoLocationSettings
    {
        bool IsEnabled();
        void OpenGeoLocationSettings();
    }
}
