﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RohTj.Models;
using RohTJ.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RohTJ.Services
{
    public class DataService : IDataService
    {
        HttpClient httpClient;
        string ServiceUrl { get { return Global.ServiceUrl + "api/"; } }

        public DataService()
        {
            var userName = "dunusov@gmail.com$$" + Global.SettingsViewModel.ClientId;
            var authData = string.Format("{0}:{1}", userName, "Dilovar123!");
            var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

            httpClient = new HttpClient();
            httpClient.MaxResponseContentBufferSize = 256000;
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
        }

        public async Task<List<T>> GetAsync<T>()
        {
            var Items = new List<T>();
            var uri = ServiceUrl + typeof(T).Name;
            try
            {
                var response = await httpClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Items = JsonConvert.DeserializeObject<List<T>>(content);
                }
            }
            catch (NullReferenceException ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Items;
        }

        public async Task<T> GetByIdAsync<T>(string id)
        {
            var uri = ServiceUrl + typeof(T).Name + "/" + id;
            try
            {
                var response = await httpClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(content);
                }
            }
            catch (NullReferenceException ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return default(T);
        }

        internal async Task<T> PostAsync<T>(T entity)
        {
            var uri = ServiceUrl + typeof(T).Name;

            try
            {
                string postBody = JsonConvert.SerializeObject(entity, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await httpClient.PostAsync(uri, new StringContent(postBody, Encoding.UTF8, "application/json"));
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    entity = JsonConvert.DeserializeObject<T>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return entity;
        }
        internal async Task<T> DeleteAsync<T>(T entity) where T : BaseModel
        {
            var uri = ServiceUrl + typeof(T).Name + "/" + entity.Id;

            try
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await httpClient.DeleteAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    entity = JsonConvert.DeserializeObject<T>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return entity;
        }
        internal async Task PutAsync<T>(T model) where T : BaseModel
        {
            var uri = ServiceUrl + typeof(T).Name + "/" + model.Id;

            try
            {
                string postBody = JsonConvert.SerializeObject(model, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await httpClient.PutAsync(uri, new StringContent(postBody, Encoding.UTF8, "application/json"));
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    model = JsonConvert.DeserializeObject<T>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
        }

        internal async Task<OrderCall> GetNewCall(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return null;
            }
            OrderCall orderCall = null;
            var uri = ServiceUrl + "OrderCall/hasclientnewcall/" + id;
            try
            {
                var response = await httpClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    orderCall = JsonConvert.DeserializeObject<OrderCall>(content);
                }
            }
            catch (NullReferenceException ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return orderCall;
        }

        internal async Task<OrderCall> GetNewCallByPhone(string orderId, string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
            {
                return null;
            }
            OrderCall orderCall = null;
            var uri = ServiceUrl + $"OrderCall/hasclientnewcallbyphone/" + orderId + "/" + phoneNumber;
            try
            {
                var response = await httpClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    orderCall = JsonConvert.DeserializeObject<OrderCall>(content);
                }
            }
            catch (NullReferenceException ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return orderCall;
        }

        internal async Task PostClientsPhotoAsync(byte[] image, Client client)
        {
            var uri = ServiceUrl + "client/photo";

            try
            {
                using (var content = new MultipartFormDataContent())
                {
                    content.Add(new StreamContent(new MemoryStream(image)), "file", client.Id + ".jpg");

                    using (
                       var message =
                           await httpClient.PostAsync(uri, content))
                    {
                        var input = await message.Content.ReadAsStringAsync();
                        client.UploadedPhoto = true;
                        var a = !string.IsNullOrWhiteSpace(input) ? Regex.Match(input, @"http://\w*\.directupload\.net/images/\d*/\w*\.[a-z]{3}").Value : null;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
        }

        internal async Task<Client> GetClientAsync(string clientId)
        {
            var uri = ServiceUrl + "Client/" + clientId;
            Client client = null;
            try
            {
                var response = await httpClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    client = JsonConvert.DeserializeObject<Client>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return client;
        }

        internal async Task<Client> ClientRegistrationAsync(Client client)
        {
            var uri = ServiceUrl + "Client/";

            try
            {
                string postBody = JsonConvert.SerializeObject(client, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await httpClient.PostAsync(uri, new StringContent(postBody, Encoding.UTF8, "application/json"));
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    client = JsonConvert.DeserializeObject<Client>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return client;
        }

        internal async Task<bool> CheckRegistrationCodeAsync(string registrationCode, Client client)
        {
            var uri = ServiceUrl + "RegistrationCode/ByClient/" + client.Id;
            bool isCorrectCode = false;
            try
            {
                string postBody = JsonConvert.SerializeObject(client, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await httpClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    RegistrationCode _registrationCode = JsonConvert.DeserializeObject<RegistrationCode>(content);
                    if (_registrationCode.Code == registrationCode)
                    {
                        isCorrectCode = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return isCorrectCode;
        }

        #region Order
        internal async Task<Order> CreateOrderAsync(Order order)
        {
            var uri = ServiceUrl + "Order/";

            try
            {
                string postBody = JsonConvert.SerializeObject(order, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await httpClient.PostAsync(uri, new StringContent(postBody, Encoding.UTF8, "application/json"));
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    order = JsonConvert.DeserializeObject<Order>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return order;
        }

        /// <summary>
        /// Returns all client's orders
        /// </summary>
        /// <param name="client"></param>
        /// <param name="orderType"></param>
        /// <returns></returns>
        public async Task<List<Order>> GetMyOrdersAsync(Client client, OrderType orderType)
        {
            var Items = new List<Order>();

            var uri = ServiceUrl + "/order/mybyclient/" + client.Id + "/" + (int)orderType;

            try
            {
                var response = await httpClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Items = JsonConvert.DeserializeObject<List<Order>>(content);
                }
            }
            catch (NullReferenceException ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Items;
        }

        /// <summary>
        /// Returns passangers orders for driver and drivers orders for passanger
        /// </summary>
        /// <param name="client"></param>
        /// <param name="orderType"></param>
        /// <returns></returns>
        public async Task<List<Order>> GetOrdersAsync(Client client, OrderType orderType)
        {
            var Items = new List<Order>();

            var uri = ServiceUrl + "order/byclient/" + client.Id + "/" + (int)orderType;

            try
            {
                var response = await httpClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Items = JsonConvert.DeserializeObject<List<Order>>(content);
                }
            }
            catch (NullReferenceException ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Items;
        }

        #endregion

        #region OrderCall
        public async Task NotifyDriverAboutOrderCall(OrderCall orderCall)
        {
            var uri = ServiceUrl + "/ordercall/notifydriver/" + orderCall.Id;

            try
            {
                var response = await httpClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                }
            }
            catch (NullReferenceException ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
        }
        #endregion

        #region GeoLocation
        /// <summary>
        /// Returns nearest clients GeoLocations
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public async Task<List<GeoLocation>> GetNearBy(Client client)
        {
            var Items = new List<GeoLocation>();

            var uri = ServiceUrl + "/geolocation/near/" + client.Id;

            try
            {
                var response = await httpClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Items = JsonConvert.DeserializeObject<List<GeoLocation>>(content);
                }
            }
            catch (NullReferenceException ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Items;
        }

        /// <summary>
        /// Return last GeoLocation of client
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public async Task<GeoLocation> GetClientGeoLocationAsync(Client client)
        {
            GeoLocation geoLocation = new GeoLocation();

            var uri = ServiceUrl + "/geolocation/byclient/" + client.Id;

            try
            {
                var response = await httpClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    geoLocation = JsonConvert.DeserializeObject<GeoLocation>(content);
                }
            }
            catch (NullReferenceException ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return geoLocation;
        }
        #endregion

        public async Task<string> PostRegisterAsync(string handle)
        {
            var uri = ServiceUrl + "register";
            string result = null;
            try
            {
                string postBody = JsonConvert.SerializeObject(handle, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await httpClient.PostAsync(uri, new StringContent(postBody, Encoding.UTF8, "application/json"));
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return result;
        }

        public async Task PutRegisterAsync(string hubRegistration, JObject deviceInfo)
        {
            var uri = ServiceUrl + "register/" + hubRegistration;

            try
            {
                string postBody = JsonConvert.SerializeObject(deviceInfo, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await httpClient.PutAsync(uri, new StringContent(postBody, Encoding.UTF8, "application/json"));
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    //model = JsonConvert.DeserializeObject(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
        }
    }
}
