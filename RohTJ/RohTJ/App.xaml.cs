﻿using Plugin.Connectivity;
using RohTJ.Models;
using RohTJ.Pages;
using RohTJ.Pages.Account;
using RohTJ.Services;
using RohTJ.ViewModels.Account;
using RohTJ.ViewModels.Settings;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RohTJ
{
    public partial class App : Application
    {
        static Application app;
        public static double ScreenHeight;
        public static double ScreenWidth;

        public static Application CurrentApp
        {
            get { return app; }
        }

        public App()
        {
            InitializeComponent();
            app = this;
            Global.SettingsViewModel = new SettingsViewModel();
            MainPage = new Pages.Account.SplashPage(); //new NavigationPage( new Pages.MainPage());

        }

        public static void GoToRoot()
        {
            /*if (Device.RuntimePlatform == Device.iOS)
            {
                CurrentApp.MainPage = new RootPage();
            }
            else
            {*/
                try
                {
                    CurrentApp.MainPage = new RootPage();
                }
                catch (NullReferenceException ex)
                {
                    Debug.WriteLine(@"				ERROR {0}", ex.Message);
                }

                ((RootPage)CurrentApp.MainPage).SettingsViewModel = Global.SettingsViewModel;
            //}
        }

        public static void GotoLoginPage()
        {
            CurrentApp.MainPage = new LoginPage();
        }

        public static void GotoUserProfilePage(Client client)
        {
            //client.BirthDay = DateTime.Now;
            //client.City = Global.SettingsViewModel.Cities.First();
            //client.Brand = Global.SettingsViewModel.Brands.First();
            //client.BrandModel = Global.SettingsViewModel.BrandModels.First(b => b.BrandId == client.Brand.Id);

            var viewModel = new UserProfileViewModel() { Client = client };
            var userProfilePage = new UserProfilePage() { Title = "Профиль" };
            userProfilePage.BindingContext = viewModel;
            CurrentApp.MainPage = userProfilePage;
        }

        protected override void OnStart()
        {
        }

        internal static void LoginIfNotAutheticated()
        {
            // If the App.IsAuthenticated property is false, modally present the SplashPage.
            if (!Global.SettingsViewModel.IsAuthenticated)
            {
                CurrentApp.MainPage = new LoginPage();
            }
            else
            {
                GoToRoot();
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            //SaveSettings();
        }

        public static void SaveSettings()
        {
            Global.SettingsViewModel?.SaveState(Current.Properties);
        }

        internal async static Task<bool> IsConnectedToInternet()
        {
            return CrossConnectivity.Current.IsConnected || await CrossConnectivity.Current.IsReachable("microsoft.com");
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
