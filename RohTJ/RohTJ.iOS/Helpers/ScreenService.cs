﻿using System;
using RohTJ.Services;
using UIKit;
using Xamarin.Forms;

namespace RohTJ.iOS.Helpers
{
    public class ScreenService : IScreenService
    {
        /// <summary>
        /// While setting true to turnOn keeps screen On
        /// </summary>
        /// <param name="turnOn"></param>
        public void KeepScreenOn(bool turnOn)
        {
            UIApplication.SharedApplication.IdleTimerDisabled = true;

            /*
             var activity = Xamarin.Forms.Forms.Context as Activity;
            if (activity != null)
            {
                if (turnOn)
                    activity.Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);
                else
                    activity.Window.ClearFlags(WindowManagerFlags.KeepScreenOn);
            }
            */
        }
    }
}

