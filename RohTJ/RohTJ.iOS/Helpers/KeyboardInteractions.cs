﻿using System;
using RohTJ.Services;
using UIKit;
using Xamarin.Forms;

namespace RohTJ.iOS.Helpers
{
    public class KeyboardInteractions : IKeyboardInteractions
    {
		public void HideKeyboard()
		{
			UIApplication.SharedApplication.KeyWindow.EndEditing(true);
		}
    }
}

