﻿using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Helpers
{
    public static class Extensions
    {
        internal static IEnumerable<GeoLocation> FindLocationsWithinDistance(this GeoLocation geoLocation, RohContext _context, bool checkNotificationOn = false)
        {
            PointLocation currentLocation = PointLocation.FromDegrees(geoLocation.Latitude, geoLocation.Longitude);
            double distance = 10;

            List<GeoLocation> results = new List<GeoLocation>();
            var now = DateTime.Now;
            var geoLocations = _context.GeoLocation.Include(g => g.Client)
                .Where(l => l.Client.IsDriver != geoLocation.Client.IsDriver);
            if (checkNotificationOn)
            {
                geoLocations = geoLocations.Where(l => l.Client.IsNotificationsOn);
            }

            geoLocations = geoLocations.Where(l => now.Subtract(l.UpdatedAt.Value.DateTime).TotalMinutes < 5 && l.Client.IsFree);

            foreach (var location in geoLocations)
            {
                var point = PointLocation.FromDegrees(location.Latitude, location.Longitude);
                if (currentLocation.DistanceTo(point) <= distance)
                {
                    results.Add(location);
                }
            }

            return results;
        }
    }
}
