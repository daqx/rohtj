﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using RohTjWeb.Data;
using RohTjWeb.Models;

namespace RohTjWeb.Migrations
{
    [DbContext(typeof(RohContext))]
    [Migration("20170319061026_To_Client_Platform")]
    partial class To_Client_Platform
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("RohTjWeb.Models.Brand", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset?>("CreatedAt");

                    b.Property<string>("Name");

                    b.Property<DateTimeOffset?>("UpdatedAt");

                    b.HasKey("Id");

                    b.ToTable("Brand");
                });

            modelBuilder.Entity("RohTjWeb.Models.BrandModel", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BrandId");

                    b.Property<DateTimeOffset?>("CreatedAt");

                    b.Property<string>("Name");

                    b.Property<DateTimeOffset?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("BrandId");

                    b.ToTable("BrandModel");
                });

            modelBuilder.Entity("RohTjWeb.Models.City", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset?>("CreatedAt");

                    b.Property<string>("District");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("RegionId")
                        .IsRequired();

                    b.Property<DateTimeOffset?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("RegionId");

                    b.ToTable("City");
                });

            modelBuilder.Entity("RohTjWeb.Models.Client", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("Balance");

                    b.Property<DateTimeOffset?>("BirthDay");

                    b.Property<bool>("Blocked");

                    b.Property<string>("BrandId");

                    b.Property<string>("BrandModelId");

                    b.Property<string>("CityId");

                    b.Property<string>("Color");

                    b.Property<DateTimeOffset?>("CreatedAt");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<int>("Gender");

                    b.Property<bool>("IsDispatching");

                    b.Property<bool>("IsDriver");

                    b.Property<bool>("IsFree");

                    b.Property<bool>("IsNotificationsOn");

                    b.Property<string>("LastName");

                    b.Property<string>("Number");

                    b.Property<string>("PhoneNumber")
                        .IsRequired();

                    b.Property<int>("Platform");

                    b.Property<double>("Rating");

                    b.Property<DateTimeOffset?>("UpdatedAt");

                    b.Property<bool>("UploadedPhoto");

                    b.Property<int>("YearOfManifacture");

                    b.HasKey("Id");

                    b.HasIndex("BrandId");

                    b.HasIndex("BrandModelId");

                    b.HasIndex("CityId");

                    b.ToTable("Client");
                });

            modelBuilder.Entity("RohTjWeb.Models.GeoLocation", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClientId");

                    b.Property<DateTimeOffset?>("CreatedAt");

                    b.Property<bool>("IsDriver");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<DateTimeOffset?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.ToTable("GeoLocation");
                });

            modelBuilder.Entity("RohTjWeb.Models.GeoLocationHistory", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClientId");

                    b.Property<DateTimeOffset?>("CreatedAt");

                    b.Property<bool>("IsDriver");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<DateTimeOffset?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.ToTable("GeoLocationHistory");
                });

            modelBuilder.Entity("RohTjWeb.Models.Order", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClientId");

                    b.Property<DateTimeOffset?>("CreatedAt");

                    b.Property<string>("DriverId");

                    b.Property<int>("FreePlace");

                    b.Property<string>("From");

                    b.Property<string>("ImagePath");

                    b.Property<bool>("IsDriver");

                    b.Property<bool>("IsNotificationSent");

                    b.Property<bool>("IsReviewSent");

                    b.Property<string>("Note");

                    b.Property<int>("OrderType");

                    b.Property<decimal>("Price");

                    b.Property<int>("Status");

                    b.Property<string>("To");

                    b.Property<DateTimeOffset?>("UpdatedAt");

                    b.Property<double>("Weight");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.HasIndex("DriverId");

                    b.ToTable("Order");
                });

            modelBuilder.Entity("RohTjWeb.Models.OrderCall", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset?>("CreatedAt");

                    b.Property<string>("OrderId");

                    b.Property<int>("State");

                    b.Property<DateTimeOffset?>("UpdatedAt");

                    b.Property<string>("WhoCalledId");

                    b.HasKey("Id");

                    b.HasIndex("OrderId");

                    b.HasIndex("WhoCalledId");

                    b.ToTable("OrderCall");
                });

            modelBuilder.Entity("RohTjWeb.Models.Payment", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset?>("CreatedAt");

                    b.Property<string>("Description");

                    b.Property<string>("PayerId");

                    b.Property<string>("ReceiverId");

                    b.Property<double>("Summa");

                    b.Property<int>("Type");

                    b.Property<DateTimeOffset?>("UpdatedAt");

                    b.Property<string>("Username");

                    b.HasKey("Id");

                    b.HasIndex("PayerId");

                    b.HasIndex("ReceiverId");

                    b.ToTable("Payment");
                });

            modelBuilder.Entity("RohTjWeb.Models.Region", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset?>("CreatedAt");

                    b.Property<string>("Name");

                    b.Property<DateTimeOffset?>("UpdatedAt");

                    b.HasKey("Id");

                    b.ToTable("Region");
                });

            modelBuilder.Entity("RohTjWeb.Models.RegistrationCode", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClientId");

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasMaxLength(4);

                    b.Property<DateTimeOffset?>("CreatedAt");

                    b.Property<DateTimeOffset?>("UpdatedAt");

                    b.Property<DateTimeOffset?>("UsedTime");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.ToTable("RegistrationCode");
                });

            modelBuilder.Entity("RohTjWeb.Models.Review", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClientId")
                        .IsRequired();

                    b.Property<string>("Comment");

                    b.Property<DateTimeOffset?>("CreatedAt");

                    b.Property<string>("DriverId")
                        .IsRequired();

                    b.Property<int>("Raiting");

                    b.Property<DateTimeOffset?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.HasIndex("DriverId");

                    b.ToTable("Review");
                });

            modelBuilder.Entity("RohTjWeb.Models.Transaction", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClientId");

                    b.Property<DateTimeOffset?>("CreatedAt");

                    b.Property<string>("Description");

                    b.Property<bool>("Dt");

                    b.Property<string>("PaymentId");

                    b.Property<double>("Summa");

                    b.Property<DateTimeOffset?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.HasIndex("PaymentId");

                    b.ToTable("Transacion");
                });

            modelBuilder.Entity("RohTjWeb.Models.BrandModel", b =>
                {
                    b.HasOne("RohTjWeb.Models.Brand", "Brand")
                        .WithMany()
                        .HasForeignKey("BrandId");
                });

            modelBuilder.Entity("RohTjWeb.Models.City", b =>
                {
                    b.HasOne("RohTjWeb.Models.Region", "Region")
                        .WithMany()
                        .HasForeignKey("RegionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("RohTjWeb.Models.Client", b =>
                {
                    b.HasOne("RohTjWeb.Models.Brand", "Brand")
                        .WithMany()
                        .HasForeignKey("BrandId");

                    b.HasOne("RohTjWeb.Models.BrandModel", "BrandModel")
                        .WithMany()
                        .HasForeignKey("BrandModelId");

                    b.HasOne("RohTjWeb.Models.City", "City")
                        .WithMany()
                        .HasForeignKey("CityId");
                });

            modelBuilder.Entity("RohTjWeb.Models.GeoLocation", b =>
                {
                    b.HasOne("RohTjWeb.Models.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientId");
                });

            modelBuilder.Entity("RohTjWeb.Models.GeoLocationHistory", b =>
                {
                    b.HasOne("RohTjWeb.Models.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientId");
                });

            modelBuilder.Entity("RohTjWeb.Models.Order", b =>
                {
                    b.HasOne("RohTjWeb.Models.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientId");

                    b.HasOne("RohTjWeb.Models.Client", "Driver")
                        .WithMany()
                        .HasForeignKey("DriverId");
                });

            modelBuilder.Entity("RohTjWeb.Models.OrderCall", b =>
                {
                    b.HasOne("RohTjWeb.Models.Order", "Order")
                        .WithMany()
                        .HasForeignKey("OrderId");

                    b.HasOne("RohTjWeb.Models.Client", "WhoCalled")
                        .WithMany()
                        .HasForeignKey("WhoCalledId");
                });

            modelBuilder.Entity("RohTjWeb.Models.Payment", b =>
                {
                    b.HasOne("RohTjWeb.Models.Client", "Payer")
                        .WithMany()
                        .HasForeignKey("PayerId");

                    b.HasOne("RohTjWeb.Models.Client", "Receiver")
                        .WithMany()
                        .HasForeignKey("ReceiverId");
                });

            modelBuilder.Entity("RohTjWeb.Models.RegistrationCode", b =>
                {
                    b.HasOne("RohTjWeb.Models.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientId");
                });

            modelBuilder.Entity("RohTjWeb.Models.Review", b =>
                {
                    b.HasOne("RohTjWeb.Models.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("RohTjWeb.Models.Client", "Driver")
                        .WithMany()
                        .HasForeignKey("DriverId");
                });

            modelBuilder.Entity("RohTjWeb.Models.Transaction", b =>
                {
                    b.HasOne("RohTjWeb.Models.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientId");

                    b.HasOne("RohTjWeb.Models.Payment", "Payment")
                        .WithMany()
                        .HasForeignKey("PaymentId");
                });
        }
    }
}
