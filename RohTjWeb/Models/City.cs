﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models
{
    public class City: BaseModel
    {
        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }
        [Required]
        public string RegionId { get; set; }
        [Display(Name = "Область")]
        public Region Region { get; set; }
        [Display(Name ="Район")]
        public string District { get; set; }
        [Display(Name = "Сортировка")]
        public int Order { get; set; }
    }
}
