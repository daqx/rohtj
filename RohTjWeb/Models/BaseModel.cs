﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models
{
    public class BaseModel
    {
        public string Id { get; set; }

        public DateTimeOffset? CreatedAt { get; set; }
        [JsonIgnore]
        public DateTimeOffset? UpdatedAt { get; set; }
    }
}
