﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models
{
    public class RegistrationCode:BaseModel
    {
        [Required]
        [MaxLength(4)]
        public string Code { get; set; }

        public DateTimeOffset? UsedTime { get; set; }
        public Client Client { get; set; }
        [Display(Name = "Клиент")]
        public string ClientId { get; set; }
    }
}
