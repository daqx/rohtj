﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models.ViewModel
{
    public class EditUserViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        //public int Year { get; set; }
        public string PhoneNumber { get; set; }
        public string NewPassword { get; set; }
    }
}
