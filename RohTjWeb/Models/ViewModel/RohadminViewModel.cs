﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models.ViewModel
{
    public class RohadminViewModel
    {
        public int TotalOrders { get; set; }
        public int CompletedOrders { get; set; }
        public int CancelledOrders { get; set; }
        public IEnumerable<Order> Orders { get; set; }

    }
}
