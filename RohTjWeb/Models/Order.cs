﻿using System.ComponentModel.DataAnnotations;

namespace RohTjWeb.Models
{
    public enum OrderStatus
    {
        New,
        Started,
        Completed,
        Cancelled
    }

    public enum OrderType
    {
        City,
        BetweenCity,
        Truck
    }

    public class Order : BaseModel
    {
        [Display(Name = "Откуда")]
        public string From { get; set; }
        [Display(Name = "Куда")]
        public string To { get; set; }
        [Display(Name = "Цена")]
        public decimal Price { get; set; }

        [Display(Name = "Свободные места")]
        public int FreePlace { get; set; }

        [Display(Name = "Вес(т.)")]
        public double Weight { get; set; }

        [Display(Name = "Водитель")]
        public bool IsDriver { get; set; }

        [Display(Name = "Тип")]
        public OrderType OrderType { get; set; }

        [Display(Name = "Примечание")]
        public string Note { get; set; }

        [Display(Name = "Путь к изображению")]
        public string ImagePath { get; set; }

        [Display(Name = "Статус")]
        public OrderStatus Status { get; set; }

        public string ClientId { get; set; }
        public Client Client { get; set; }

        public string DriverId { get; set; }
        public Client Driver { get; set; }

        public bool IsReviewSent { get; set; }
        
        /// <summary>
        /// Is notification sent to drivers
        /// </summary>
        public bool IsNotificationSent { get; set; }
        
        //public virtual ICollection<OrderCall> OrderCalls { get; set; }
    }
}
