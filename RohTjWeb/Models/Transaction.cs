﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models
{
    public class Transaction: BaseModel
    {
        public Client Client { get; set; }
        public Payment Payment { get; set; }
        public double Summa { get; set; }
        public string Description { get; set; }
        /// <summary>
        /// If True - increasing client balance, if False decreasing
        /// </summary>
        public bool Dt { get; set; }
    }
}
