﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models
{
    public enum SettingType
    {
        Intager,
        String,
        Boolean,
        Double,
        Guid
    }

    public enum SettingCode
    {
        IsSendingReivewStarted,
        IsSendingNotifyDriverStarted
    }

    public class SysSetting :BaseModel
    {
        public SettingType SettingType { get; set; }
        public SettingCode SettingCode { get; set; }
        public string Value { get; set; }
    }
}
