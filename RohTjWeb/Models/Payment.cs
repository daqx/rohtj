﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models
{
    public enum PaymentType
    {
        IncreasePay,
        DecreasePay
    }

    public class Payment: BaseModel
    {
        public Client Payer { get; set; }
        public Client Receiver { get; set; }
        public double Summa { get; set; }
        public string Description { get; set; }
        public PaymentType Type { get; set; }
        /// <summary>
        /// User who created payment
        /// </summary>
        public string Username { get; set; }
        public Guid OrderGuid { get; set; }
        public bool IsDispatching { get; set; }
    }
}
