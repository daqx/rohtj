﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace RohTjWeb.Models
{
    public class GeoLocation: BaseModel
    {
        [Display(Name = "Широта")]
        public double Latitude { get; set; }
        [Display(Name = "Долгота")]
        public double Longitude { get; set; }
        public Client Client { get; set; }
        public string ClientId { get; set; }
        public bool IsDriver { get; set; }
    }
}
