﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models
{
    public class BrandModel : BaseModel
    {
        [Display(Name="Модель")]
        public string Name { get; set; }

        public Brand Brand { get; set; }

        [Display(Name= "Марка")]
        public string BrandId { get; set; }
    }
}
