﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models
{
    public class Region: BaseModel
    {
        [Display(Name = "Область")]
        public string Name { get; set; }
    }
}
