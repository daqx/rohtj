﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models
{
    public class Feedback
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Message { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Необходимо ввести номер")]
        [RegularExpression(@"^\(?([0-9]{5})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Введите правильный номер.")]
        public string Phone { get; set; }
    }

    public class Feedback_vacancy
    {
        [Required]
        public string Message { get; set; }
        [Required]
        public string Driver_name { get; set; }
        [Required]
        public string Driver_surname { get; set; }
        [Required]
        public string Driver_birthday { get; set; }
        [Required]
        public string Brand_avto { get; set; }
        [Required]
        public string Model_avto { get; set; }
        [Required]
        public string Year_avto { get; set; }
        [Required]
        public string Color_avto { get; set; }
        


        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Необходимо ввести номер")]
        [RegularExpression(@"^\(?([0-9]{5})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Введите правильный номер.")]
        public string Phone { get; set; }
    }
}
