﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models
{
    public class Brand: BaseModel
    {
        [Display(Name = "Название")]
        public string Name { get; set; }
    }
}
