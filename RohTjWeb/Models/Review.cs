﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models
{
    public class Review: BaseModel
    {
        [Required]
        public string ClientId { get; set; }

        [Required]
        public string DriverId { get; set; }
        /// <summary>
        /// Client who did review
        /// </summary>
        public Client Client { get; set; }
        /// <summary>
        /// Client who were reviewed
        /// </summary>
        public Client Driver { get; set; }
        
        /// <summary>
        /// Raiting from 1-5
        /// </summary>
        public int Raiting { get; set; }

        public string Comment { get; set; }
    }
}
