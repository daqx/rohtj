﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models
{
    public enum Gender
    {
        Мужской,
        Женский
    }
    public enum Platform
    {
        Android, iOS, Windows
    }
    public class Client : BaseModel
    {
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Водитель")]
        public bool IsDriver { get; set; }

        [Display(Name = "Статус")]
        public bool IsFree { get; set; }

        [Display(Name = "Дата рождения")]
        [DataType(DataType.Date)]
        public DateTimeOffset? BirthDay { get; set; }

        [Display(Name = "Остаток")]
        public double Balance { get; set; }

        [Display(Name = "Пол")]
        public Gender Gender { get; set; }

        [Display(Name ="Платформа")]
        public Platform Platform { get; set; }

        [Display(Name = "Почта")]
        public string Email { get   ; set; }

        public string CityId { get; set; }
        [Display(Name = "Город")]
        public City City { get; set; }
        
        [Required]
        [Display(Name = "Номер телефона")]
        public string PhoneNumber { get; set; }


        public string BrandId { get; set; }
        public Brand Brand { get; set; }

        public string BrandModelId { get; set; }
        public BrandModel BrandModel { get; set; }

        [Display(Name = "Номер")]
        public string Number { get; set; }
        [Display(Name = "Цвет")]
        public string Color { get; set; }
        [Display(Name = "Дата выпуска")]
        public int YearOfManifacture { get; set; }

        public bool UploadedPhoto { get; set; }

        public bool Blocked { get; set; }

        public bool IsNotificationsOn { get; set; }

        public double Rating { get; set; }

        public bool IsDispatching { get; set; }

        [NotMapped]
        public string FullName { get { return FirstName + " " + LastName; } }

        //[InverseProperty("Client")]
        //public List<Order> ClientOrders { get; set; }

        //[InverseProperty("Driver")]
        //public List<Order> DriverOrders { get; set; }

    }
}
