﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models
{
    public class ReviewSummary: BaseModel
    {
        [Required]
        public string DriverId { get; set; }
        public Client Driver { get; set; }
        public int ReviewNumber { get; set; }
        public double Rate { get; set; }
    }
}
