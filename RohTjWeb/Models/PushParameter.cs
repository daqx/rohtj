﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Models
{
    public class PushParameter
    {
        public string Code { get; set; }
        public string Id { get; set; }

        public PushParameter(string code, string id)
        {
            Code = code;
            Id = id;
        }

        public string Serialize()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
