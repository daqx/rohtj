﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RohTjWeb.Data;
using RohTjWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Data
{
    public class RohInitializer
    {
        public static void Seed(RohContext context, ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager)
        {
            if (context.Brand.Any())
                return;

            //CreateAdminUser(applicationDbContext, userManager);
            CleanAll(context);
            SeedBrand(context);

            SeedCity(context);
        }

        internal async static void CreateRoles(ApplicationDbContext applicationDbContext, IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            List<IdentityRole> roles = new List<IdentityRole>
            {
                new IdentityRole() { Name = "Admin", NormalizedName = "ADMIN" },
                new IdentityRole() { Name = "Manager", NormalizedName = "Manager" },
                new IdentityRole() { Name = "Mobile", NormalizedName = "MOBILE" },
                new IdentityRole() { Name = "Operator", NormalizedName = "OPERATOR" }
            };
            foreach (var role in roles)
            {
                var roleExists = await roleManager.RoleExistsAsync(role.Name);
                if (!roleExists)
                {
                    applicationDbContext.Roles.Add(role);
                    applicationDbContext.SaveChanges();
                }
            }

            var adminUser = await userManager.FindByEmailAsync("jahmadbekov@gmail.com");
            if (adminUser != null)
                await userManager.AddToRoleAsync(adminUser, "Admin");

            var mobileUser = await userManager.FindByEmailAsync("dunusov@gmail.com");
            if (mobileUser != null)
                await userManager.AddToRoleAsync(mobileUser, "Mobile");
        }

        //private static async void CreateAdminUser(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager)
        //{
        //    var user = new ApplicationUser { UserName = "jahmadbekov@gmail.com", Email = "jahmadbekov@gmail.com" };
        //    var result = await userManager.CreateAsync(user, "adminRoh2017!");
        //}

        private static void SeedBrand(RohContext context)
        {

            List<Brand> brands = new List<Brand>() {
                new Brand() { Id = "cefd0193-94bc-42a3-a5ca-5e836a553d4a",Name = "Mersedess" },
                new Brand() { Id = "cefd0193-94bc-42a3-a5ca-5e836a553d4b",Name = "BMW" },
                new Brand() { Id = "cefd0193-94bc-42a3-a5ca-5e836a553d4c",Name = "Opel" },
                new Brand() { Id = "cefd0193-94bc-42a3-a5ca-5e836a553d4d",Name = "Toyota" }
            };
            brands.ForEach(b => context.Brand.Add(b));
            context.SaveChanges();

            var brandModels = new List<BrandModel>();
            var mersedess = context.Brand.First(b => b.Name == "Mersedess");
            brandModels.Add(new BrandModel() { Id = "25967c30-8a06-458e-8f1b-0d15cd451c51", Brand = mersedess, Name = "A class" });
            brandModels.Add(new BrandModel() { Id = "25967c30-8a06-458e-8f1b-0d15cd451c52", Brand = mersedess, Name = "B class" });
            brandModels.Add(new BrandModel() { Id = "25967c30-8a06-458e-8f1b-0d15cd451c53", Brand = mersedess, Name = "C class" });

            var bmw = context.Brand.First(b => b.Name == "BMW");
            brandModels.Add(new BrandModel() { Id = "25967c30-8a06-458e-8f1b-0d15cd451c61", Brand = bmw, Name = "M 3" });
            brandModels.Add(new BrandModel() { Id = "25967c30-8a06-458e-8f1b-0d15cd451c62", Brand = bmw, Name = "M 5" });
            brandModels.Add(new BrandModel() { Id = "25967c30-8a06-458e-8f1b-0d15cd451c63", Brand = bmw, Name = "M 7" });

            var opel = context.Brand.First(b => b.Name == "Opel");
            brandModels.Add(new BrandModel() { Id = "25967c30-8a06-458e-8f1b-0d15cd451c71", Brand = opel, Name = "Vectra" });
            brandModels.Add(new BrandModel() { Id = "25967c30-8a06-458e-8f1b-0d15cd451c72", Brand = opel, Name = "Astra" });
            brandModels.Add(new BrandModel() { Id = "25967c30-8a06-458e-8f1b-0d15cd451c73", Brand = opel, Name = "Zafira" });

            var toyota = context.Brand.First(b => b.Name == "Toyota");
            brandModels.Add(new BrandModel() { Id = "25967c30-8a06-458e-8f1b-0d15cd451c81", Brand = toyota, Name = "Carola" });
            brandModels.Add(new BrandModel() { Id = "25967c30-8a06-458e-8f1b-0d15cd451c82", Brand = toyota, Name = "Chaiser" });
            brandModels.Add(new BrandModel() { Id = "25967c30-8a06-458e-8f1b-0d15cd451c83", Brand = toyota, Name = "Yaris" });

            brandModels.ForEach(m => context.BrandModel.Add(m));
            context.SaveChangesAsync();
        }

        static void CleanAll(RohContext context)
        {
            foreach (var code in context.RegistrationCode)
            {
                context.RegistrationCode.Remove(code);
            }
            foreach (var client in context.Client)
            {
                context.Client.Remove(client);
            }

            foreach (var region in context.Region)
            {
                context.Region.Remove(region);
            }

            foreach (var city in context.City)
            {
                context.City.Remove(city);
            }

            foreach (var brand in context.Brand)
            {
                context.Brand.Remove(brand);
            }

            foreach (var brandModel in context.BrandModel)
            {
                context.BrandModel.Remove(brandModel);
            }
            context.SaveChanges();
        }

        static void SeedCity(RohContext context)
        {

            List<Region> regions = new List<Region>() {
                new Region()
                {
                    Id = "460e5a2d-e544-4021-9082-afca5bba290a",
                    Name = "Сугд"
                },
                new Region() { Id = "460e5a2d-e544-4021-9082-afca5bba290b",Name = "Хатлон" },
                new Region() { Id = "460e5a2d-e544-4021-9082-afca5bba290c",Name = "РРП" },
                new Region() { Id = "460e5a2d-e544-4021-9082-afca5bba290d",Name = "Бадахшон" }
            };

            regions.ForEach(b => context.Region.Add(b));
            context.SaveChanges();

            List<City> cities = new List<City>
            {
                new City() { Id = "dabd9637-7747-4709-9e5c-9994f7e458da", Region = regions[0], Name = "Хучанд" },
                new City() { Id = "dabd9637-7747-4709-9e5c-9994f7e458db", Region = regions[0], Name = "Дехмой", District = "р-н Ч. Расулов" },
                new City() { Id = "dabd9637-7747-4709-9e5c-9994f7e458dc", Region = regions[0], Name = "Исфара" },
                new City() { Id = "dabd9637-7747-4709-9e5c-9994f7e458dd", Region = regions[1], Name = "Кургантюбе" },
                new City() { Id = "dabd9637-7747-4709-9e5c-9994f7e458de", Region = regions[1], Name = "Кулоб" },
                new City() { Id = "dabd9637-7747-4709-9e5c-9994f7e458df", Region = regions[2], Name = "Душанбе" },
                new City() { Id = "dabd9637-7747-4709-9e5c-9994f7e458dg", Region = regions[2], Name = "Ёвон" },
                new City() { Id = "dabd9637-7747-4709-9e5c-9994f7e458dh", Region = regions[3], Name = "Хоруг" },
                new City() { Id = "dabd9637-7747-4709-9e5c-9994f7e458di", Region = regions[3], Name = "Мургоб" }
            };
            cities.ForEach(b => context.City.Add(b));
            context.SaveChanges();

        }
    }
}
