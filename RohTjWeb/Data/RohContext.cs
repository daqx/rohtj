﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Models;
using Microsoft.EntityFrameworkCore.Metadata;

namespace RohTjWeb.Data
{
    public class RohContext : DbContext
    {
        public RohContext(DbContextOptions<RohContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity(typeof(Review))
            .HasOne(typeof(Client), "Driver")
            .WithMany()
            .HasForeignKey("DriverId")
            .OnDelete(DeleteBehavior.Restrict); // no ON DELETE

            modelBuilder.Entity(typeof(Review))
                .HasOne(typeof(Client), "Client")
                .WithMany()
                .HasForeignKey("ClientId")
                .OnDelete(DeleteBehavior.Cascade); // set ON DELETE CASCADE
        }

        public DbSet<Brand> Brand { get; set; }
        public DbSet<BrandModel> BrandModel { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<Region> Region { get; set; }
        public DbSet<GeoLocation> GeoLocation { get; set; }
        public DbSet<GeoLocationHistory> GeoLocationHistory { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderCall> OrderCall { get; set; }
        public DbSet<Review> Review { get; set; }
        public DbSet<RegistrationCode> RegistrationCode { get; set; }
        public DbSet<Payment> Payment { get; set; }
        public DbSet<SysSetting> SysSetting { get; set; }
        public DbSet<Transaction> Transacion { get; set; }


        #region Adding Creation Date
        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            AddTimestamps();
            return base.SaveChangesAsync(cancellationToken);
        }
        //public override async Task<int> SaveChangesAsync()
        //{
        //    AddTimestamps();
        //    return await base.SaveChangesAsync();
        //}

        private void AddTimestamps()
        {
            var entities = ChangeTracker.Entries()
                .Where(x => x.Entity is BaseModel && (x.State == EntityState.Added || x.State == EntityState.Modified));

            //var currentUsername = !string.IsNullOrEmpty(System.Web.HttpContext.Current?.User?.Identity?.Name)
            //    ? HttpContext.Current.User.Identity.Name
            //    : "Anonymous";

            foreach (var entity in entities)
            {
                if (entity.State == EntityState.Added)
                {
                    ((BaseModel)entity.Entity).CreatedAt = DateTime.Now;
                    //((BaseModel)entity.Entity).UserCreated = currentUsername;
                }

                ((BaseModel)entity.Entity).UpdatedAt = DateTime.Now;
                //((BaseModel)entity.Entity).UserModified = currentUsername;
            }
        }
        //public override async Task<int> SaveChangesAsync()
        //{
        //    AddTimestamps();
        //    return await base.SaveChangesAsync();
        //}

        #endregion
    }
}
