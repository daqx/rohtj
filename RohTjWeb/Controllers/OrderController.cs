using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;
using Microsoft.AspNetCore.Authorization;

namespace RohTjWeb.Controllers
{
    [Authorize(Roles = "Admin, Operator")]
    public class OrderController : Controller
    {
        private readonly RohContext _context;

        public OrderController(RohContext context)
        {
            _context = context;    
        }

        // GET: Order
        public async Task<IActionResult> Index()
        {
            return View(await _context.Order.Include(o=>o.Client).ToListAsync());
        }

        // GET: OrderCity
        public async Task<IActionResult> OrderCity()
        {
            return View(await _context.Order.Include(o => o.Client).Where(o =>o.OrderType == OrderType.City).ToListAsync());
        }

        // GET: OrderBetweenCity
        public async Task<IActionResult> OrderBetweenCity()
        {
            return View(await _context.Order.Include(o => o.Client).Where(o => o.OrderType == OrderType.BetweenCity).ToListAsync());
        }

        // GET: OrderBetweenCity
        public async Task<IActionResult> OrderTruck()
        {
            return View(await _context.Order.Include(o => o.Client).Where(o => o.OrderType == OrderType.Truck).ToListAsync());
        }

        // GET: Order/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order.SingleOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // GET: Order/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Order/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CreatedAt,FreePlace,From,ImagePath,IsDriver,Note,Price,State,To,UpdatedAt")] Order order)
        {
            order.Id = Guid.NewGuid().ToString();
            if (ModelState.IsValid)
            {
                _context.Add(order);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(order);
        }

        // GET: Order/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order.SingleOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }
            return View(order);
        }

        // POST: Order/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,CreatedAt,FreePlace,From,ImagePath,IsDriver,Note,Price,State,To,UpdatedAt")] Order order)
        {
            if (id != order.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(order);
        }

        // GET: Order/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order.SingleOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Order/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var order = await _context.Order.SingleOrDefaultAsync(m => m.Id == id);
            _context.Order.Remove(order);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool OrderExists(string id)
        {
            return _context.Order.Any(e => e.Id == id);
        }
    }
}
