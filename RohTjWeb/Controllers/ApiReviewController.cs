using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;

namespace RohTjWeb.Controllers
{
    [Produces("application/json")]
    [Route("api/Review")]
    public class ApiReviewController : Controller
    {
        private readonly RohContext _context;

        public ApiReviewController(RohContext context)
        {
            _context = context;
        }

        // GET: api/ApiReview
        [HttpGet]
        public IEnumerable<Review> GetReview()
        {
            return _context.Review;
        }

        // GET: api/ApiReview/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetReview([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Review review = await _context.Review.SingleOrDefaultAsync(m => m.Id == id);

            if (review == null)
            {
                return NotFound();
            }

            return Ok(review);
        }

        // PUT: api/ApiReview/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReview([FromRoute] string id, [FromBody] Review review)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != review.Id)
            {
                return BadRequest();
            }

            _context.Entry(review).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReviewExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ApiReview
        [HttpPost]
        public async Task<IActionResult> PostReview([FromBody] Review review)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (string.IsNullOrEmpty(review.Id))
                review.Id = Guid.NewGuid().ToString();

            _context.Review.Add(review);
            try
            {
                await _context.SaveChangesAsync();
                UpdateClientsRating(review);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ReviewExists(review.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetReview", new { id = review.Id }, review);
        }

        private void UpdateClientsRating(Review review)
        {
            var allReviewes = _context.Review.Where(r => r.Driver.Id == review.DriverId);
            double rateTotal = 0;
            foreach (var reviewItem in allReviewes)
                rateTotal += reviewItem.Raiting;
            
            var driver = _context.Client.FirstOrDefault(r => r.Id == review.DriverId);
            if (driver != null)
                driver.Rating = rateTotal / allReviewes.Count();

        }

        // DELETE: api/ApiReview/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReview([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Review review = await _context.Review.SingleOrDefaultAsync(m => m.Id == id);
            if (review == null)
            {
                return NotFound();
            }

            _context.Review.Remove(review);
            await _context.SaveChangesAsync();

            return Ok(review);
        }

        private bool ReviewExists(string id)
        {
            return _context.Review.Any(e => e.Id == id);
        }
    }
}