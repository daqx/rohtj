﻿using Microsoft.AspNetCore.Mvc;

namespace RohTjWeb.Controllers
{
    public class ErrorController : Controller
    {

        public ActionResult Error_404()
        {
            Response.StatusCode = 404;
            return View();
        }

        public ActionResult Forbidden()
        {
            Response.StatusCode = 403;
            return View();
        }
    }
}
