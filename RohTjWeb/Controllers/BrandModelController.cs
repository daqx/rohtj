using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;
using Microsoft.AspNetCore.Authorization;

namespace RohTjWeb.Controllers
{
    [Authorize(Roles = "Admin, Operator")]
    public class BrandModelController : Controller
    {
        private readonly RohContext _context;

        public BrandModelController(RohContext context)
        {
            _context = context;
        }

        // GET: BrandModel
        public async Task<IActionResult> Index()
        {
            return View(await _context.BrandModel.Include(c => c.Brand).ToListAsync());
        }

        // GET: BrandModel/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var brandModel = await _context.BrandModel.Include(b => b.Brand).SingleOrDefaultAsync(m => m.Id == id);
            if (brandModel == null)
            {
                return NotFound();
            }

            return View(brandModel);
        }

        // GET: BrandModel/Create
        public IActionResult Create()
        {
            PopulateBrandsDropDownList();
            return View();
        }

        // POST: BrandModel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CreatedAt,Name,UpdatedAt,BrandId")] BrandModel brandModel)
        {
            if (ModelState.IsValid)
            {
                brandModel.Id = Guid.NewGuid().ToString();
                _context.Add(brandModel);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            PopulateBrandsDropDownList(brandModel.BrandId);

            return View(brandModel);
        }

        // GET: BrandModel/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var brandModel = await _context.BrandModel.SingleOrDefaultAsync(m => m.Id == id);
            if (brandModel == null)
            {
                return NotFound();
            }
            PopulateBrandsDropDownList(brandModel.BrandId);

            return View(brandModel);
        }

        // POST: BrandModel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,CreatedAt,Name,UpdatedAt")] BrandModel brandModel)
        {
            if (id != brandModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(brandModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BrandModelExists(brandModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            PopulateBrandsDropDownList(brandModel.BrandId);
            return View(brandModel);
        }

        // GET: BrandModel/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var brandModel = await _context.BrandModel.SingleOrDefaultAsync(m => m.Id == id);
            if (brandModel == null)
            {
                return NotFound();
            }

            return View(brandModel);
        }

        // POST: BrandModel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var brandModel = await _context.BrandModel.SingleOrDefaultAsync(m => m.Id == id);
            _context.BrandModel.Remove(brandModel);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private void PopulateBrandsDropDownList(string selectedBrand = null)
        {
            var brandsQuery = from d in _context.Brand
                              orderby d.Name
                              select d;
            ViewBag.BrandID = new SelectList(brandsQuery.AsNoTracking(), "Id", "Name", selectedBrand);
        }
        private bool BrandModelExists(string id)
        {
            return _context.BrandModel.Any(e => e.Id == id);
        }
    }
}
