using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;
using Microsoft.AspNetCore.Authorization;

namespace RohTjWeb.Controllers
{
    [Authorize(Roles = "Admin, Operator")]
    public class CityController : Controller
    {
        private readonly RohContext _context;

        public CityController(RohContext context)
        {
            _context = context;
        }

        // GET: City
        public async Task<IActionResult> Index()
        {
            return View(await _context.City.Include(c => c.Region).OrderByDescending(c=>c.Order).ThenBy(c=>c.Name).ToListAsync());
        }

        // GET: City/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var city = await _context.City.Include(c => c.Region).SingleOrDefaultAsync(m => m.Id == id);
            if (city == null)
            {
                return NotFound();
            }

            return View(city);
        }

        // GET: City/Create
        public IActionResult Create()
        {
            PopulateRegionsDropDownList();

            return View();
        }

        // POST: City/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,RegionId,District,Order,CreatedAt,Name,UpdatedAt")] City city)
        {
            city.Id = Guid.NewGuid().ToString();
            if (ModelState.IsValid)
            {
                _context.Add(city);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            PopulateRegionsDropDownList(city.RegionId);

            return View(city);
        }

        // GET: City/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var city = await _context.City.Include(c => c.Region).SingleOrDefaultAsync(m => m.Id == id);
            if (city == null)
            {
                return NotFound();
            }
            PopulateRegionsDropDownList(city.RegionId);

            return View(city);
        }

        // POST: City/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,RegionId, Order, District,CreatedAt,Name,UpdatedAt")] City city)
        {
            if (id != city.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(city);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CityExists(city.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            PopulateRegionsDropDownList(city.RegionId);
            return View(city);
        }

        // GET: City/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var city = await _context.City.SingleOrDefaultAsync(m => m.Id == id);
            if (city == null)
            {
                return NotFound();
            }

            return View(city);
        }

        // POST: City/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var city = await _context.City.SingleOrDefaultAsync(m => m.Id == id);
            _context.City.Remove(city);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        private void PopulateRegionsDropDownList(string selectedRegin = null)
        {
            var regionsQuery = from d in _context.Region
                               orderby d.Name
                               select d;
            ViewBag.RegionID = new SelectList(regionsQuery.AsNoTracking(), "Id", "Name", selectedRegin);
        }
        private bool CityExists(string id)
        {
            return _context.City.Any(e => e.Id == id);
        }
    }
}
