using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;

namespace RohTjWeb.Controllers
{
    [Produces("application/json")]
    [Route("api/BrandModel")]
    public class ApiBrandModelController : Controller
    {
        private readonly RohContext _context;

        public ApiBrandModelController(RohContext context)
        {
            _context = context;
        }

        // GET: api/ApiBrandModel
        [HttpGet]
        public IEnumerable<BrandModel> GetBrandModel()
        {
            return _context.BrandModel;
        }

        // GET: api/ApiBrandModel/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBrandModel([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            BrandModel brandModel = await _context.BrandModel.SingleOrDefaultAsync(m => m.Id == id);

            if (brandModel == null)
            {
                return NotFound();
            }

            return Ok(brandModel);
        }

        // PUT: api/ApiBrandModel/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBrandModel([FromRoute] string id, [FromBody] BrandModel brandModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != brandModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(brandModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BrandModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ApiBrandModel
        [HttpPost]
        public async Task<IActionResult> PostBrandModel([FromBody] BrandModel brandModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.BrandModel.Add(brandModel);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BrandModelExists(brandModel.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetBrandModel", new { id = brandModel.Id }, brandModel);
        }

        // DELETE: api/ApiBrandModel/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBrandModel([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            BrandModel brandModel = await _context.BrandModel.SingleOrDefaultAsync(m => m.Id == id);
            if (brandModel == null)
            {
                return NotFound();
            }

            _context.BrandModel.Remove(brandModel);
            await _context.SaveChangesAsync();

            return Ok(brandModel);
        }

        private bool BrandModelExists(string id)
        {
            return _context.BrandModel.Any(e => e.Id == id);
        }
    }
}