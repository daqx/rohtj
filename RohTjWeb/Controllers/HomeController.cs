﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MimeKit;
using MailKit.Net.Smtp;
using MailKit.Security;
using RohTjWeb.Services;
using RohTjWeb.Models;

namespace RohTjWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEmailSender _emailSender;
        public HomeController(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "О компании";

            return View();
        }

        public IActionResult Intercities()
        {
            ViewData["Message"] = "Межгород";

            return View();
        }
        public IActionResult Cargo()
        {
            ViewData["Message"] = "Грузовые";

            return View();
        }
        public IActionResult Dispatching()
        {
            ViewData["Message"] = "Диспетчерская служба";

            return View();
        }

        public IActionResult Contact()
        {
            //ViewData["Message"] = "Контактные данные";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Contact([Bind("Title, Message, Phone")] Feedback feedback)
        {
            // This is for the Contact Form view, you can test the form 
            // at http://localhost:41717/Home/Contact
            ViewData["Message"] = null;
            ViewData["Error"] = null;

            if (ModelState.IsValid)
            {
                _emailSender.SendEmail(
                    "info@roh.tj",
                    "Roh Contact: " + feedback.Title,
                    feedback.Message + "\n \n \n Тел.: +" + feedback.Phone

                    );
                ViewData["message_color"] = "green";
                ViewData["Message"] = "*Ваше сообщение отправлено.";
                //return RedirectToAction("Contact");
            }
            else
            {
                ViewData["message_color"] = "red";
                ViewData["Message"] = "*Заполните все поля";
            }

            ModelState.Clear();
            return View();
            //return View("Contact", feedback);
        }
        
        public IActionResult Vacancy()
        {
            //ViewData["Message"] = "Вакансии";

            return View();
        }
        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Vacancy([Bind("Message, Driver_name, Driver_surname, Driver_birthday, Brand_avto, Model_avto, Year_avto, Color_avto, Phone")] Feedback_vacancy feedback)
        {
            // This is for the Contact Form view, you can test the form 
            // at http://localhost:41717/Home/Contact
            ViewData["Message"] = null;
            ViewData["Error"] = null;

            if (ModelState.IsValid)
            {
                _emailSender.SendEmail(
                    "info@roh.tj",
                    "Roh Вакансия: ",
                    "Информация о водителе \n \n Имя: " + feedback.Driver_name + "\n Фамилия: " + feedback.Driver_surname 
                    + "\n Дата рождения: " + feedback.Driver_birthday + "\n Тел.: +" + feedback.Phone
                    + "\n \n \n Информация о транспортном средстве \n \n Марка: " + feedback.Brand_avto + "\n Модель авто: " + feedback.Model_avto 
                    + "\n Год выпуска: " + feedback.Year_avto + "\n Цвет авто: " + feedback.Color_avto
                    + "\n \n Сообщение: \n " + feedback.Message

                    );
                ViewData["message_color"] = "green";
                ViewData["Message"] = "*Ваше сообщение отправлено.";
                //return RedirectToAction("Vacancy");
                ModelState.Clear();
            }
            else
            {
                ViewData["message_color"] = "red";
                ViewData["Message"] = "*Заполните все поля";
            }

            
            return View();

            //return View("Vacancy", feedback);
        }

        public IActionResult Downloads()
        {
            ViewData["Message"] = "Скачать";
            return View();
        }

        public IActionResult Rules()
        {
            ViewData["Message"] = "Правила";
            return View();
        }

        public IActionResult Oferta()
        {
            ViewData["Message"] = "Оферта";
            return View();
        }

        public IActionResult Privacy()
        {
            ViewData["Message"] = "Политика конфиденциальности";
            return View();
        }

        public IActionResult Help()
        {
            ViewData["Message"] = "Помощь";
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
