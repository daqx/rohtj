using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Headers;
using System.IO;
using RohTjWeb.Services;
using Microsoft.ApplicationInsights;

namespace RohTjWeb.Controllers
{
    [Produces("application/json")]
    [Route("api/Client")]
    public class ApiClientController : Controller
    {
        private readonly RohContext _context;
        private IHostingEnvironment _hostingEnv;
        private readonly ISmsSender _smsSender;


        public ApiClientController(RohContext context, IHostingEnvironment env, ISmsSender smsSender)
        {
            _context = context;
            _hostingEnv = env;
            _smsSender = smsSender;
        }

        // GET: api/ApiClient
        [HttpGet]
        public IEnumerable<Client> GetClient()
        {
            return _context.Client;
        }

        // GET: api/ApiClient/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetClient([FromRoute] string id)
        {
            Client client = null;
            var telemetryClient = new TelemetryClient();
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                client = await _context.Client.Include(c => c.Brand).Include(c => c.BrandModel).SingleOrDefaultAsync(m => m.Id == id);

                if (client == null)
                {
                    return NotFound();
                }

            }
            catch (Exception ex)
            {
                telemetryClient.TrackException(ex);
            }
            return Ok(client);
        }

        // PUT: api/ApiClient/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutClient([FromRoute] string id, [FromBody] Client client)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != client.Id)
            {
                return BadRequest();
            }

            // Balance should not be updated form API
            //var clientFromDB = _context.Client.FirstOrDefault(c => c.Id == id);
            //if (clientFromDB != null)
            //    client.Balance = clientFromDB.Balance;

            try
            {
                _context.Entry(client).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ApiClient
        [HttpPost]
        public async Task<IActionResult> PostClient([FromBody] Client client)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            // check if client exitsts don't create new
            var existingClient = await _context.Client.FirstOrDefaultAsync(c => c.PhoneNumber == client.PhoneNumber);
            if (existingClient != null)
            {
                client = existingClient;
            }
            else
            {
                if (string.IsNullOrEmpty(client.Id))
                    client.Id = Guid.NewGuid().ToString();
                //Todo: temporary, will be removed soon
                if (client.Balance == 0)
                    client.Balance = 1;

                _context.Client.Add(client);
            }
            // Generate new registration code
            var registrationCode = new RegistrationCode() { Id = Guid.NewGuid().ToString(), Client = client };
            registrationCode.Code = new Random(DateTime.Now.Millisecond).Next(1000, 9999).ToString();
            _context.RegistrationCode.Add(registrationCode);
            try
            {
                await _context.SaveChangesAsync();
                await _smsSender.SendSmsAsync(client.PhoneNumber, "Vkhod v RohTJ: " + registrationCode.Code);
            }
            catch (DbUpdateException)
            {
                if (ClientExists(client.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetClient", new { id = client.Id }, client);
        }

        // DELETE: api/ApiClient/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClient([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Client client = await _context.Client.SingleOrDefaultAsync(m => m.Id == id);
            if (client == null)
            {
                return NotFound();
            }

            _context.Client.Remove(client);
            await _context.SaveChangesAsync();

            return Ok(client);
        }

        [HttpPost("photo")]
        public async Task<IActionResult> PostProfilePicture(IFormFile file)
        {
            if (file != null)
            {
                long size = 0;
                var fileName = ContentDispositionHeaderValue
                        .Parse(file.ContentDisposition)
                        .FileName
                        .Trim('"');
                await MarkClientAsUploadedPhoto(fileName);

                fileName = _hostingEnv.WebRootPath + $@"\images\client\{fileName}";
                size += file.Length;
                using (FileStream fs = System.IO.File.Create(fileName))
                {
                    await file.CopyToAsync(fs);
                    await fs.FlushAsync();
                }
            }

            return Ok(); //null just to make error free
        }
        /// <summary>
        /// If client with filename based id exists, mark as uploaded photo
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private async Task MarkClientAsUploadedPhoto(string fileName)
        {
            var clientId = fileName.Substring(0, fileName.Length - 4);
            Client client = await _context.Client.SingleOrDefaultAsync(m => m.Id == clientId);
            if (client != null && !client.UploadedPhoto)
            {
                client.UploadedPhoto = true;
                await _context.SaveChangesAsync();
            }
        }

        private bool ClientExists(string id)
        {
            return _context.Client.Any(e => e.Id == id);
        }
    }
}