using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RohTjWeb.Models;
using Microsoft.Azure.NotificationHubs;
using System.Net;
using RohTjWeb.Services;

namespace RohTjWeb.Controllers
{
    public class MessageParams
    {
        public string Message { get; set; }
        public string ToTag { get; set; }
    }


    [Produces("application/json")]
    [Route("api/Notifications")]
    public class ApiNotificationsController : Controller
    {
        private NotificationHubClient hub;
        private readonly IPushSender _pushSender;

        public ApiNotificationsController(IPushSender pushSender)
        {
            _pushSender = pushSender;
            hub = Notifications.Instance.Hub;
        }

        // POST api/register
        // This creates a registration id
        [HttpPost("{id}")]
        public async Task<IActionResult> Post([FromRoute]string id, [FromBody]MessageParams message)
        {
            //var user = "Daler"; // Todo: HttpContext.Current.User.Identity.Name;
            string[] userTag = new string[1];
            userTag[0] = message.ToTag;
            //userTag[0] = "username:" + message.ToTag;
            HttpStatusCode ret = await _pushSender.SendPushAsync(id, message.Message, userTag);

            return Ok(ret);
        }
    }
}