using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Controllers
{
    [Produces("application/json")]
    [Route("api/RegistrationCode")]
    public class ApiRegistrationCodeController : Controller
    {
        private readonly RohContext _context;

        public ApiRegistrationCodeController(RohContext context)
        {
            _context = context;
        }

        // GET: api/ApiRegistrationCode
        [HttpGet]
        public IEnumerable<RegistrationCode> GetRegistrationCode()
        {
            return _context.RegistrationCode;
        }

        // GET: api/ApiRegistrationCode/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRegistrationCode([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            RegistrationCode registrationCode = await _context.RegistrationCode.SingleOrDefaultAsync(m => m.Id == id);

            if (registrationCode == null)
            {
                return NotFound();
            }

            return Ok(registrationCode);
        }

        [HttpGet("ByClient/{id}")]
        public async Task<IActionResult> GetRegistrationCodeByClient([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            RegistrationCode registrationCode = await _context.RegistrationCode
                .OrderByDescending(r=>r.CreatedAt)
                .FirstOrDefaultAsync(m => m.
            ClientId == id);

            if (registrationCode == null)
            {
                return NotFound();
            }
            registrationCode.UsedTime = DateTime.UtcNow;
            return Ok(registrationCode);
        }

        // PUT: api/ApiRegistrationCode/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRegistrationCode([FromRoute] string id, [FromBody] RegistrationCode registrationCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != registrationCode.Id)
            {
                return BadRequest();
            }

            _context.Entry(registrationCode).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RegistrationCodeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ApiRegistrationCode
        [HttpPost]
        public async Task<IActionResult> PostRegistrationCode([FromBody] RegistrationCode registrationCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.RegistrationCode.Add(registrationCode);
            try
            {
                await _context.SaveChangesAsync();
                SendPushNotification(registrationCode);
            }
            catch (DbUpdateException)
            {
                if (RegistrationCodeExists(registrationCode.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetRegistrationCode", new { id = registrationCode.Id }, registrationCode);
        }

        private void SendPushNotification(RegistrationCode registrationCode)
        {
            // Get the settings for the server project.
            //HttpConfiguration config = this.Configuration;
            //MobileAppSettingsDictionary settings =
            //    this.Configuration.GetMobileAppSettingsProvider().GetMobileAppSettings();

            //// Get the Notification Hubs credentials for the Mobile App.
            //string notificationHubName = settings.NotificationHubName;
            //string notificationHubConnection = settings
            //    .Connections[MobileAppSettingsKeys.NotificationHubConnectionString].ConnectionString;

            //// Create a new Notification Hub client.
            //NotificationHubClient hub = NotificationHubClient
            //.CreateClientFromConnectionString(notificationHubConnection, notificationHubName);

            //// Sending the message so that all template registrations that contain "messageParam"
            //// will receive the notifications. This includes APNS, GCM, WNS, and MPNS template registrations.
            //Dictionary<string, string> templateParams = new Dictionary<string, string>();
            //templateParams["messageParam"] = item.Text + " was added to the list.";

            //try
            //{
            //    // Send the push notification and log the results.
            //    var result = await hub.SendTemplateNotificationAsync(templateParams);

            //    // Write the success result to the logs.
            //    config.Services.GetTraceWriter().Info(result.State.ToString());
            //}
            //catch (System.Exception ex)
            //{
            //    // Write the failure result to the logs.
            //    config.Services.GetTraceWriter()
            //        .Error(ex.Message, null, "Push.SendAsync Error");
            //}
        }

        // DELETE: api/ApiRegistrationCode/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRegistrationCode([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            RegistrationCode registrationCode = await _context.RegistrationCode.SingleOrDefaultAsync(m => m.Id == id);
            if (registrationCode == null)
            {
                return NotFound();
            }

            _context.RegistrationCode.Remove(registrationCode);
            await _context.SaveChangesAsync();

            return Ok(registrationCode);
        }

        private bool RegistrationCodeExists(string id)
        {
            return _context.RegistrationCode.Any(e => e.Id == id);
        }
    }
}