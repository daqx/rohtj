﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using RohTjWeb.Data;
using RohTjWeb.Models.ViewModel;
using Microsoft.EntityFrameworkCore;

namespace RohTjWeb.Controllers
{
    [Authorize]
    public class RohadminController : Controller
    {
        private readonly RohContext _context;

        public RohadminController(RohContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            var rohadminViewModel = new RohadminViewModel();
            rohadminViewModel.TotalOrders = _context.Order.Count();
            rohadminViewModel.CompletedOrders = _context.Order.Count(o=>o.Status == Models.OrderStatus.Completed);
            rohadminViewModel.CancelledOrders = _context.Order.Count(o=>o.Status == Models.OrderStatus.Cancelled);

            rohadminViewModel.Orders = _context.Order.Include(o=>o.Client).OrderByDescending(o => o.CreatedAt).Take(10);

            return View(rohadminViewModel);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Ваше описание страницы.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Контактные данные.";

            return View();
        }

        public IActionResult InterCity()
        {
            ViewData["Message"] = "Межгород";

            return View();
        }

        public IActionResult ViewMap()
        {
            ViewData["Message"] = "Местоположение водителей";

            return View();
        }

        public IActionResult Rules()
        {
            ViewData["Message"] = "Правила";
            return View();
        }

        public IActionResult Oferta()
        {
            ViewData["Message"] = "Оферта";
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
