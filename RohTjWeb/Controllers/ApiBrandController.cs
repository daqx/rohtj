using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;

namespace RohTjWeb.Controllers
{
    [Produces("application/json")]
    [Route("api/Brand")]
    public class ApiBrandController : Controller
    {
        private readonly RohContext _context;

        public ApiBrandController(RohContext context)
        {
            _context = context;
        }

        // GET: api/ApiBrand
        [HttpGet]
        public IEnumerable<Brand> GetBrand()
        {
            return _context.Brand;
        }

        // GET: api/ApiBrand/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBrand([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Brand brand = await _context.Brand.SingleOrDefaultAsync(m => m.Id == id);

            if (brand == null)
            {
                return NotFound();
            }

            return Ok(brand);
        }

        // PUT: api/ApiBrand/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBrand([FromRoute] string id, [FromBody] Brand brand)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != brand.Id)
            {
                return BadRequest();
            }

            _context.Entry(brand).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BrandExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ApiBrand
        [HttpPost]
        public async Task<IActionResult> PostBrand([FromBody] Brand brand)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Brand.Add(brand);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BrandExists(brand.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetBrand", new { id = brand.Id }, brand);
        }

        // DELETE: api/ApiBrand/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBrand([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Brand brand = await _context.Brand.SingleOrDefaultAsync(m => m.Id == id);
            if (brand == null)
            {
                return NotFound();
            }

            _context.Brand.Remove(brand);
            await _context.SaveChangesAsync();

            return Ok(brand);
        }

        private bool BrandExists(string id)
        {
            return _context.Brand.Any(e => e.Id == id);
        }
    }
}