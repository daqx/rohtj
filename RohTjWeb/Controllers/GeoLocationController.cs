using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;
using Microsoft.AspNetCore.Authorization;

namespace RohTjWeb.Controllers
{
    [Authorize(Roles = "Admin, Operator, Manager")]
    public class GeoLocationController : Controller
    {
        private readonly RohContext _context;

        public GeoLocationController(RohContext context)
        {
            _context = context;    
        }

        // GET: GeoLocation
        public async Task<IActionResult> Index()
        {
            var rohContext = _context.GeoLocation.Include(g => g.Client);
            return View(await rohContext.ToListAsync());
        }

        // GET: GeoLocation/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var geoLocation = await _context.GeoLocation.SingleOrDefaultAsync(m => m.Id == id);
            if (geoLocation == null)
            {
                return NotFound();
            }

            return View(geoLocation);
        }

        // GET: GeoLocation/Create
        public IActionResult Create()
        {
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Id");
            return View();
        }

        // POST: GeoLocation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ClientId,CreatedAt,IsDriver,Latitude,Longitude,UpdatedAt")] GeoLocation geoLocation)
        {
            if (ModelState.IsValid)
            {
                _context.Add(geoLocation);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Id", geoLocation.ClientId);
            return View(geoLocation);
        }

        // GET: GeoLocation/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var geoLocation = await _context.GeoLocation.Include(c=>c.Client).SingleOrDefaultAsync(m => m.Id == id);
            if (geoLocation == null)
            {
                return NotFound();
            }
            var clientsQuery = from d in _context.Client
                               orderby d.PhoneNumber
                               select d;
            ViewData["ClientId"] = new SelectList(clientsQuery, "Id", "PhoneNumber", geoLocation.ClientId);
            return View(geoLocation);
        }

        // POST: GeoLocation/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,ClientId,CreatedAt,IsDriver,Latitude,Longitude,UpdatedAt")] GeoLocation geoLocation)
        {
            if (id != geoLocation.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(geoLocation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GeoLocationExists(geoLocation.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Id", geoLocation.ClientId);
            return View(geoLocation);
        }

        // GET: GeoLocation/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var geoLocation = await _context.GeoLocation.SingleOrDefaultAsync(m => m.Id == id);
            if (geoLocation == null)
            {
                return NotFound();
            }

            return View(geoLocation);
        }

        // POST: GeoLocation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var geoLocation = await _context.GeoLocation.SingleOrDefaultAsync(m => m.Id == id);
            _context.GeoLocation.Remove(geoLocation);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool GeoLocationExists(string id)
        {
            return _context.GeoLocation.Any(e => e.Id == id);
        }
    }
}
