using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;
using RohTjWeb.Services;
using Newtonsoft.Json;

namespace RohTjWeb.Controllers
{
    [Produces("application/json")]
    [Route("api/OrderCall")]
    public class ApiOrderCallController : Controller
    {
        private readonly RohContext _context;
        private readonly IPushSender _pushSender;

        public ApiOrderCallController(RohContext context, IPushSender pushSender)
        {
            _context = context;
            _pushSender = pushSender;
        }

        // GET: api/ApiOrderCall
        [HttpGet]
        public IEnumerable<OrderCall> GetOrderCall()
        {
            return _context.OrderCall;
        }

        // GET: api/ApiOrderCall/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrderCall([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            OrderCall orderCall = await _context.OrderCall.Include(o => o.Order).SingleOrDefaultAsync(m => m.Id == id);

            if (orderCall == null)
            {
                return NotFound();
            }

            return Ok(orderCall);
        }

        [HttpGet("hasclientnewcall/{id}")]
        public async Task<IActionResult> GetNewOrderCall([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            // Find orderCall
            var orderCall = await _context.OrderCall
                .Include(o => o.WhoCalled.Brand)
                .Include(o => o.WhoCalled.BrandModel)
                .Where(o => o.OrderId == id && o.State == OrderCallState.New)
                .OrderBy(oc => oc.CreatedAt).FirstOrDefaultAsync();

            return Ok(orderCall);
        }

        [HttpGet("hasclientnewcallbyphone/{id}/{phone}")]
        public async Task<IActionResult> GetNewOrderCallByNumber([FromRoute] string id, [FromRoute] string phone)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            phone = "+" + phone;
            // Find orderCall
            var orderCall = await _context.OrderCall
                .Include(o => o.WhoCalled.Brand)
                .Include(o => o.WhoCalled.BrandModel)
                .Where(o => o.OrderId == id && o.WhoCalled.PhoneNumber == phone && o.State == OrderCallState.New)
                .OrderBy(oc => oc.CreatedAt).FirstOrDefaultAsync();

            return Ok(orderCall);
        }

        [HttpGet("notifydriver/{id}")]
        public async Task<IActionResult> NotifyDriver([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            // Find orderCall
            var orderCall = _context.OrderCall.Include(o => o.WhoCalled).FirstOrDefault(o => o.Id == id && o.WhoCalled != null);
            if (orderCall != null)
            {
                var driverPhoneNumber = orderCall.WhoCalled.PhoneNumber.Replace("+", "");
                var pushParameter = new PushParameter("client_accepted", id);
                await _pushSender.SendPushAsync("gcm", "������ ������ ���� �����������", new string[] { driverPhoneNumber }, pushParameter.Serialize());
            }
            return Ok();
        }

        // PUT: api/ApiOrderCall/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrderCall([FromRoute] string id, [FromBody] OrderCall orderCall)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != orderCall.Id)
            {
                return BadRequest();
            }

            _context.Entry(orderCall).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderCallExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ApiOrderCall
        [HttpPost]
        public async Task<IActionResult> PostOrderCall([FromBody] OrderCall orderCall)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (string.IsNullOrEmpty(orderCall.Id))
            {
                orderCall.Id = Guid.NewGuid().ToString();
            }

            _context.OrderCall.Add(orderCall);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (OrderCallExists(orderCall.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetOrderCall", new { id = orderCall.Id }, orderCall);
        }

        // DELETE: api/ApiOrderCall/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrderCall([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            OrderCall orderCall = await _context.OrderCall.SingleOrDefaultAsync(m => m.Id == id);
            if (orderCall == null)
            {
                return NotFound();
            }

            _context.OrderCall.Remove(orderCall);
            await _context.SaveChangesAsync();

            return Ok(orderCall);
        }

        private bool OrderCallExists(string id)
        {
            return _context.OrderCall.Any(e => e.Id == id);
        }
    }
}