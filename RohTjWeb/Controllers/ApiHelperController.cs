using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;
using Microsoft.AspNetCore.Authorization;

namespace RohTjWeb.Controllers
{
    [Authorize]
    [Route("Helper")]
    public class ApiHelperController : Controller
    {
        private readonly RohContext _context;

        public ApiHelperController(RohContext context)
        {
            _context = context;    
        }

        [HttpGet("CurrentDrivers")]
        public IEnumerable<GeoLocation> GetGeoLocation()
        {
            var now = DateTime.Now;
            return _context.GeoLocation
                .Include(g => g.Client)
                .Where(g=>now.Subtract(g.UpdatedAt.Value.DateTime).TotalMinutes < 5);
        }
        
    }
}
