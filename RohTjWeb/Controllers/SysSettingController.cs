using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;

namespace RohTjWeb.Controllers
{
    public class SysSettingController : Controller
    {
        private readonly RohContext _context;

        public SysSettingController(RohContext context)
        {
            _context = context;    
        }

        // GET: SysSetting
        public async Task<IActionResult> Index()
        {
            return View(await _context.SysSetting.ToListAsync());
        }

        // GET: SysSetting/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sysSetting = await _context.SysSetting
                .SingleOrDefaultAsync(m => m.Id == id);
            if (sysSetting == null)
            {
                return NotFound();
            }

            return View(sysSetting);
        }

        // GET: SysSetting/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SysSetting/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SettingType,SettingCode,Value,Id,CreatedAt,UpdatedAt")] SysSetting sysSetting)
        {
            if (ModelState.IsValid)
            {
                _context.Add(sysSetting);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(sysSetting);
        }

        // GET: SysSetting/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sysSetting = await _context.SysSetting.SingleOrDefaultAsync(m => m.Id == id);
            if (sysSetting == null)
            {
                return NotFound();
            }
            return View(sysSetting);
        }

        // POST: SysSetting/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("SettingType,SettingCode,Value,Id,CreatedAt,UpdatedAt")] SysSetting sysSetting)
        {
            if (id != sysSetting.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(sysSetting);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SysSettingExists(sysSetting.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(sysSetting);
        }

        // GET: SysSetting/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sysSetting = await _context.SysSetting
                .SingleOrDefaultAsync(m => m.Id == id);
            if (sysSetting == null)
            {
                return NotFound();
            }

            return View(sysSetting);
        }

        // POST: SysSetting/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var sysSetting = await _context.SysSetting.SingleOrDefaultAsync(m => m.Id == id);
            _context.SysSetting.Remove(sysSetting);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool SysSettingExists(string id)
        {
            return _context.SysSetting.Any(e => e.Id == id);
        }
    }
}
