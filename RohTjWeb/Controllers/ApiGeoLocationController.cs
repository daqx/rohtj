using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;
using RohTjWeb.Helpers;

namespace RohTjWeb.Controllers
{
    [Produces("application/json")]
    [Route("api/GeoLocation")]
    public class ApiGeoLocationController : Controller
    {
        private readonly RohContext _context;

        public ApiGeoLocationController(RohContext context)
        {
            _context = context;
        }

        // GET: api/ApiGeoLocation
        [HttpGet]
        public IEnumerable<GeoLocation> GetGeoLocation()
        {
            return _context.GeoLocation.Include(g => g.Client);
        }

        // GET: api/ApiGeoLocation/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetGeoLocation([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            GeoLocation geoLocation = await _context.GeoLocation.SingleOrDefaultAsync(m => m.Id == id);

            if (geoLocation == null)
            {
                return NotFound();
            }

            return Ok(geoLocation);
        }

        // GET: api/ApiGeoLocation/byclient/5
        [HttpGet("byclient/{id}")]
        public async Task<IActionResult> GetGeoLocationByClient([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            GeoLocation geoLocation = await _context.GeoLocation.Include(g=>g.Client).SingleOrDefaultAsync(m => m.ClientId == id);

            if (geoLocation == null)
            {
                return NotFound();
            }

            return Ok(geoLocation);
        }

        // GET: api/ApiGeoLocation/5
        [HttpGet("near/{id}")]
        public async Task<IActionResult> GetNearBy([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            GeoLocation geoLocation = await _context.GeoLocation.Include(c => c.Client).SingleOrDefaultAsync(m => m.ClientId == id);

            if (geoLocation == null)
            {
                return NotFound();
            }

            var resultSet = geoLocation.FindLocationsWithinDistance( _context);

            return Ok(resultSet);
        }

        // PUT: api/ApiGeoLocation/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGeoLocation([FromRoute] string id, [FromBody] GeoLocation geoLocation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != geoLocation.Id)
            {
                return BadRequest();
            }

            _context.Entry(geoLocation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GeoLocationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ApiGeoLocation
        [HttpPost]
        public IActionResult PostGeoLocation([FromBody] GeoLocation geoLocation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (string.IsNullOrEmpty(geoLocation.Id))
            {
                geoLocation.Id = Guid.NewGuid().ToString();
            }

            var existingLocation = _context.GeoLocation.FirstOrDefault(l => l.ClientId == geoLocation.ClientId);
            if (existingLocation != null)
            {
                _context.GeoLocation.Remove(existingLocation);
            }
            _context.GeoLocation.Add(geoLocation);
            // Add to history for being able to track
            GeoLocationHistory history = new GeoLocationHistory()
            {
                Id = Guid.NewGuid().ToString(),
                IsDriver = geoLocation.IsDriver,
                Latitude = geoLocation.Latitude,
                Longitude = geoLocation.Longitude,
                ClientId = geoLocation.ClientId
            };
            _context.GeoLocationHistory.Add(history);

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (GeoLocationExists(geoLocation.Id))
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                else
                    throw;
            }

            return CreatedAtAction("GetGeoLocation", new { id = geoLocation.Id }, geoLocation);
        }

        // DELETE: api/ApiGeoLocation/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGeoLocation([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            GeoLocation geoLocation = await _context.GeoLocation.SingleOrDefaultAsync(m => m.Id == id);
            if (geoLocation == null)
            {
                return NotFound();
            }

            _context.GeoLocation.Remove(geoLocation);
            await _context.SaveChangesAsync();

            return Ok(geoLocation);
        }

        private bool GeoLocationExists(string id)
        {
            return _context.GeoLocation.Any(e => e.Id == id);
        }
    }
}