﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MimeKit;
using MailKit.Net.Smtp;
using MailKit.Security;
using RohTjWeb.Services;
using RohTjWeb.Models;

namespace RohTjWeb.Controllers
{
    public class MobileController : Controller
    {
        private readonly IEmailSender _emailSender;
        public MobileController(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        public IActionResult Index()
        {
            return View();
        }
        

        public IActionResult Rules()
        {
            ViewData["Message"] = "Правила";
            return View();
        }

        public IActionResult Oferta()
        {
            ViewData["Message"] = "Оферта";
            return View();
        }

        public IActionResult Help()
        {
            ViewData["Message"] = "Помощь";
            return View();
        }
 
    }
}
