﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;
using RohTjWeb.Services;
using Newtonsoft.Json;
using RohTjWeb.Helpers;
using Microsoft.ApplicationInsights;
using Hangfire;
using System.Configuration;

namespace RohTjWeb.Controllers
{
    [Produces("application/json")]
    [Route("api/Order")]
    public class ApiOrderController : Controller
    {
        private readonly RohContext _context;
        private readonly IPushSender _pushSender;

        public ApiOrderController(RohContext context, IPushSender pushSender)
        {
            _context = context;
            _pushSender = pushSender;
        }

        // GET: api/ApiOrder
        [HttpGet]
        public IEnumerable<Order> GetOrder()
        {
            return _context.Order;
        }

        /// <summary>
        /// Returns clients(or drivers) his own orders
        /// </summary>
        /// <param name="id">Client Id</param>
        /// <param name="ordertype">Taxi type (City, Between, Truck)</param>
        /// <returns></returns>
        [HttpGet("mybyclient/{id}/{ordertype}")]
        public IEnumerable<Order> GetMyOrdersByClientIdAndType([FromRoute] string id, [FromRoute] OrderType ordertype)
        {
            var clientId = HttpContext.Items["ClientId"] as string;
            var client = _context.Client.FirstOrDefault(c => c.Id == id);
            if (client == null)
            {
                return null;
            }
            if (client.IsDriver)
            {
                return _context.Order.Include(o => o.Client).Where(o => o.OrderType == ordertype && o.DriverId == client.Id && o.Status != OrderStatus.Cancelled).OrderByDescending(o => o.CreatedAt);
            }
            return _context.Order.Include(o => o.Client).Where(o => o.OrderType == ordertype && o.ClientId == client.Id && o.Status != OrderStatus.Cancelled).OrderByDescending(o => o.CreatedAt);
        }
        /// <summary>
        /// Returns all client orders for drivers, or all driver orders for clients by type
        /// </summary>
        /// <param name="id">Client Id</param>
        /// <param name="ordertype">Taxi type (City, Between, Truck)</param>
        /// <returns></returns>
        [HttpGet("byclient/{id}/{ordertype}")]
        public async Task<IEnumerable<Order>> GetOrdersByClientIdAndType([FromRoute] string id, [FromRoute] OrderType ordertype)
        {
            var client = await _context.Client.FirstOrDefaultAsync(c => c.Id == id);
            if (client == null)
                return null;

            var now = DateTime.Now.Date;
            var orders = _context.Order
                .Include(o => o.Client)
                .Include(o => o.Driver)
                .Where(o => o.OrderType == ordertype
                && o.IsDriver != client.IsDriver
                && o.ClientId != id && o.DriverId != id
                && o.Status != OrderStatus.Cancelled
                && (o.Status != OrderStatus.Completed || o.UpdatedAt.Value.Date == now.Date)
                );

            // if orderType is city then get only orders where client or driver is registered on that city
            if (ordertype == OrderType.City)
            {
                if (client.IsDriver)
                    orders = orders.Where(o => o.Client != null && o.Client.CityId == client.CityId);
                else
                    orders = orders.Where(o => o.Driver != null && o.Driver.CityId == client.CityId);
            }

            IQueryable<Order> result = orders.OrderByDescending(o => o.CreatedAt).Take(10);

            return result;
        }

        // GET: api/Order/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrder([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Order order = await _context.Order
                .Include(o => o.Client)
                .Include(o => o.Driver)
                .Include(o => o.Client.Brand)
                .Include(o => o.Client.BrandModel)
                .Include(o => o.Driver.Brand)
                .Include(o => o.Driver.BrandModel)
                .SingleOrDefaultAsync(m => m.Id == id);

            if (order == null)
            {
                return NotFound();
            }

            return Ok(order);
        }

        [HttpGet("update_city_order_state")]
        public async Task<IActionResult> UpdateCityOrderState()
        {
            var oneHoutBefor = DateTime.Now - TimeSpan.FromHours(1);
            var orders = _context.Order.Where(ord => ord.Status == OrderStatus.Started && ord.OrderType == OrderType.City && ord.CreatedAt < oneHoutBefor);

            orders.ToList().ForEach(ord => ord.Status = OrderStatus.Completed);
            await _context.SaveChangesAsync();
            return Ok();
        }

        // PUT: api/ApiOrder/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrder([FromRoute] string id, [FromBody] Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != order.Id)
            {
                return BadRequest();
            }

            _context.Entry(order).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ApiOrder
        [HttpPost]
        public async Task<IActionResult> PostOrder([FromBody] Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (string.IsNullOrEmpty(order.Id))
            {
                order.Id = Guid.NewGuid().ToString();
            }

            _context.Order.Add(order);
            try
            {
                await _context.SaveChangesAsync();

                if (!order.IsDriver)
                    BackgroundJob.Enqueue<IRohJob>((x) => x.NotifyDriver(order));
            }
            catch (DbUpdateException)
            {
                if (OrderExists(order.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetOrder", new { id = order.Id }, order);
        }

        // DELETE: api/ApiOrder/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrder([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Order order = await _context.Order.SingleOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            _context.Order.Remove(order);
            await _context.SaveChangesAsync();

            return Ok(order);
        }

        [HttpGet("review_orders")]
        public IActionResult ReviewOrders()
        {
            BackgroundJob.Enqueue<IRohJob>((x) => x.ReviewAwailableOrders());
            return Ok();
        }

        //public async Task ReviewAwailableOrders()
        //{
        //    string sentNumbers = string.Empty;
        //    var message = "Пожалуйста, оцените вашу поездку";
        //    var now = DateTime.Now;
        //    var telemetryClient = new TelemetryClient();

        //    try
        //    {
        //        var optionsBuilder = new DbContextOptionsBuilder<RohContext>();
        //            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"))
        //        using (var context = new RohContext())
        //        {
        //            var orders = context.Order.Include(o => o.Client)
        //                        .Where(o => o.Status == OrderStatus.Started
        //                        && !o.IsReviewSent
        //                        && now.Subtract(o.CreatedAt.Value.DateTime).TotalMinutes > 30
        //                        && o.OrderType == OrderType.City
        //                        && !string.IsNullOrEmpty(o.ClientId) && !string.IsNullOrEmpty(o.DriverId)
        //                        );
        //            bool isReviewSent = false;
        //            foreach (var order in orders)
        //            {
        //                var phoneNumber = order.Client.PhoneNumber.Replace("+", "");
        //                var pushParameter = new PushParameter("review", order.Id);

        //                await _pushSender.SendPushAsync("gcm", message, new string[] { phoneNumber }, pushParameter.Serialize());
        //                sentNumbers = sentNumbers + ";" + phoneNumber;
        //                order.IsReviewSent = true;
        //                isReviewSent = true;
        //            }
        //            if (isReviewSent)
        //                _context.SaveChanges(); 
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        telemetryClient.TrackException(ex);
        //    }
        //}

        //[HttpGet("notify_driver")]
        //public IActionResult NotifyDriverAboutNewCityOrders()
        //{
        //    string sentNumbers = string.Empty;
        //    var telemetryClient = new TelemetryClient();
        //    var now = DateTime.Now;

        //    try
        //    {
        //        var orders = _context.Order.Include(o => o.Client)
        //            .Where(o => o.Status == OrderStatus.New
        //            && now.Subtract(o.CreatedAt.Value.DateTime).TotalMinutes < 10
        //            && !o.IsDriver
        //            && !o.IsNotificationSent
        //            && o.OrderType == OrderType.City
        //            ).ToList();
        //        foreach (var order in orders)
        //        {
        //            BackgroundJob.Enqueue<IRohJob>((x) => x.NotifyDriver(order));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        telemetryClient.TrackException(ex);
        //    }
        //    return Ok(sentNumbers);
        //}
        
        //async Task<List<Client>> GetNearestClients(string clientId)
        //{
        //    double distance = 2;
        //    GeoLocation geoLocation = await _context.GeoLocation
        //        .Include(c => c.Client)
        //        .SingleOrDefaultAsync(m => m.ClientId == clientId);

        //    PointLocation currentLocation = PointLocation.FromDegrees(geoLocation.Latitude, geoLocation.Longitude);

        //    List<GeoLocation> results = new List<GeoLocation>();
        //    var now = DateTime.Now;
        //    var driversGeoLocations = _context.GeoLocation.Include(g => g.Client)
        //        .Where(l => l.Client.IsDriver && now.Subtract(l.UpdatedAt.Value.DateTime).TotalMinutes < 2 && l.Client.IsFree);

        //    foreach (var location in driversGeoLocations)
        //    {
        //        var point = PointLocation.FromDegrees(location.Latitude, location.Longitude);
        //        if (currentLocation.DistanceTo(point) <= distance)
        //        {
        //            results.Add(location);
        //        }
        //    }
        //    return results.Select(l => l.Client).ToList();
        //}

        [HttpGet("get_client_id")]
        public IActionResult GetClientId()
        {
            return Ok(HttpContext.Items["ClientId"]);
        }

        [HttpGet("review_order/{id}")]
        public async Task<IActionResult> ReviewOrder([FromRoute] string id)
        {
            var message = "Пожалуйста, оцените вашу поездку";
            var now = DateTime.Now;
            var order = await _context.Order.Include(o => o.Client).FirstOrDefaultAsync(o => o.Id == id);
            string sentNumbers = string.Empty;
            if (order != null)
            {
                var phoneNumber = order.Client.PhoneNumber.Replace("+", "");
                var pushParameter = new PushParameter("review", order.Id);

                await _pushSender.SendPushAsync("gcm", message, new string[] { phoneNumber }, pushParameter.Serialize());
                sentNumbers += ";" + phoneNumber;
            }
            return Ok(sentNumbers);
        }

        private bool OrderExists(string id)
        {
            return _context.Order.Any(e => e.Id == id);
        }


    }
}