using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;
using Microsoft.AspNetCore.Authorization;

namespace RohTjWeb.Controllers
{
    [Authorize]
    public class RegistrationCodeController : Controller
    {
        private readonly RohContext _context;

        public RegistrationCodeController(RohContext context)
        {
            _context = context;    
        }

        // GET: RegistrationCode
        public async Task<IActionResult> Index()
        {
            var rohContext = _context.RegistrationCode.Include(r => r.Client);
            return View(await rohContext.OrderByDescending(r=>r.CreatedAt).ToListAsync());
        }

        // GET: RegistrationCode/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var registrationCode = await _context.RegistrationCode.Include(r=>r.Client).SingleOrDefaultAsync(m => m.Id == id);
            if (registrationCode == null)
            {
                return NotFound();
            }

            return View(registrationCode);
        }

        // GET: RegistrationCode/Create
        public IActionResult Create()
        {
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Id");
            return View();
        }

        // POST: RegistrationCode/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ClientId,Code,CreatedAt,UpdatedAt,UsedTime")] RegistrationCode registrationCode)
        {
            registrationCode.Id = Guid.NewGuid().ToString();
            if (ModelState.IsValid)
            {
                _context.Add(registrationCode);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Id", registrationCode.ClientId);
            return View(registrationCode);
        }

        // GET: RegistrationCode/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var registrationCode = await _context.RegistrationCode.Include(r=>r.Client).SingleOrDefaultAsync(m => m.Id == id);
            if (registrationCode == null)
            {
                return NotFound();
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Id", registrationCode.ClientId);
            return View(registrationCode);
        }

        // POST: RegistrationCode/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,ClientId,Code,CreatedAt,UpdatedAt,UsedTime")] RegistrationCode registrationCode)
        {
            if (id != registrationCode.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(registrationCode);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RegistrationCodeExists(registrationCode.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Id", registrationCode.ClientId);
            return View(registrationCode);
        }

        // GET: RegistrationCode/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var registrationCode = await _context.RegistrationCode.Include(r => r.Client).SingleOrDefaultAsync(m => m.Id == id);
            if (registrationCode == null)
            {
                return NotFound();
            }

            return View(registrationCode);
        }

        // POST: RegistrationCode/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var registrationCode = await _context.RegistrationCode.SingleOrDefaultAsync(m => m.Id == id);
            _context.RegistrationCode.Remove(registrationCode);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool RegistrationCodeExists(string id)
        {
            return _context.RegistrationCode.Any(e => e.Id == id);
        }
    }
}
