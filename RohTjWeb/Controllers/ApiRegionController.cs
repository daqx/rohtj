using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;

namespace RohTjWeb.Controllers
{
    [Produces("application/json")]
    [Route("api/Region")]
    public class ApiRegionController : Controller
    {
        private readonly RohContext _context;

        public ApiRegionController(RohContext context)
        {
            _context = context;
        }

        // GET: api/ApiRegion
        [HttpGet]
        public IEnumerable<Region> GetRegion()
        {
            return _context.Region;
        }

        // GET: api/ApiRegion/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRegion([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Region region = await _context.Region.SingleOrDefaultAsync(m => m.Id == id);

            if (region == null)
            {
                return NotFound();
            }

            return Ok(region);
        }

        // PUT: api/ApiRegion/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRegion([FromRoute] string id, [FromBody] Region region)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != region.Id)
            {
                return BadRequest();
            }

            _context.Entry(region).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RegionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ApiRegion
        [HttpPost]
        public async Task<IActionResult> PostRegion([FromBody] Region region)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Region.Add(region);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (RegionExists(region.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetRegion", new { id = region.Id }, region);
        }

        // DELETE: api/ApiRegion/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRegion([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Region region = await _context.Region.SingleOrDefaultAsync(m => m.Id == id);
            if (region == null)
            {
                return NotFound();
            }

            _context.Region.Remove(region);
            await _context.SaveChangesAsync();

            return Ok(region);
        }

        private bool RegionExists(string id)
        {
            return _context.Region.Any(e => e.Id == id);
        }
    }
}