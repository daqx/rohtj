﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;
using RohTjWeb.Services;
using Microsoft.AspNetCore.Authorization;

namespace RohTjWeb.Controllers
{
    [Route("Feedback")]
    public class FeedbackController : Controller
    {
        private readonly IEmailSender _emailSender;
        public FeedbackController(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        // GET: City
        [HttpGet("{id}")]
        public IActionResult Index([FromRoute] string id)
        {
            var feedback = new Feedback() { Phone = id };
            return View(feedback);
        }
        
        // POST: City/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Title, Message, Phone")] Feedback feedback)
        {
            // This is for the Contact Form view, you can test the form 
            // at http://localhost:41717/Home/Contact
            ViewData["Message"] = null;
            ViewData["Error"] = null;
            if (ModelState.IsValid)
            {
                _emailSender.SendEmail(
                    "info@roh.tj",
                    "Roh APP: " + feedback.Title,
                    feedback.Message + "\n \n \n Тел.: +" + feedback.Phone

                    );
                ViewData["message_color"] = "green";
                ViewData["Message"] = "*Ваше сообщение отправлено.";
                //return RedirectToAction(feedback.Phone);
            }
            else
            {
                ViewData["message_color"] = "red";
                ViewData["Message"] = "*Заполните все поля";
            }

            //ModelState.Clear();
            //return View();
            return View("Index", feedback);
        }
    }
}
