using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;

namespace RohTjWeb.Controllers
{
    [Produces("application/json")]
    [Route("api/City")]
    public class ApiCityController : Controller
    {
        private readonly RohContext _context;

        public ApiCityController(RohContext context)
        {
            _context = context;
        }

        // GET: api/ApiCity
        [HttpGet]
        public IEnumerable<City> GetCity()
        {
            return _context.City.OrderByDescending(c => c.Order).ThenBy(c => c.Name);
        }

        // GET: api/ApiCity/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCity([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            City city = await _context.City.SingleOrDefaultAsync(m => m.Id == id);

            if (city == null)
            {
                return NotFound();
            }

            return Ok(city);
        }

        // PUT: api/ApiCity/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCity([FromRoute] string id, [FromBody] City city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != city.Id)
            {
                return BadRequest();
            }

            _context.Entry(city).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ApiCity
        [HttpPost]
        public async Task<IActionResult> PostCity([FromBody] City city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.City.Add(city);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CityExists(city.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCity", new { id = city.Id }, city);
        }

        // DELETE: api/ApiCity/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCity([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            City city = await _context.City.SingleOrDefaultAsync(m => m.Id == id);
            if (city == null)
            {
                return NotFound();
            }

            _context.City.Remove(city);
            await _context.SaveChangesAsync();

            return Ok(city);
        }

        private bool CityExists(string id)
        {
            return _context.City.Any(e => e.Id == id);
        }
    }
}