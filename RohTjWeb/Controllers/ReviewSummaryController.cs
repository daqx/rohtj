using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RohTjWeb.Data;
using RohTjWeb.Models;
using Microsoft.AspNetCore.Authorization;

namespace RohTjWeb.Controllers
{
    [Authorize]
    public class ReviewSummaryController : Controller
    {
        private readonly RohContext _context;

        public ReviewSummaryController(RohContext context)
        {
            _context = context;    
        }
    }
}
