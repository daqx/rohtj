﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RohTjWeb.Data;
using RohTjWeb.Models;
using RohTjWeb.Services;
using NonFactors.Mvc.Grid;
using RohTjWeb.Utilities;
using Microsoft.AspNetCore.Identity;
using Hangfire;
using Hangfire.SqlServer;
using Hangfire.Dashboard;

namespace RohTjWeb
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddMvc();

            services.AddMvcGrid();
            // add our activator to the GlobalConfiguration
            var hangfireOptions = new SqlServerStorageOptions
            {
                QueuePollInterval = TimeSpan.FromSeconds(50) // Default value
            };
            services.AddHangfire(configuration => {
                configuration.UseSqlServerStorage(Configuration.GetConnectionString("DefaultConnection"),hangfireOptions);
            });
            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
            services.AddTransient<IPushSender, AuthMessageSender>();
            services.AddTransient<IRohJob, RohJob>();


            services.AddDbContext<RohContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory
            , RohContext dbContext, ApplicationDbContext applicationDbContext, IServiceProvider serviceProvider, UserManager<ApplicationUser> userManager)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddAzureWebAppDiagnostics();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseIdentity();

            app.UseWhen(x => (x.Request.Path.StartsWithSegments("/api", StringComparison.OrdinalIgnoreCase)),
            builder =>
            {
                builder.UseMiddleware<AuthenticationMiddleware>();
            });
            // Add external authentication middleware below. To configure them please see http://go.microsoft.com/fwlink/?LinkID=532715

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                name: "default1",
                template: "{action}/{id?}",
                defaults: new { controller = "Home", action = "Index" });

                routes.MapRoute(
                    name: "apiorder",
                    template: "{controller}/{action}/{id}/{ordertype}");
                routes.MapRoute(
                    name: "apiphone",
                    template: "{controller}/{action}/{id}/{phone}");
            });

            //Hangfire            
            app.UseHangfireDashboard();
            app.UseHangfireServer();

            app.UseHangfireDashboard("/hangfire", new DashboardOptions()
            {
                Authorization = new[] { new MyRestrictiveAuthorizationFilter() }
            });

            RecurringJob.AddOrUpdate<IRohJob>((x) => x.ReviewAwailableOrders(),Cron.Minutely);
            RecurringJob.AddOrUpdate<IRohJob>((x) => x.CloseOldOrders(),Cron.Minutely);

            RohInitializer.Seed(dbContext, applicationDbContext, userManager);
            //RohInitializer.CreateRoles(applicationDbContext, serviceProvider);
        }

    }
}
