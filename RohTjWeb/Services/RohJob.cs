﻿using Microsoft.ApplicationInsights;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RohTjWeb.Data;
using RohTjWeb.Helpers;
using RohTjWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RohTjWeb.Services
{
    public class RohJob : IDisposable, IRohJob
    {
        private readonly RohContext _context;
        private readonly IPushSender _pushSender;
        private readonly ILogger _logger;


        public RohJob(RohContext context, IPushSender pushSender, ILoggerFactory loggerFactory)
        {
            _context = context;
            _pushSender = pushSender;
            _logger = loggerFactory.CreateLogger<RohJob>();
        }

        public async Task ReviewAwailableOrders()
        {
            string sentNumbers = string.Empty;
            var message = "Пожалуйста, оцените вашу поездку";
            var now = DateTime.Now;
            var telemetryClient = new TelemetryClient();

            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<RohContext>();
                var orders = _context.Order.Include(o => o.Client)
                            .Where(o => o.Status == OrderStatus.Started
                            && !o.IsReviewSent
                            && now.Subtract(o.CreatedAt.Value.DateTime).TotalMinutes > 30
                            && o.OrderType == OrderType.City
                            && !string.IsNullOrEmpty(o.ClientId) && !string.IsNullOrEmpty(o.DriverId)
                            );
                bool isReviewSent = false;
                foreach (var order in orders)
                {
                    var phoneNumber = order.Client.PhoneNumber.Replace("+", "");
                    var pushParameter = new PushParameter("review", order.Id);

                    await _pushSender.SendPushAsync("gcm", message, new string[] { phoneNumber }, pushParameter.Serialize());
                    sentNumbers = sentNumbers + ";" + phoneNumber;
                    order.IsReviewSent = true;
                    isReviewSent = true;
                }
                if (isReviewSent)
                    _context.SaveChanges();
            }
            catch (Exception ex)
            {
                telemetryClient.TrackException(ex);
            }
        }

        public async Task NotifyDriver(Order order)
        {
            string fullMessage = $"ПУТЬ:  {order.From} -> {order.To}, цена {order.Price}";
            // Todo: need to lock orders
            order.IsNotificationSent = true;
            _context.SaveChanges();

            GeoLocation geoLocation = _context.GeoLocation.Include(c => c.Client).FirstOrDefault(m => m.ClientId == order.ClientId);

            if (geoLocation != null)
            {
                var activeDrivers = geoLocation.FindLocationsWithinDistance(_context, true);

                if (activeDrivers.Any())
                {
                    var driverPhoneNumbers = activeDrivers.Select(l => l.Client.PhoneNumber.Replace("+", "")).ToArray();
                    _logger.LogWarning($"############### NotifyDriver {driverPhoneNumbers.ToString()}");
                    _logger.LogWarning($"############### FULL MESSAGE {fullMessage}");

                    await _pushSender.SendPushAsync("gcm", fullMessage, driverPhoneNumbers);
                }
            }
        }

        public async Task CloseOldOrders()
        {
            _logger.LogWarning("!!!!!!!!!!!!!!!Close old orders!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            var oneHourBefor = DateTimeOffset.Now - TimeSpan.FromHours(1);
            _logger.LogWarning($"OneHourBefor ={oneHourBefor.ToString()}");
            var cityOrders = _context.Order.Where(ord => (ord.Status == OrderStatus.Started || ord.Status == OrderStatus.New)
                                                    && ord.OrderType == OrderType.City
                                                    && ord.CreatedAt < oneHourBefor);

            foreach (var order in cityOrders)
            {
                _logger.LogWarning($"ord.CreatedAt ={order.CreatedAt.ToString()}");
                if (order.Status == OrderStatus.New)
                    order.Status = OrderStatus.Cancelled;
                else if(order.Status == OrderStatus.Started)
                    order.Status = OrderStatus.Completed;
            }

            var oneDayBefor = DateTimeOffset.Now - TimeSpan.FromDays(1);
            var betweenCityOrders = _context.Order.Where(ord => ord.Status == OrderStatus.Started && ord.OrderType == OrderType.BetweenCity && ord.CreatedAt < oneDayBefor);
            betweenCityOrders.ToList().ForEach(ord => ord.Status = OrderStatus.Completed);

            var oneMonthBefor = DateTimeOffset.Now - TimeSpan.FromDays(30);
            var truckOrders = _context.Order.Where(ord => ord.Status == OrderStatus.Started && ord.OrderType == OrderType.Truck && ord.CreatedAt < oneMonthBefor);
            truckOrders.ToList().ForEach(ord => ord.Status = OrderStatus.Completed);

            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
