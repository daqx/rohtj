﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Azure.NotificationHubs;
using MimeKit;
using RohTjWeb.Models;
using RohTjWeb.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace RohTjWeb.Services
{
    // This class is used by the application to send Email and SMS and Push
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link http://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender, ISmsSender, IPushSender
    {
        public void SendEmail(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Компания Рох", "rohtj@yandex.ru"));
            emailMessage.To.Add(new MailboxAddress("Client roh", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart("plain") { Text = message };

            using (var client = new SmtpClient())
            {
                //client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                client.Connect("smtp.yandex.ru", 25, false);
                //client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate("rohtj@yandex.ru", "Dilovar123");
                client.Send(emailMessage);
                client.Disconnect(true);
            }
        }

        public async Task<HttpStatusCode> SendPushAsync(string pns, string message, string[] userTag, string param = null)
        {
            NotificationOutcome outcome = null;
            HttpStatusCode ret = HttpStatusCode.InternalServerError;
            param = "\"param\":" + (param ?? "\"\"");
            switch (pns.ToLower())
            {
                case "wns":
                    // Windows 8.1 / Windows Phone 8.1
                    var toast = @"<toast><visual><binding template=""ToastText01""><text id=""1"">" +
                                 message + "</text></binding></visual></toast>";
                    outcome = await Notifications.Instance.Hub.SendWindowsNativeNotificationAsync(toast, userTag);
                    break;
                case "apns":
                    // iOS
                    var alert = "{\"aps\":{\"alert\":\"" + message + "\"}}";
                    outcome = await Notifications.Instance.Hub.SendAppleNativeNotificationAsync(alert, userTag);
                    break;
                case "gcm":
                    // Android
                    var notif = "{ \"data\" : {\"message\":\"" + message + "\", " + param + "}}";
                    try
                    {
                        outcome = await Notifications.Instance.Hub.SendGcmNativeNotificationAsync(notif, userTag);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    break;
            }

            if (outcome != null)
            {
                if (!((outcome.State == NotificationOutcomeState.Abandoned) ||
                    (outcome.State == NotificationOutcomeState.Unknown)))
                {
                    ret = HttpStatusCode.OK;
                }
            }

            return ret;
        }

        public async Task SendSmsAsync(string number, string message)
        {
            bool success = false;
            var serializer = new Serializer();
            ArrayList array = new ArrayList(1);
            var ht = new Hashtable();

            ht["tel"] = number;
            ht["text"] = message;
            array.Add(ht);
            var text_info = StringUtils.Base64Encode(serializer.Serialize(array));

            var uri = "http://217.11.179.169:7678/sms_notification/send_sms.php";

            try
            {
                var formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("Username", "Roh"),
                    new KeyValuePair<string, string>("Password", "R0h!S@fed"),
                    new KeyValuePair<string, string>("post", "true"),
                    new KeyValuePair<string, string>("text_info",text_info)
                });
                var httpClient = new HttpClient();
                var response = await httpClient.PostAsync(uri, formContent);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    success = content.Contains("Траназакция закончено, записано: 1");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
        }

    }
}
