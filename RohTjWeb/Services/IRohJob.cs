﻿using RohTjWeb.Models;
using System.Threading.Tasks;

namespace RohTjWeb.Services
{
    internal interface IRohJob
    {
        Task ReviewAwailableOrders();

        Task NotifyDriver(Order order);

        Task CloseOldOrders();
    }
}