﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RohTjWeb.Services
{
    public interface IPushSender
    {
        Task<HttpStatusCode> SendPushAsync(string pns, string message, string[] userTag, string param = null);
    }
}
