﻿// Write your Javascript code.
$('.mvc-grid').mvcgrid();


    $.fn.mvcgrid.lang = {
        Text: {
            Contains: 'Содержание',
            Equals: 'Равно',
            NotEquals: 'Не Равно',
            StartsWith: 'Начинается с',
            EndsWith: 'Заканчивается'
        },
        Number: {
            Equals: 'Равно',
            NotEquals: 'Не Равно',
            LessThan: 'Меньше чем',
            GreaterThan: 'Больше чем',
            LessThanOrEqual: 'Меньше или равно',
            GreaterThanOrEqual: 'Больше или равно'
        },
        Date: {
            Equals: 'Равно',
            NotEquals: 'Не Равно',
            LessThan: 'Меньше чем',
            GreaterThan: 'Больше чем',
            LessThanOrEqual: 'Меньше или равно',
            GreaterThanOrEqual: 'Больше или равно'
        },
        Boolean: {
            Yes: 'Да',
            No: 'Нет'
        },
        Filter: {
            Apply: '✔',
            Remove: '✘'
        },
        Operator: {
            Select: '',
            And: 'и',
            Or: 'или'
        }
    };
