var popup_pin = "";
var markersArray = [];
var newmarkersArray = [];
var customIcons = {
    restaura3t: {
        icon: '/images/map/mm_20_blue.png',
        shadow: '/images/map/mm_20_shadow.png'
    },
    bar: {
        icon: '/images/map/mm_20_red.png',
        shadow: '/images/map/mm_20_shadow.png'
    },
    driver_free: {
        icon: '/images/map/driver_available.png',
        shadow: '/images/map/mm_20_shadow.png'
    },
    driver_not_approved: {
        icon: '/images/map/driver_not_approved.png',
        shadow: '/images/map/mm_20_shadow.png'
    },
    driver_on_trip: {
        icon: '/images/map/driver_on_trip.png',
        shadow: '/images/map/mm_20_shadow.png'
    },
    passenger: {
        icon: '/images/map/passenger.png',
        shadow: '/images/map/mm_20_shadow.png'
    },
    driver: {
        icon: '/images/map/driver-70.png',
        shadow: '/images/map/mm_20_shadow.png'
    }
};

function load(lat, lng) {
    var latitude = '';
    var longitude = '';
    if (lat != '') {
        latitude = lat;
        longitude = lng;
    } else {
        var mapOptions = {
            zoom: 13
        };
        map = new google.maps.Map(document.getElementById('map'),
            mapOptions);
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {


                var pos = new google.maps.LatLng(position.coords.latitude,
                    position.coords.longitude);
                console.log("geo locating");

                var infowindow = new google.maps.InfoWindow({
                    map: map,
                    position: pos,
                    content: 'Ваше местоположение'
                });

                map.setCenter(pos);
            }, function () {
                handleNoGeolocation(true);
            });
        } else {
            // Browser doesn't support Geolocation
            handleNoGeolocation(false);
        }
    }
    var address = (document.getElementById('my-address'));
    var autocomplete = new google.maps.places.Autocomplete(address);
    autocomplete.setTypes(['geocode']);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            return;
        }
        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
        }
    });
    var map = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 13,
        mapTypeId: 'roadmap',
        scrollwheel: false,
    });
    var infoWindow = new
    google.maps.InfoWindow;
    (function () {
        var f = function () {
            var marker = new google.maps.Marker();
            downloadUrl(
                "/Helper/CurrentDrivers",
                function (data) {
                    var markers = data
                    popup_pin = "";
                    for (var i = 0; i < markers.length; i++) {
                        var name = markers[i].client.firstName;
                        var client_name = markers[i].client.lastName;
                        var contact = markers[i].client.phoneNumber;
                        var amount = 0;
                        var type = 'driver_free';
                        if (markers[i].client.isFree == false) {
                            type = 'driver_on_trip'
                        }
                        if (markers[i].client.isDriver == false) {
                            type = 'passenger'
                        }
                        var id = "";
                        var angl = "";
                        var point = new google.maps.LatLng(
                            parseFloat(markers[i].latitude),
                            parseFloat(markers[i].longitude));
                        var html = "";
                        var color = "";
                        if (type == 'driver_on_trip') {
                            color = "blue";
                            /*html = "<b>" + client_name + "</b></p><p><span class ='icon-phone' style=''></span><span style='margin-left:5px;'><br>" + contact + "</span></p><b>";*/
                            html = "<b>" + name + " " + client_name + "</b><p style='font-size:16px;'><b><span class ='fa fa-mobile-phone icon-phone' style=''></span><span style='margin-left:5px;'>" + contact + "</span></b><br/>Статус : В пути<br/></p>";
                        } else if (type == 'driver_free') {
                            color = "green";
                            /*html = "<b>" + client_name + "</b></p><p><span class ='icon-phone' style=''></span><span style='margin-left:5px;'><br>" + contact + "</span></p><b>";*/
                            html = "<b>" + name + " " + client_name + "</b><p style='font-size:16px;'><b><span class ='fa fa-mobile-phone icon-phone' style=''></span><span style='margin-left:5px;'>" + contact + "</span></b><br/>Статус : Свободен<br/></p>";
                        } else if (type == 'passenger') {
                            color = "green";
                            /*html = "<b>" + client_name + "</b></p><p><span class ='icon-phone' style=''></span><span style='margin-left:5px;'><br>" + contact + "</span></p><b>";*/
                            html = "<b>" + name + " " + client_name + "</b><p style='font-size:16px;'><b><span class ='fa fa-mobile-phone icon-phone' style=''></span><span style='margin-left:5px;'>" + contact + "</span></b><br/>Статус : Пассажир<br/></p>";
                        } else {
                            color = "red";
                            html = "<b>" + name + " " + client_name + "</b><p style='font-size:16px;'><b><span class ='fa fa-mobile-phone icon-phone' style=''></span><span style='margin-left:5px;'>" + contact + "</span></b><br/>Статус : Неактивный<br/></p>";
                            /*html = "<b>" + client_name + "</b></p><p><span class ='icon-phone' style=''></span><span style='margin-left:5px;'><br>" + contact + "</span></p><b>";*/
                        }

                        var icon = customIcons[type] || {};
                        marker = new google.maps.Marker({
                            map: map,
                            position: point,
                            icon: icon.icon,
                            shadow: icon.shadow
                        });
                        /*marker = new google.maps.Marker({
                         map: map,
                         position: point,
                         icon: {
                         path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                         //path: 'M150 0 L75 200 L225 200 Z',
                         //scale: .1,
                         scale: 6,
                         fillColor: color,
                         fillOpacity: 0.8,
                         strokeWeight: 2,
                         rotation: parseFloat(markers[i].getAttribute("angl")) //this is how to rotate the pointer
                         },
                         shadow: icon.shadow});*/
                        newmarkersArray.push(marker);
                        bindInfoWindow(marker, map,
                            infoWindow, html, type, name, popup_pin);
                    }
                    clearOverlays(markersArray);
                    markersArray = newmarkersArray;
                    newmarkersArray = [];
                });
        };
        window.setInterval(f, 15000);
        f();

        var legendDiv = document.createElement('DIV');
        var legend = new Legend(legendDiv, map);
        legendDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_TOP].push(legendDiv);

    })();
}


function clearOverlays(arr) {
    for (var i = 0; i < arr.length; i++) {
        arr[i].setMap(null);
    }
}

function bindInfoWindow(marker, map, infoWindow, html, type, name, popup_pin) {
    if (name == popup_pin) {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
        popup_pin = "";
    }
    google.maps.event.addListener(marker, 'click', function () {

        if (type == 'driver_free') {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
        } else if (type == 'driver_on_trip') {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
        } else if (type == 'passenger') {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
        } else {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
        }
    });
}

function downloadUrl(url, callback) {
    // Call Web API to get a list of Product
    $.ajax({
        url: '/Helper/CurrentDrivers/',
        type: 'GET',
        dataType: 'json',
        success: function (geoLocations) {
            callback(geoLocations);
        },
        error: function (request, message, error) {
            handleException(request, message, error);
        }
    });
    //var request = window.ActiveXObject ?
    //        new ActiveXObject('Microsoft.XMLHTTP') :
    //        new XMLHttpRequest;
    //request.onreadystatechange = function () {
    //    if (request.readyState == 4) {
    //        request.onreadystatechange = doNothing;
    //        callback(request, request.status);
    //    }
    //};
    //request.open('GET', url, true);
    //request.send(null);
}


function initialize() {

}

function codeAddress() {
    geocoder = new google.maps.Geocoder();
    var address = document.getElementById("my-address").value;
    geocoder.geocode({
        'address': address
    }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {

            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            // initialize_map(results[0].geometry.location.lat(),results[0].geometry.location.lng());
            load(latitude, longitude);
            //         var latlng = new google.maps.LatLng(latitude, longitude);
            // var map = new google.maps.Map(document.getElementById('map'), {
            //     center: latlng,
            //     zoom: 11,
            //     mapTypeId: google.maps.MapTypeId.ROADMAP
            // });
            // var marker = new google.maps.Marker({
            //     position: latlng,
            //     map: map,
            //     title: 'Set lat/lon values for this property',
            //     draggable: true
            // });
        } else {
            //alert("Geocode was not successful for the following reason: " + status);
        }
    });
}

function doNothing() {}

function Legend(controlDiv, map) {
    // Set CSS styles for the DIV containing the control
    // Setting padding to 5 px will offset the control
    // from the edge of the map
    controlDiv.style.padding = '5px';

    // Set CSS for the control border
    var controlUI = document.createElement('DIV');
    controlUI.style.backgroundColor = 'white';
    controlUI.style.borderStyle = 'solid';
    controlUI.style.borderWidth = '1px';
    controlUI.title = 'Legend';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control text
    var controlText = document.createElement('DIV');
    controlText.style.fontFamily = 'Arial,sans-serif';
    controlText.style.fontSize = '12px';
    controlText.style.paddingLeft = '4px';
    controlText.style.paddingRight = '4px';

    // Add the text
    controlText.innerHTML = '<b>Статус:</b><br />' +
        '<img src="/images/map/driver_available.png" style="height:25px;"/> Доступный<br />' +
        '<img src="/images/map/driver_on_trip.png" style="height:25px;"/> В пути<br />' +
        '<img src="/images/map/passenger.png" style="height:25px;"/> Пассажир<br />' //+
        //'<img src="/images/map/driver_not_approved.png" style="height:25px;"/> Неактивный<br />'
    controlUI.appendChild(controlText);
}
google.maps.event.addDomListener(window, 'load', load('', ''));